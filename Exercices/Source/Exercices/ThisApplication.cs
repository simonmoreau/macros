﻿/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 17/01/2016
 * Time: 17:30
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;

namespace Exercices
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("0C27993F-8993-4057-A420-6B32469209DB")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		
		public void Hello()
		{
			TaskDialog td = new TaskDialog("MyTaskDialog");
			
			td.MainContent = "Hello World";
			td.ExpandedContent = "This is my first Revit program";
			
			td.CommonButtons = TaskDialogCommonButtons.Ok | TaskDialogCommonButtons.Cancel | TaskDialogCommonButtons.Close;
			
			td.Show();
		}
		
		public void CreateWall()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//Create a filter
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			
			//Use this filter to retrieve all levels
			ICollection<Element> collection = collector.OfClass(typeof(Level)).ToElements();
			
			//Pick up the first level
			Element level = collection.FirstOrDefault();
			
			//Get its Id
			ElementId levelId = level.Id;
			
			// Build a location line for the wall creation
			XYZ start = new XYZ(0, 0, 0);
			XYZ end = new XYZ(10, 10, 0);
			Line line = Line.CreateBound(start, end);
			
			using (Transaction tx = new Transaction(doc))
			{
				tx.Start("Create Wall");
				
				// Create a wall using the location line
				Wall.Create(doc, line, levelId, false);
				
				tx.Commit();
			}
		}
	}
}