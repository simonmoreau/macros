/*
 * Created by SharpDevelop.
 * User: moreaus
 * Date: 4/15/2014
 * Time: 8:06 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;

namespace WallElevation
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("6694125F-9C71-429F-B7B7-54F5A042B317")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}
		
		private Document _doc;

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		public void WallDimension()
		{
			_doc = this.ActiveUIDocument.Document;
			
			// Find a 3D view to use for the ReferenceIntersector constructor
			FilteredElementCollector docCollector = new FilteredElementCollector(_doc);
			Func<View3D, bool> isNotTemplate = v3 => !(v3.IsTemplate);
			View3D view3D = docCollector.OfClass(typeof(View3D)).Cast<View3D>().First<View3D>(isNotTemplate);
			
			//Find all view
			IEnumerable<View3D> viewEnum = from elem in new FilteredElementCollector(_doc).OfClass(typeof(View3D))
				let view = elem as View3D
				where view.IsTemplate == false
				where view.Name.Contains("smoreau")
				select view;
			
			view3D = viewEnum.First() as View3D;
			
			//Retrive walls
			FilteredElementCollector collector = new FilteredElementCollector(_doc,_doc.ActiveView.Id);
			List<Element> walls = collector.OfCategory(BuiltInCategory.OST_Walls).ToElements().ToList();
			ICollection<ElementId> collectionids = collector.OfCategory(BuiltInCategory.OST_Walls).ToElementIds();
			
			
			//Load the transaction
			using (Transaction tx = new Transaction(_doc)) {
				tx.Start("Create Dimention");
				
				foreach (Element e in walls)
				{
					Wall wall = e as Wall;
					
					if (wall != null)
					{
						// obtain location curve and check that it is straight:
						LocationCurve lc = wall.Location as LocationCurve;

						Curve curve = lc.Curve;
						Line line = curve as Line;
						
						if( null != line )
						{
							//Create an elevation along this line
							//View elevationView = CreateWallElevation(doc,line,wall);
							
//							tx.Commit();
//
//							tx.Start("Create Dimention");
							
							//use the boundingbox of the wall to find the center point of the wall
							double sectionPlanHeight =  1.2 * 3.28084;
							Level viewLevel = _doc.ActiveView.GenLevel;
							double viewElevation =  viewLevel.ProjectElevation +sectionPlanHeight;
							BoundingBoxXYZ wallBBox = wall.get_BoundingBox(view3D);
							XYZ wallCenter = wallBBox.Min.Add(wallBBox.Max).Multiply(0.5);
							wallCenter = new XYZ(wallCenter.X,wallCenter.Y,viewElevation);
							double epsilon = 0.01 * 3.28084;
							
							XYZ wallLeftSide = wallCenter + line.Direction.CrossProduct(new XYZ(0,0,1)).Normalize().Multiply(wall.Width/2 + epsilon);
							XYZ wallrightSide = wallCenter + line.Direction.CrossProduct(new XYZ(0,0,-1)).Normalize().Multiply(wall.Width/2 + epsilon);

							
							//Create a ReferenceIntersector on the current wall and set up its parameters
							ReferenceIntersector refIntersector = new ReferenceIntersector(collectionids, FindReferenceTarget.Face, view3D);
							
							ReferenceArray ra = new ReferenceArray();
							
							//Find a direction along the wall
							XYZ wallDirection = line.Direction;
							
							double rayLenght = line.Length/2 + wall.Width;
							
							//Add first found references to a reference array
							AppendReference(wallCenter,wallDirection,refIntersector,ref ra,rayLenght);
							AppendReference(wallLeftSide,wallDirection,refIntersector,ref ra,rayLenght);
							AppendReference(wallrightSide,wallDirection,refIntersector,ref ra,rayLenght);
							
							
							//Change direction
							wallDirection = -wallDirection;
							//Add second found references to a reference array
							AppendReference(wallCenter,wallDirection,refIntersector,ref ra,rayLenght);
							AppendReference(wallLeftSide,wallDirection,refIntersector,ref ra,rayLenght);
							AppendReference(wallrightSide,wallDirection,refIntersector,ref ra,rayLenght);
							
							
							//Create the dimension in the levation
							Dimension dim =CreateDimensionElement(ra,line,_doc.ActiveView, wall.Width);
							
							if (dim == null)
							{
								//The dimension is not valid, print the wall
							}
							
							//Apply a tag to the wall
							double minLenght = 0.5 * 3.28084;
							
							if (line.ApproximateLength > minLenght)
							{
								IndependentTag wallTag = AddTagToWall(wall, line);
							}
							
							
						}
						else
						{
							//the wall is not strait
							//Print it to the error log
						}
					}
				}
				
				
				
				tx.Commit();
			}
			

		}
		
		private Dimension CreateDimensionElement(ReferenceArray ra, Line line, View currentView, double wallWidth)
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//define an extension value (m)
			double extention = 0.05 * 3.28084;
			XYZ offset = line.Direction.CrossProduct(new XYZ(0,0,1)).Normalize() * (extention +wallWidth/2) ;
			
			if (offset.Y < 0)
			{
				offset = new XYZ(offset.X,-offset.Y,offset.Z);
			}
			
			if (offset.X > 0)
			{
				offset = new XYZ(-offset.X,offset.Y,offset.Z);
			}
			
			Line baseline = Line.CreateBound(line.GetEndPoint(0) + offset,line.GetEndPoint(1) + offset);
			
			if (ra.Size > 1)
			{
				Dimension dim = doc.Create.NewDimension(currentView,baseline,ra);
				
				return dim;
			}
			else
			{
				return null;
			}
			

		}
		
		private void AppendReference(XYZ wallCenter, XYZ wallDirection, ReferenceIntersector refIntersector, ref ReferenceArray ra, double rayLenght)
		{

			IList<ReferenceWithContext> referencesWithContext = refIntersector.Find(wallCenter, wallDirection);

			double eps = 0.01 * 3.28084;
			bool isAppend = true;
			
			foreach (ReferenceWithContext rfWithCont in referencesWithContext)
			{
				
				if (rfWithCont.Proximity < rayLenght) //limit the dimension to the lenght of the wall
				{
					foreach (Reference rf in ra) {
						
						XYZ distance = rf.GlobalPoint-rfWithCont.GetReference().GlobalPoint;

						if (Math.Abs(distance.DotProduct(wallDirection)) < eps) //Check for 0 lenght dimension
						{
							isAppend = false;
							break;
						}
					}
					
					if ( isAppend == true)
					{
						ra.Append(rfWithCont.GetReference());
					}
				}
			}
		}
		
		private IndependentTag AddTagToWall(Wall wall, Line axisLine)
		{
			XYZ wallCenterPoint = (axisLine.GetEndPoint(1) + axisLine.GetEndPoint(0))/2;
			
			//define an extension value (m)
			double extention = 0.05 * 3.28084;
			
			XYZ offset = axisLine.Direction.CrossProduct(new XYZ(0,0,1)).Normalize() * (extention +wall.Width/2) ;
			
			if (offset.Y > 0)
			{
				offset = new XYZ(offset.X,-offset.Y,offset.Z);
			}
			
			if (offset.X < 0)
			{
				offset = new XYZ(-offset.X,offset.Y,offset.Z);
			}
			
			return _doc.Create.NewTag(_doc.ActiveView,wall,false, TagMode.TM_ADDBY_CATEGORY, TagOrientation.Horizontal, wallCenterPoint + offset);
		}
		
		private ViewSection CreateWallElevation(Line line, Wall wall)
		{

			//define an extension value (m)
			double extention = 0.2 * 3.28084;
			
			//Get wall elevation
			Parameter heightParam = wall.get_Parameter(BuiltInParameter.WALL_USER_HEIGHT_PARAM);
			double height = heightParam.AsDouble() + extention*3;
			
			// Find a section view type
			IEnumerable<ViewFamilyType> viewFamilyTypes = from elem in new FilteredElementCollector(_doc).OfClass(typeof(ViewFamilyType))
				let type = elem as ViewFamilyType
				where type.ViewFamily == ViewFamily.Section
				select type;
			ElementId sectionViewTypeId = viewFamilyTypes.First().Id;
			
			
			double extend = line.Length/2 + extention*2;
			//Create the bounding box
			//The transformation orient the bounding box
			Transform transform = Transform.Identity;
			
			Transform curveTransform = line.ComputeDerivatives(0.5, true);
			XYZ origin = curveTransform.Origin;
			XYZ viewDirection = curveTransform.BasisX.Normalize(); // tangent vector along the location curve
			XYZ normal = viewDirection.CrossProduct(XYZ.BasisZ).Normalize(); // location curve normal @ mid-point

			transform.Origin = origin;
			transform.BasisX = XYZ.BasisZ.CrossProduct(normal);
			transform.BasisY = XYZ.BasisZ;
			transform.BasisZ = normal;
			
			BoundingBoxXYZ sectionBox = new BoundingBoxXYZ();
			
			sectionBox.Transform = transform;
			sectionBox.Min = new XYZ(-extend,-extention*3,-extention);
			sectionBox.Max = new XYZ(extend,height,extention);
			
			ViewSection wallElevation = ViewSection.CreateSection(_doc,sectionViewTypeId,sectionBox);
			//wallElevation.ApplyViewTemplateParameters(_sectionViewTemplate);
			
			Parameter annotationCrop = wallElevation.get_Parameter(BuiltInParameter.VIEWER_ANNOTATION_CROP_ACTIVE);
			annotationCrop.Set(1);
			Parameter cropRegionVisible = wallElevation.get_Parameter(BuiltInParameter.VIEWER_CROP_REGION_VISIBLE);
			cropRegionVisible.Set(1);
			
			return wallElevation;
		}
		public void WallExtension()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			
			WallExtension test = new WallExtension();
			test.WallExtensionTest(doc);
		}
		
		public void TagElement()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			using (Transaction ts = new Transaction(doc,"toto"))
			{
				ts.Start("coucou");
				
				foreach (Document d in this.Application.Documents) {
					if (d.PathName.Contains("Projet1"))
					{
						Document _doc = d;
						
						// Retrieve the family if it is already present:
						IEnumerable<Wall> walls = from elem in new FilteredElementCollector(_doc).OfCategory(BuiltInCategory.OST_Walls)
							let type = elem as Wall
							select type;
						
						// define tag mode and tag orientation for new tag
						TagMode tagMode = TagMode.TM_ADDBY_CATEGORY;
						TagOrientation tagorn = TagOrientation.Horizontal;

						foreach (Wall bw in walls) {

							if (bw != null)
							{
								// Add the tag to the middle of the wall
								LocationCurve wallLoc = bw.Location as LocationCurve;
								XYZ wallStart = wallLoc.Curve.GetEndPoint(0);
								XYZ wallEnd = wallLoc.Curve.GetEndPoint(1);
								XYZ wallMid = wallLoc.Curve.Evaluate(0.5, true);
								
								IndependentTag planTag = _doc.Create.NewTag(doc.ActiveView, bw, true, tagMode, tagorn, wallMid);
								if (null == planTag)
								{
									throw new Exception("Create IndependentTag Failed.");
								}
							}
						}
					}
				}
				
				ts.Commit();
			}
		}
		
		public void WallLegend()
		{
			_doc = this.ActiveUIDocument.Document;
			
			//Retrive elements in legend
			FilteredElementCollector collector = new FilteredElementCollector(_doc,_doc.ActiveView.Id);
			List<Element> elements = collector.ToElements().ToList();

			XYZ origin = new XYZ(0,0,0);
			XYZ baseVec= new XYZ(0,0,0);
			XYZ upVec= new XYZ(0,0,0);
			
			using (Transaction tx = new Transaction(_doc))
			{
				tx.Start("Legend");
				
				foreach (Element element in elements) {
					
					if (element.Category.Name == "Composants de légende")
					{
						Parameter param = element.GetParameters("Type de composant").FirstOrDefault();
						string strText = param.AsValueString();
						double width = 2;
						//Only available for Revit 2016
//						TextNoteOptions opts = new TextNoteOptions();
//						opts.HorizontalAlignment = HorizontalTextAlignment.Left;
//						TextNote text = TextNote.Create(_doc,_doc.ActiveView.Id, origin,width, strText,opts);
						
					}

				}
				
				tx.Commit();
			}
		}
		
		public void WallFinichTag()
		{
			_doc = this.ActiveUIDocument.Document;

			
			//Retrive walls
			FilteredElementCollector collector = new FilteredElementCollector(_doc,_doc.ActiveView.Id);
			List<Element> walls = collector.OfCategory(BuiltInCategory.OST_Walls).ToElements().ToList();
			ICollection<ElementId> collectionids = collector.OfCategory(BuiltInCategory.OST_Walls).ToElementIds();
			
			//Retrive line styles
			collector = new FilteredElementCollector(_doc);
			List<Element> lines = collector.OfClass(typeof(GraphicsStyle)).ToElements().ToList();
			
			using (Transaction tx = new Transaction(_doc))
			{
				tx.Start("Detail Wall Finishes");
				
				foreach (Element elem in walls) {
					
					Wall wall = elem as Wall;
					bool isFlipped = wall.Flipped;
					int finishSide = 0;
					
					if (isFlipped)
					{
						finishSide = -1;
					}
					else
					{
						finishSide = 1;
					}
					
					LocationCurve locationCurve = wall.Location as LocationCurve;
					
					WallType wallType = wall.WallType;

					CompoundStructure cs = wallType.GetCompoundStructure();
					
					IList<CompoundStructureLayer> layers = cs.GetLayers();
					
					
					Line line = locationCurve.Curve as Line;
					Transform curveTransform = line.ComputeDerivatives(0.5, true);
					XYZ viewDirection = curveTransform.BasisX.Normalize(); // tangent vector along the location curve
					XYZ normal = wallType.Width * viewDirection.CrossProduct(XYZ.BasisZ).Normalize(); // location curve normal @ mid-point
					
					

					int layerIndex = 0; // cs.GetNumberOfShellLayers(ShellLayerType.Interior);
					
					if (cs.GetLayerFunction(layerIndex) == MaterialFunctionAssignment.Finish1 || cs.GetLayerFunction(layerIndex) == MaterialFunctionAssignment.Finish2)
					{
						Line geomLine = Line.CreateBound(locationCurve.Curve.GetEndPoint(0) - normal*finishSide, locationCurve.Curve.GetEndPoint(1) - normal*finishSide);
						DetailLine detailLine = _doc.Create.NewDetailCurve(  _doc.ActiveView, geomLine ) as DetailLine;
						//detailLine.LineStyle =
					}
					
					layerIndex = layers.Count -1; // cs.GetNumberOfShellLayers(ShellLayerType.Interior);
					
					if (cs.GetLayerFunction(layerIndex) == MaterialFunctionAssignment.Finish1 || cs.GetLayerFunction(layerIndex) == MaterialFunctionAssignment.Finish2)
					{
						Line geomLine = Line.CreateBound(locationCurve.Curve.GetEndPoint(0) + normal*finishSide, locationCurve.Curve.GetEndPoint(1) + normal*finishSide);
						DetailLine detailLine = _doc.Create.NewDetailCurve(  _doc.ActiveView, geomLine ) as DetailLine;
					}
					
					
					
					
				}
				
				tx.Commit();
			}
		}
		public void CreateWall()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//Create a filter on the entire document
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			
			//Use this filter to retrieve all levels
			ICollection<Element> levelCollection = collector.OfClass(typeof(Level)).ToElements();
			
			//Pick up the first level
			Element level = levelCollection.FirstOrDefault();
			
			//Get its Id
			ElementId levelId = level.Id;
			
			using (Transaction tx = new Transaction(doc))
			{
				tx.Start("Create Walls");
				
				for (int i = 0;i<10;i++)
				{
					double delta = 2*3.28084;
					// Build a location line for the wall creation
					XYZ start = new XYZ(0, delta*i, 0);
					XYZ end = new XYZ(10*3.28084,delta*i, 0);
					Line line = Line.CreateBound(start, end);
					
					// Create a wall using the location line
					Wall.Create(doc, line, levelId, false);
				}

				
				tx.Commit();
			}

		}
		public void TagWalls()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//Create a filter on the current view
			FilteredElementCollector collector = new FilteredElementCollector(doc, doc.ActiveView.Id);
			
			//Use this filter to retrieve all wall in the current view
			List<Wall> walls = collector.OfCategory(BuiltInCategory.OST_Walls).Cast<Wall>().ToList();
			
			//Define tag mode and tag orientation for new tag
			TagMode tagMode = TagMode.TM_ADDBY_CATEGORY;
			TagOrientation tagorn = TagOrientation.Horizontal;
			
			//Open a transaction
			using (Transaction tx = new Transaction(doc))
			{
				tx.Start("Tag Walls");
				
				//Loop on all visible walls
				foreach (Wall wall in walls) {

					if (wall != null)
					{
						// Add the tag to the middle of the wall
						LocationCurve wallLoc = wall.Location as LocationCurve;
						XYZ wallMid = wallLoc.Curve.Evaluate(0.5, true);
						
						//Define the final position of the tag
						XYZ wallDirection = (wallLoc.Curve.GetEndPoint(1) - wallLoc.Curve.GetEndPoint(0)).Normalize();
						wallMid = wallLoc.Curve.GetEndPoint(1) + 0.4*3.28084*wallDirection.CrossProduct(new XYZ(0,0,-1));
						
						IndependentTag tag = doc.Create.NewTag(doc.ActiveView, wall, false, tagMode, tagorn, wallMid);
					}
				}

				tx.Commit();
			}
			
		}
	}
}