﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: moreaus
 * Date: 15/04/2014
 * Heure: 16:52
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;

namespace WallElevation
{
	/// <summary>
	/// Description of WallExtension.
	/// </summary>
	public class WallExtension
	{
		public void WallExtensionTest(Document doc)
		{
			
			// Find a 3D view to use for the ReferenceIntersector constructor
			FilteredElementCollector viewcollector = new FilteredElementCollector(doc);
			Func<View3D, bool> isNotTemplate = v3 => !(v3.IsTemplate);
			View3D view3D = viewcollector.OfClass(typeof(View3D)).Cast<View3D>().First<View3D>(isNotTemplate);
			
			//Retrive walls
			FilteredElementCollector collector = new FilteredElementCollector(doc,doc.ActiveView.Id);
			List<Element> walls = collector.OfCategory(BuiltInCategory.OST_Walls).ToElements().ToList();
			ICollection<ElementId> collectionids = collector.OfCategory(BuiltInCategory.OST_Walls).ToElementIds();

			
			//Create a ReferenceIntersector and set up its parameters
			ReferenceIntersector refIntersector = new ReferenceIntersector(collectionids, FindReferenceTarget.Face, view3D);
			refIntersector.FindReferencesInRevitLinks = true;
			
			//Tolerance
			double rayLenght = 100 * 3.28084;
			
			foreach (Element e in walls)
			{
				Wall wall = e as Wall;
				
				if (wall != null)
				{
					// obtain location curve and check that it is straight:
					LocationCurve lc = wall.Location as LocationCurve;

					Curve curve = lc.Curve;
					Line line = curve as Line;
					
					if( null != line )
					{
						XYZ wallDirection = line.Direction;
						XYZ wallEnd = line.GetEndPoint(0);
						
						ReferenceWithContext referencesWithContext = refIntersector.FindNearest(wallEnd, wallDirection);
						
						if (referencesWithContext != null)
						{
							if (referencesWithContext.Proximity < rayLenght) //limit the dimension of the ray
							{
								//string test  = "toto";
								
								if (referencesWithContext.GetReference().LinkedElementId != ElementId.InvalidElementId)
								{
									//string test2  = "toto";
								}
							}
						}
					}
				}
			}
			
		}
	}
}
