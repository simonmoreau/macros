﻿/*
 * Created by SharpDevelop.
 * User: moreaus
 * Date: 11/04/2016
 * Time: 17:27
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace ClimaDIM
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("60FB8AF7-D531-4CCF-AA5A-CC723F7B1989")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion

		public void RawCreateProjectParameter()
		{
			//InternalDefinition def = new InternalDefinition();
			//Definition def = new Definition();

		}
		public void CreateFicheLocauxProjet()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			ViewSchedule ficheBaseKeySchedule = doc.GetElement(new Autodesk.Revit.DB.ElementId(834128)) as ViewSchedule;
			ViewSchedule ficheProjetKeySchedule = doc.GetElement(new Autodesk.Revit.DB.ElementId(835445)) as ViewSchedule;
			
			List<Element> ficheBaseRows = new FilteredElementCollector(doc, ficheBaseKeySchedule.Id).ToElements().ToList();
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Create rows");
				
				foreach (Element element in ficheBaseRows) {
					
					Parameter CD_FicheBase_EstFicheProjet = element.GetParameters("CD_FicheBase_EstFicheProjet").FirstOrDefault();
					
					if (CD_FicheBase_EstFicheProjet.AsInteger() == 1)
					{
						Parameter key = element.GetParameters("Nom de la clé").FirstOrDefault();
						string text = key.AsString();
						
						TableData colTableData = ficheProjetKeySchedule.GetTableData();
						
						TableSectionData sectionData = colTableData.GetSectionData(SectionType.Body);
						
						sectionData.InsertRow(sectionData.NumberOfRows);
						
						
					}
				}
				
				tx.Commit();
			}

		}
		public void ExportParam()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			ViewSchedule ficheBaseKeySchedule = doc.GetElement(new Autodesk.Revit.DB.ElementId(834128)) as ViewSchedule;
			ViewSchedule ficheProjetKeySchedule = doc.GetElement(new Autodesk.Revit.DB.ElementId(835445)) as ViewSchedule;
			
			List<Element> ficheBaseRows = new FilteredElementCollector(doc, ficheBaseKeySchedule.Id).ToElements().ToList();
			
			List<string> lines = new List<string>();
			
			foreach (Parameter param in ficheBaseRows.FirstOrDefault().Parameters) {
				
				string line = param.Definition.Name+ ";" + param.Definition.ParameterGroup + ";" + param.Definition.UnitType;
				lines.Add(line);
			}
			
			string path = @"C:\Google Drive\01 - Ingerop\04 - R&D\Revit\ClimaDIM\FicheBaseParam.csv";
			File.WriteAllLines(path,lines.ToArray(),Encoding.UTF8);
			
		}
	}
}