/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 24/06/2015
 * Time: 19:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Mechanical;
using Autodesk.Revit.DB.Plumbing;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Globalization;

namespace MEP
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    [Autodesk.Revit.DB.Macros.AddInId("AAADB2EC-17CB-4DB8-9751-FE495DE24EBC")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		
				public void SystemCreation()
		{
			
			Document doc = this.ActiveUIDocument.Document;
			
			List<string> logFile = new List<string>();
			
			
			//Retrive all MEPSystemClassification
			Dictionary<string,MEPSystemClassification> systemTypes = new Dictionary<string, MEPSystemClassification>();
			
			var types = Enum.GetValues(typeof(MEPSystemClassification)); //.Cast<PipingSystemType>().ToArray();
			
			foreach (MEPSystemClassification systemTypeEnum in types ) {
				
				systemTypes.Add(systemTypeEnum.ToString(),systemTypeEnum);
			}
			
			
			//Retrive all material
			Dictionary<string,ElementId> materialsDico = new Dictionary<string, ElementId>();
			FilteredElementCollector elementCollector = new FilteredElementCollector(doc);
			elementCollector.WherePasses(new ElementClassFilter(typeof(Material)));
			IList<Element> materials = elementCollector.ToElements();
			
			foreach (Element materialElement in materials)
			{
				Material material = materialElement as Material;
				materialsDico.Add(material.Name,material.Id);
			}
			
			//Retrive existing systems
			Dictionary<string,MEPSystemType> systemsTypesDico = new Dictionary<string, MEPSystemType>();
			elementCollector = new FilteredElementCollector(doc);
			elementCollector.WherePasses(new ElementClassFilter(typeof(MEPSystemType)));
			IList<Element> systemsTypes = elementCollector.ToElements();
			
			foreach (Element systemTypeElement in systemsTypes)
			{
				MEPSystemType systemType = systemTypeElement as MEPSystemType;
				systemsTypesDico.Add(systemType.Name,systemType);
			}
			
			string[] systemLines = File.ReadAllLines(@"C:\Affaires\Revit\CouleurSYS\Systems.txt",Encoding.Default);
			
			using (Transaction tx = new Transaction(doc)) {
				tx.Start("Create Systems");
				
				foreach (String line in systemLines) {
					
					string[] systemParams = line.Split(';');
					
					PipingSystemType newSystemType = PipingSystemType.Create(doc, systemTypes[systemParams[1]],systemParams[0]);
					
					newSystemType.Abbreviation = systemParams[2];
					
					newSystemType.LineColor = new Color(Convert.ToByte(systemParams[3]),Convert.ToByte(systemParams[4]),Convert.ToByte(systemParams[5]));
					
					if (materialsDico.ContainsKey(systemParams[6]))
					{
						newSystemType.MaterialId = materialsDico[systemParams[6]];
					}
					else
					{
						logFile.Add(string.Format("Le matériaux {0} n'existe pas dans le modèle",systemParams[6]));
					}
					
					if (FluidType.GetFluidType(doc,systemParams[7]) != null)
					{
						newSystemType.FluidType = FluidType.GetFluidType(doc,systemParams[7]).Id;
					}
					else
					{
						logFile.Add(string.Format("Le fluide {0} n'existe pas dans le modèle",systemParams[7]));
					}
					
					NumberFormatInfo provider = new NumberFormatInfo();
					provider.NumberDecimalSeparator = ",";
					double temp = Convert.ToDouble(systemParams[8],provider);
					newSystemType.FluidTemperature = temp + 273.15;
					
				}
				
				tx.Commit();
				
				File.WriteAllLines(@"C:\Affaires\Revit\CouleurSYS\LogSystems.txt",logFile.ToArray());
			}
		}
				
		public void RenameSystem()
		{
			
			Document doc = this.ActiveUIDocument.Document;
			
			//Retrive all material
			Dictionary<string,ElementId> materialsDico = new Dictionary<string, ElementId>();
			FilteredElementCollector elementCollector = new FilteredElementCollector(doc);
			elementCollector.WherePasses(new ElementClassFilter(typeof(PipingSystemType)));
			IList<Element> systemElems = elementCollector.ToElements();
			
			using (Transaction tx = new Transaction(doc)) {
				tx.Start("Create Systems");
				
				foreach (Element systemElem in systemElems)
				{
					PipingSystemType system = systemElem as PipingSystemType;
					system.Name = "zzz_" + system.Name;
				}
				
				tx.Commit();
			}
			
			
		}
		
		public void ExportMEPSystemType()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//Retrive all MEPSystemType
			FilteredElementCollector elementCollector = new FilteredElementCollector(doc);
			elementCollector.WherePasses(new ElementClassFilter(typeof(MEPSystemType)));
			IList<Element> systemElems = elementCollector.ToElements();
			
			//Export list
			List<string> MEPSystemTypeList = new List<string>();
			
			foreach (Element element in systemElems) {
				
				MEPSystemType systemType = element as MEPSystemType;
				MEPSystemTypeList.Add(systemType.Name + ";" + ";" + systemType.Abbreviation+systemType.SystemClassification.ToString());
			}
			
			File.WriteAllLines(@"C:\Affaires\Revit\test.txt",MEPSystemTypeList.ToArray(),Encoding.Default);
		}
				
		public void ImportMEPSystemType()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//Retrive all MEPSystemType
			FilteredElementCollector elementCollector = new FilteredElementCollector(doc);
			elementCollector.WherePasses(new ElementClassFilter(typeof(MEPSystemType)));
			IList<Element> systemElems = elementCollector.ToElements();
			
			
			Dictionary<string,MEPSystemType> MEPSystemTypeDico = new Dictionary<string, MEPSystemType>();
			
			foreach (Element element in systemElems) {
				
				MEPSystemType systemType = element as MEPSystemType;
				MEPSystemTypeDico.Add(systemType.Name,systemType);
			}
			
			//Retrive all MEPSystemClassification
			Dictionary<string,MEPSystemClassification> systemClassifications = new Dictionary<string, MEPSystemClassification>();
			
			var types = Enum.GetValues(typeof(MEPSystemClassification)); //.Cast<PipingSystemType>().ToArray();
			
			foreach (MEPSystemClassification systemTypeEnum in types ) {
				
				systemClassifications.Add(systemTypeEnum.ToString(),systemTypeEnum);
			}
			
			//Import list
			string[] systemsTypes = File.ReadAllLines(@"C:\Affaires\Revit\test.txt",Encoding.Default);
			
			foreach (string line in systemsTypes) {
				
				string[] systemsProperties = line.Split(';');
				
				//if the system already exist
				if (MEPSystemTypeDico.ContainsKey(systemsProperties[0]))
				{
					MEPSystemType editSystemType = MEPSystemTypeDico[systemsProperties[0]];
					editSystemType.Name = systemsProperties[0];
					editSystemType.Abbreviation = systemsProperties[2];
				}
				else
				{
					MEPSystemType newSystemType = PipingSystemType.Create(doc,systemClassifications[systemsProperties[3]],systemsProperties[0]);
				}
			}
		}
		
		public void GetPipeSegmentSizesFromDocument()
		{
			
			Document document = this.ActiveUIDocument.Document;
			FilteredElementCollector collectorPipeType = new FilteredElementCollector(document);
			collectorPipeType.OfClass(typeof(Segment));

			IEnumerable<Segment> segments = collectorPipeType.ToElements().Cast<Segment>();
			foreach (Segment segment in segments)
			{
				StringBuilder strPipeInfo = new StringBuilder();
				strPipeInfo.AppendLine("Segment: " + segment.Name);

				strPipeInfo.AppendLine("Roughness: " + segment.Roughness);

				strPipeInfo.AppendLine("Pipe Sizes:");
				double dLengthFac = 304.8;  // used to convert stored units from ft to mm for display
				foreach (MEPSize size in segment.GetSizes())
				{
					strPipeInfo.AppendLine(string.Format("Nominal: {0:F3}, ID: {1:F3}, OD: {2:F3}",
					                                     size.NominalDiameter * dLengthFac, size.InnerDiameter * dLengthFac, size.OuterDiameter * dLengthFac));
				}


				TaskDialog.Show("PipeSetting Data", strPipeInfo.ToString());
				break;
			}
		}
		
		public void CreatePipeSegment()
		{
			Document doc = this.ActiveUIDocument.Document;
			List<string> logFile = new List<string>();
			
//			//Get a PipeScheduleType from name
//			FilteredElementCollector collectorPipeType = new FilteredElementCollector(doc);
//			collectorPipeType.OfClass(typeof(PipeScheduleType));
//			Element PSType = collectorPipeType.First();
			
			//Retrive all PipeScheduleType
			Dictionary<string, ElementId> PipeScheduleTypesDico = new Dictionary<string, ElementId>();
			FilteredElementCollector elementCollector = new FilteredElementCollector(doc);
			elementCollector.WherePasses(new ElementClassFilter(typeof(PipeScheduleType)));
			IList<Element> PipeScheduleTypes = elementCollector.ToElements();

			foreach (Element PipeScheduleType in PipeScheduleTypes)
			{
				PipeScheduleTypesDico.Add(PipeScheduleType.Name, PipeScheduleType.Id);
			}

			//Retrive all material
			Dictionary<string,ElementId> materialsDico = new Dictionary<string, ElementId>();
			elementCollector = new FilteredElementCollector(doc);
			elementCollector.WherePasses(new ElementClassFilter(typeof(Material)));
			IList<Element> materials = elementCollector.ToElements();
			
			foreach (Element materialElement in materials)
			{
				Material material = materialElement as Material;
				materialsDico.Add(material.Name,material.Id);
			}
			
			
			//Read the text file and store the values
			string[] systemLines = File.ReadAllLines(@"C:\Affaires\Revit\CouleurSYS\Segments.txt",Encoding.Default);
			
			PipingSegment currentSegment = new PipingSegment();
			List<PipingSegment> segmentList = new List<PipingSegment>();
			
			foreach (String line in systemLines) {

				string[] properties = line.Split(';');
				
				if (properties[0] != "")
				{
					if (currentSegment.Material != null)
					{
						segmentList.Add(currentSegment);
					}
					
					currentSegment = new PipingSegment(properties[0],properties[1]);
					currentSegment.AddSize(Convert.ToDouble(properties[2]),Convert.ToDouble(properties[3]),Convert.ToDouble(properties[4]));
				}
				else
				{
					currentSegment.AddSize(Convert.ToDouble(properties[2]),Convert.ToDouble(properties[3]),Convert.ToDouble(properties[4]));
				}
			}
			
			segmentList.Add(currentSegment);
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Create Pipe Segments");
				
				foreach (PipingSegment SegmentToCreate in segmentList) {
					
					if (materialsDico.ContainsKey(SegmentToCreate.Material))
					{
						PipeSegment.Create(doc,materialsDico[SegmentToCreate.Material],PipeScheduleTypesDico[SegmentToCreate.ScheduleType],SegmentToCreate.Sizes);
					}
					else
					{
						logFile.Add(string.Format("Le matériaux {0} n'existe pas dans le modèle",SegmentToCreate.Material));
					}
				}
				tx.Commit();
			}
			
			
			File.WriteAllLines(@"C:\Affaires\Revit\CouleurSYS\LogSegments.txt",logFile.ToArray());
		}

	}
	
		public class PipingSegment
	{
		private List<MEPSize> _sizes;
		private string _material;
		private string _scheduleType;
		
		public PipingSegment()
		{
			
		}
		
		public PipingSegment(string Material, string ScheduleType )
		{
			_sizes = new List<MEPSize>();
			_material = Material;
			_scheduleType = ScheduleType;
		}
		
		public string Material
		{
			get {return _material;}
			set {value = _material;}
		}
		
		public string ScheduleType
		{
			get {return _scheduleType;}
			set {value = _scheduleType;}
		}
		
		public List<MEPSize> Sizes
		{
			get {return _sizes;}
		}
		
		public void AddSize(double DN, double Dint, double Dext)
		{
			_sizes.Add(new MEPSize(DN,Dint,Dext,true,true));
		}
	}
}