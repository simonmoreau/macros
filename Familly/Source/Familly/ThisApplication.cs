/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 09/04/2016
 * Time: 10:39
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Crea = Autodesk.Revit.Creation;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Familly
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("51FF5C1E-339D-4947-B976-12238D999AE4")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}
		
		
		private Autodesk.Revit.ApplicationServices.Application revit;
		private Document familyDocument;
		private Autodesk.Revit.Creation.FamilyItemFactory creationFamily;
		
		public void BatchCreateProjects()
		{
			revit = this.Application;
			
			string familiesDirectory = @"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Tests";
			string projectsDirectory = @"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Test Projects";
			SaveAsOptions saveAsOpt = new SaveAsOptions();
			saveAsOpt.MaximumBackups = 1;
			saveAsOpt.Compact = true;
			
			foreach (string familyPath in Directory.GetFiles(familiesDirectory))
			{
				Document doc = revit.NewProjectDocument(UnitSystem.Metric);
				Family family;
				
				using (Transaction tx = new Transaction(doc)) {
					
					tx.Start("Load Family");
					doc.LoadFamily(familyPath,out family);
					tx.Commit();
				}
				
				string projectName = "0000_" + Path.GetFileNameWithoutExtension(familyPath) + ".rvt";
				string projectPath = Path.Combine(projectsDirectory,projectName);
				
				doc.SaveAs(projectPath,saveAsOpt);
				
				using (Transaction tx = new Transaction(doc)) {
					
					tx.Start("Place One family");
					
					FilteredElementCollector collector = new FilteredElementCollector(doc);
					Level level = collector.OfCategory(BuiltInCategory.OST_Levels).ToElements().FirstOrDefault() as Level;
					
					FamilySymbol symbol = doc.GetElement(family.GetFamilySymbolIds().FirstOrDefault()) as FamilySymbol;
					
					if (!symbol.IsActive) symbol.Activate();
					
					doc.Create.NewFamilyInstance(new XYZ(0,0,0),symbol,level, StructuralType.NonStructural);
					
					tx.Commit();
				}
				
				projectName = "0001_" + Path.GetFileNameWithoutExtension(familyPath) + ".rvt";
				projectPath = Path.Combine(projectsDirectory,projectName);
				
				doc.SaveAs(projectPath,saveAsOpt);
				
				using (Transaction tx = new Transaction(doc)) {
					
					tx.Start("Place 1000 families");
					
					FilteredElementCollector collector = new FilteredElementCollector(doc);
					Level level = collector.OfCategory(BuiltInCategory.OST_Levels).ToElements().FirstOrDefault() as Level;
					
					FamilySymbol symbol = doc.GetElement(family.GetFamilySymbolIds().FirstOrDefault()) as FamilySymbol;
					if (!symbol.IsActive) symbol.Activate();
					
					List<Crea.FamilyInstanceCreationData> creationData = new List<Crea.FamilyInstanceCreationData>();
					
					for (int i = 0; i < 10; i++) {
						for (int j = 0; j < 10; j++) {
							for (int k = 0; k < 10; k++) {
								if (i== 0 && j==0 && k==0)
								{
									
								}
								else
								{
									creationData.Add(new Crea.FamilyInstanceCreationData(new XYZ(i*3.5,j*3.5,k*3.5),symbol,level, StructuralType.NonStructural));
								}
							}
						}
					}
					
					doc.Create.NewFamilyInstances2(creationData);
					
					tx.Commit();
				}
				
				projectName = "1000_" + Path.GetFileNameWithoutExtension(familyPath) + ".rvt";
				projectPath = Path.Combine(projectsDirectory,projectName);
				
				doc.SaveAs(projectPath,saveAsOpt);
				
				doc.Close();
			}
			
			
			
		}
		
		public void BatchCreateFamillies()
		{
			revit = this.Application;
			familyDocument = revit.NewFamilyDocument(@"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Support\Generic Model.rft");
			
			if (null != familyDocument)
			{
				for (int i = 0; i < 100; i++) {
					
					using (Transaction transaction = new Transaction(familyDocument, "CreateGenericModel"))
					{
						transaction.Start();
						
						creationFamily = familyDocument.FamilyCreate;
						double rank = Convert.ToDouble(i);
						double size = 1/(rank+1);
						
						CurveArrArray profile = CreateRectangularProfile(size);
						CreateExtrusion(i,profile);
						
						transaction.Commit();
					}

					string path = @"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Tests\Extrusion_Cube_"+ i.ToString("D3") + ".rfa";
					
					familyDocument.SaveAs(path);
					
				}
				
				familyDocument.Close(false);
				familyDocument = revit.NewFamilyDocument(@"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Support\Generic Model.rft");
				
				for (int i = 0; i < 100; i++) {
					
					using (Transaction transaction = new Transaction(familyDocument, "CreateGenericModel"))
					{
						transaction.Start();
						
						creationFamily = familyDocument.FamilyCreate;
						double rank = Convert.ToDouble(i);
						double size = 1/(rank+1);
						
						CurveArrArray profile = CreateCircularProfile(size);
						CreateExtrusion(i,profile);
						
						transaction.Commit();
					}

					string path = @"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Tests\Extrusion_Cylinder_"+ i.ToString("D3") + ".rfa";
					
					familyDocument.SaveAs(path);
					
				}
				familyDocument.Close(false);
				familyDocument = revit.NewFamilyDocument(@"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Support\Generic Model.rft");
				
				
				for (int i = 0; i < 100; i++) {
					
					using (Transaction transaction = new Transaction(familyDocument, "CreateGenericModel"))
					{
						transaction.Start();
						
						creationFamily = familyDocument.FamilyCreate;
						double rank = Convert.ToDouble(i);
						double size = 1/(rank+1);
						
						CurveArrArray profile = CreateTriangularProfile(size);
						CreateExtrusion(i,profile);
						
						transaction.Commit();
					}

					string path = @"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Tests\Extrusion_Prism_"+ i.ToString("D3") + ".rfa";
					
					familyDocument.SaveAs(path);
					
				}
				
				familyDocument.Close(false);
				familyDocument = revit.NewFamilyDocument(@"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Support\Generic Model.rft");
				
				
				for (int i = 0; i < 100; i++) {
					
					using (Transaction transaction = new Transaction(familyDocument, "CreateGenericModel"))
					{
						transaction.Start();
						
						creationFamily = familyDocument.FamilyCreate;
						double rank = Convert.ToDouble(i);
						double size = 1/(rank+1);
						
						CreateSphereByRevolution(i);
						
						transaction.Commit();
					}

					string path = @"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Tests\Revolution_Sphere_"+ i.ToString("D3") + ".rfa";
					
					familyDocument.SaveAs(path);
					
				}
				
				familyDocument.Close(false);
				familyDocument = revit.NewFamilyDocument(@"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Support\Generic Model.rft");
				
				
				for (int i = 0; i < 100; i++) {
					
					using (Transaction transaction = new Transaction(familyDocument, "CreateGenericModel"))
					{
						transaction.Start();
						
						creationFamily = familyDocument.FamilyCreate;
						double rank = Convert.ToDouble(i);
						double size = 1/(rank+1);
						
						CreateCylindarByRevolution(i);
						
						transaction.Commit();
					}

					string path = @"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Tests\Revolution_Cylinder_"+ i.ToString("D3") + ".rfa";
					
					familyDocument.SaveAs(path);
					
				}
				
				familyDocument.Close(false);
				familyDocument = revit.NewFamilyDocument(@"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Support\Generic Model.rft");
				
				
				for (int i = 0; i < 100; i++) {
					
					using (Transaction transaction = new Transaction(familyDocument, "CreateGenericModel"))
					{
						transaction.Start();
						
						creationFamily = familyDocument.FamilyCreate;
						double rank = Convert.ToDouble(i);
						double size = 1/(rank+1);
						
						SweepProfile profile = CreateRectangularSweepProfile(size);
						CreateSweep(i,profile);
						
						transaction.Commit();
					}

					string path = @"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Tests\Sweep_Cube_"+ i.ToString("D3") + ".rfa";
					
					familyDocument.SaveAs(path);
					
				}
				
				familyDocument.Close(false);
				familyDocument = revit.NewFamilyDocument(@"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Support\Generic Model.rft");
				
				
				for (int i = 0; i < 100; i++) {
					
					using (Transaction transaction = new Transaction(familyDocument, "CreateGenericModel"))
					{
						transaction.Start();
						
						creationFamily = familyDocument.FamilyCreate;
						double rank = Convert.ToDouble(i);
						double size = 1/(rank+1);
						
						SweepProfile profile = CreateCircularSweepProfile(size);
						CreateSweep(i,profile);
						
						transaction.Commit();
					}

					string path = @"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Tests\Sweep_Cylinder_"+ i.ToString("D3") + ".rfa";
					
					familyDocument.SaveAs(path);
					
				}
				
				familyDocument.Close(false);
				familyDocument = revit.NewFamilyDocument(@"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Support\Generic Model.rft");
				
				
				for (int i = 0; i < 100; i++) {
					
					using (Transaction transaction = new Transaction(familyDocument, "CreateGenericModel"))
					{
						transaction.Start();
						
						creationFamily = familyDocument.FamilyCreate;
						double rank = Convert.ToDouble(i);
						double size = 1/(rank+1);
						
						SweepProfile profile = CreateTriangularSweepProfile(size);
						CreateSweep(i,profile);
						
						transaction.Commit();
					}

					string path = @"C:\Users\smoreau\Desktop\Famillies\Batch Creation\Tests\Sweep_Prism_"+ i.ToString("D3") + ".rfa";
					
					familyDocument.SaveAs(path);
					
				}
				
				familyDocument.Close(false);
			}
			
		}
		#region Extrusion
		
		private void CreateExtrusion(int i, CurveArrArray profile)
		{
			double rank = Convert.ToDouble(i);
			double size = 1/(rank+1);
			double deplacement = Sum1ToN(i);
			


			Autodesk.Revit.DB.XYZ normal = Autodesk.Revit.DB.XYZ.BasisZ;
			SketchPlane sketchPlane = CreateSketchPlane(normal, Autodesk.Revit.DB.XYZ.Zero);
			
			// here create rectangular extrusion
			Extrusion rectExtrusion = creationFamily.NewExtrusion(true, profile, sketchPlane, size);
			// move to proper place
			Autodesk.Revit.DB.XYZ transPoint1 = new Autodesk.Revit.DB.XYZ(deplacement, 0, 0);
			ElementTransformUtils.MoveElement(familyDocument, rectExtrusion.Id, transPoint1);



		}
		
		private CurveArrArray CreateRectangularProfile(double size)
		{
			CurveArrArray curveArrArray = new CurveArrArray();
			CurveArray curveArray1 = new CurveArray();

			// create one rectangular extrusion
			Autodesk.Revit.DB.XYZ p0 = Autodesk.Revit.DB.XYZ.Zero;
			Autodesk.Revit.DB.XYZ p1 = new Autodesk.Revit.DB.XYZ(size, 0, 0);
			Autodesk.Revit.DB.XYZ p2 = new Autodesk.Revit.DB.XYZ(size, size, 0);
			Autodesk.Revit.DB.XYZ p3 = new Autodesk.Revit.DB.XYZ(0, size, 0);
			Line line1 = Line.CreateBound(p0, p1);
			Line line2 = Line.CreateBound(p1, p2);
			Line line3 = Line.CreateBound(p2, p3);
			Line line4 = Line.CreateBound(p3, p0);
			curveArray1.Append(line1);
			curveArray1.Append(line2);
			curveArray1.Append(line3);
			curveArray1.Append(line4);
			
			curveArrArray.Append(curveArray1);
			
			return curveArrArray;
		}
		
		private CurveArrArray CreateTriangularProfile(double size)
		{
			CurveArrArray curveArrArray = new CurveArrArray();
			CurveArray curveArray1 = new CurveArray();

			// create one rectangular extrusion
			Autodesk.Revit.DB.XYZ p0 = Autodesk.Revit.DB.XYZ.Zero;
			Autodesk.Revit.DB.XYZ p1 = new Autodesk.Revit.DB.XYZ(size, size/2, 0);
			Autodesk.Revit.DB.XYZ p2 = new Autodesk.Revit.DB.XYZ(size, -size/2, 0);
			Line line1 = Line.CreateBound(p0, p1);
			Line line2 = Line.CreateBound(p1, p2);
			Line line3 = Line.CreateBound(p2, p0);
			curveArray1.Append(line1);
			curveArray1.Append(line2);
			curveArray1.Append(line3);
			
			curveArrArray.Append(curveArray1);
			
			return curveArrArray;
		}
		
		private CurveArrArray CreateCircularProfile(double size)
		{
			CurveArrArray curveArrArray = new CurveArrArray();
			CurveArray curveArray1 = new CurveArray();
			Plane plane = Plane.CreateByNormalAndOrigin(new XYZ(0,0,1),new XYZ(size/2,0,0));
			Arc arc = Arc.Create(plane,size/2,0,360);
			curveArray1.Append(arc);
			
			curveArrArray.Append(curveArray1);
			
			return curveArrArray;
		}
		
		#endregion

		#region Revolution
		
		/// <summary>
		/// Create one circular profile revolution
		/// </summary>
		private void CreateSphereByRevolution(int i)
		{
			double rank = Convert.ToDouble(i);
			double size = 1/(rank+1);
			double deplacement = Sum1ToN(i);
			


			CurveArrArray curveArrArray = new CurveArrArray();
			CurveArray curveArray = new CurveArray();

			Autodesk.Revit.DB.XYZ normal = Autodesk.Revit.DB.XYZ.BasisZ;
			SketchPlane sketchPlane = CreateSketchPlane(normal, Autodesk.Revit.DB.XYZ.Zero);

			// create one circular profile revolution
			Autodesk.Revit.DB.XYZ p0 = Autodesk.Revit.DB.XYZ.Zero;
			Autodesk.Revit.DB.XYZ p1 = new Autodesk.Revit.DB.XYZ(size/2, size/2, 0);
			Autodesk.Revit.DB.XYZ p2 = new Autodesk.Revit.DB.XYZ(size, 0, 0);
			Curve arc = Arc.Create(p0,p2,p1);
			Line axis1 = Line.CreateBound(p0,p2);
			curveArray.Append(arc);
			curveArray.Append(axis1);

			curveArrArray.Append(curveArray);

			// here create circular profile revolution
			Revolution revolution1 = creationFamily.NewRevolution(true, curveArrArray, sketchPlane, axis1, -2*Math.PI, 0);
			// move to proper place
			Autodesk.Revit.DB.XYZ transPoint1 = new Autodesk.Revit.DB.XYZ(deplacement, 0, 0);
			ElementTransformUtils.MoveElement(familyDocument, revolution1.Id, transPoint1);
			
		}
		
		/// <summary>
		/// Create one cynlinder profile revolution
		/// </summary>
		private void CreateCylindarByRevolution(int i)
		{
			double rank = Convert.ToDouble(i);
			double size = 1/(rank+1);
			double deplacement = Sum1ToN(i);
			

			CurveArrArray curveArrArray = new CurveArrArray();
			CurveArray curveArray = new CurveArray();

			Autodesk.Revit.DB.XYZ normal = -Autodesk.Revit.DB.XYZ.BasisY;
			SketchPlane sketchPlane = CreateSketchPlane(normal, Autodesk.Revit.DB.XYZ.Zero);

			// create one cylinder profile revolution
			Autodesk.Revit.DB.XYZ p0 = Autodesk.Revit.DB.XYZ.Zero;
			Autodesk.Revit.DB.XYZ p1 = new Autodesk.Revit.DB.XYZ(size/2, 0, 0);
			Autodesk.Revit.DB.XYZ p2 = new Autodesk.Revit.DB.XYZ(size/2, 0, size);
			Autodesk.Revit.DB.XYZ p3 = new Autodesk.Revit.DB.XYZ(0, 0, size);
			Line line1 = Line.CreateBound(p0, p1);
			Line line2 = Line.CreateBound(p1, p2);
			Line line3 = Line.CreateBound(p2, p3);
			Line line4 = Line.CreateBound(p3, p0);
			curveArray.Append(line1);
			curveArray.Append(line2);
			curveArray.Append(line3);
			curveArray.Append(line4);
			Line axis1 = Line.CreateBound(p1,p2);


			curveArrArray.Append(curveArray);

			// here create circular profile revolution
			Revolution revolution1 = creationFamily.NewRevolution(true, curveArrArray, sketchPlane, axis1, -2*Math.PI, 0);
			// move to proper place
			Autodesk.Revit.DB.XYZ transPoint1 = new Autodesk.Revit.DB.XYZ(deplacement, 0, 0);
			ElementTransformUtils.MoveElement(familyDocument, revolution1.Id, transPoint1);

			
		}
		
		#endregion
		
		#region Sweep
		
		private void CreateSweep(int i, SweepProfile profile)
		{
			double rank = Convert.ToDouble(i);
			double size = 1/(rank+1);
			double deplacement = Sum1ToN(i);
			

			Autodesk.Revit.DB.XYZ normal = Autodesk.Revit.DB.XYZ.BasisX;
			SketchPlane sketchPlane = CreateSketchPlane(normal, Autodesk.Revit.DB.XYZ.Zero);
			XYZ p0 = XYZ.Zero;
			XYZ p1 = new XYZ(0,0,size);
			Line linePath = Line.CreateBound(p0,p1);
			
			CurveArray curves = new CurveArray();
			curves.Append(linePath);

			
			// here create rectangular extrusion
			Sweep sweep = creationFamily.NewSweep(true,curves,sketchPlane, profile, 0, ProfilePlaneLocation.Start);
			// move to proper place
			Autodesk.Revit.DB.XYZ transPoint1 = new Autodesk.Revit.DB.XYZ(deplacement, 0, 0);
			ElementTransformUtils.MoveElement(familyDocument, sweep.Id, transPoint1);


		}
		
		private SweepProfile CreateRectangularSweepProfile(double size)
		{

			CurveArrArray curveArrArray = new CurveArrArray();
			CurveArray curveArray1 = new CurveArray();

			// create one rectangular extrusion
			Autodesk.Revit.DB.XYZ p0 = Autodesk.Revit.DB.XYZ.Zero;
			Autodesk.Revit.DB.XYZ p1 = new Autodesk.Revit.DB.XYZ(size, 0, 0);
			Autodesk.Revit.DB.XYZ p2 = new Autodesk.Revit.DB.XYZ(size, size, 0);
			Autodesk.Revit.DB.XYZ p3 = new Autodesk.Revit.DB.XYZ(0, size, 0);
			Line line1 = Line.CreateBound(p0, p1);
			Line line2 = Line.CreateBound(p1, p2);
			Line line3 = Line.CreateBound(p2, p3);
			Line line4 = Line.CreateBound(p3, p0);
			curveArray1.Append(line1);
			curveArray1.Append(line2);
			curveArray1.Append(line3);
			curveArray1.Append(line4);
			
			curveArrArray.Append(curveArray1);
			
			return revit.Create.NewCurveLoopsProfile(curveArrArray);
		}
		
		private SweepProfile CreateCircularSweepProfile(double size)
		{

			CurveArrArray curveArrArray = new CurveArrArray();
			CurveArray curveArray1 = new CurveArray();

			Plane plane = Plane.CreateByNormalAndOrigin(new XYZ(0,0,1),new XYZ(size/2,0,0));
			Arc arc = Arc.Create(plane,size/2,0,360);
			curveArray1.Append(arc);
			
			curveArrArray.Append(curveArray1);
			
			return revit.Create.NewCurveLoopsProfile(curveArrArray);
		}
		
		private SweepProfile CreateTriangularSweepProfile(double size)
		{

			CurveArrArray curveArrArray = new CurveArrArray();
			CurveArray curveArray1 = new CurveArray();

			Autodesk.Revit.DB.XYZ p0 = Autodesk.Revit.DB.XYZ.Zero;
			Autodesk.Revit.DB.XYZ p1 = new Autodesk.Revit.DB.XYZ(size, size/2, 0);
			Autodesk.Revit.DB.XYZ p2 = new Autodesk.Revit.DB.XYZ(size, -size/2, 0);
			Line line1 = Line.CreateBound(p0, p1);
			Line line2 = Line.CreateBound(p1, p2);
			Line line3 = Line.CreateBound(p2, p0);
			curveArray1.Append(line1);
			curveArray1.Append(line2);
			curveArray1.Append(line3);
			
			curveArrArray.Append(curveArray1);
			
			return revit.Create.NewCurveLoopsProfile(curveArrArray);
		}
		

		
		#endregion
		
		/// <summary>
		/// Create sketch plane for generic model profile
		/// </summary>
		/// <param name="normal">plane normal</param>
		/// <param name="origin">origin point</param>
		/// <returns></returns>
		internal SketchPlane CreateSketchPlane(Autodesk.Revit.DB.XYZ normal, Autodesk.Revit.DB.XYZ origin)
		{
			// First create a Geometry.Plane which need in NewSketchPlane() method
			Plane geometryPlane = Plane.CreateByNormalAndOrigin(normal, origin);
			if (null == geometryPlane)  // assert the creation is successful
			{
				throw new Exception("Create the geometry plane failed.");
			}
			// Then create a sketch plane using the Geometry.Plane
			SketchPlane plane = SketchPlane.Create(familyDocument, geometryPlane);
			// throw exception if creation failed
			if (null == plane)
			{
				throw new Exception("Create the sketch plane failed.");
			}
			return plane;
		}
		
		private double Sum1ToN(int n)
		{
			double result = 0;
			for (int i = 0; i < n; i++)
			{
				result = result + 1/(Convert.ToDouble(i)+1);
			}
			
			return result;
		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
	}
}