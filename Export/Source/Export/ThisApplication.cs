﻿/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 20/06/2015
 * Time: 11:44
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.IO;
using RestSharp;
using System.Net;
using System.Xml.Serialization;
using RestSharp.Authenticators;
using RestSharp.Authenticators.OAuth;
using Autodesk.Revit.DB.ExtensibleStorage;

namespace Export
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("642C4FF5-BBFE-4D90-B624-18A80E4A7B99")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		
		public void CategoryParameters()
		{
			Document doc = this.ActiveUIDocument.Document;
			Autodesk.Revit.ApplicationServices.Application app = this.Application;
			//Create a list of category
			CategorySet myCategories = CreateCategoryList(doc, app);
			
			//Create a string list for printing
			List<string> values = new List<string>();
			values.Add("Category.Name;category.CategoryType;Instance/Type;param.Definition.Name;param.Definition.ParameterGroup;param.Definition.ParameterType;param.Definition.UnitType;param.IsReadOnly;param.IsShared");
			//Retrive all model elements
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			
			IList<Element> elementsList = collector.WhereElementIsNotElementType().ToElements();
			
			foreach (Element element in elementsList) {
				if (element.Category!= null)
				{
					Category category = element.Category;
					
					
					string text = category.Name + ";" + category.CategoryType + ";Instance;";

					foreach (Autodesk.Revit.DB.Parameter param in element.Parameters) {
						values.Add(text +
						           param.Definition.Name + ";" +
						           param.Definition.ParameterGroup.ToString() + ";" +
						           param.Definition.ParameterType + ";" +
						           param.Definition.UnitType + ";" +
						           param.IsReadOnly + ";" +
						           param.IsShared);
					}
					
					ElementId typeId = element.GetTypeId();
					
					if (typeId.IntegerValue != -1)
					{
						Element type = doc.GetElement(typeId);
						
						text = category.Name + ";" + category.CategoryType + ";Type;";

						foreach (Autodesk.Revit.DB.Parameter param in type.Parameters) {
							values.Add(text +
							           param.Definition.Name + ";" +
							           param.Definition.ParameterGroup + ";" +
							           param.Definition.ParameterType + ";" +
							           param.Definition.UnitType + ";" +
							           param.IsReadOnly + ";" +
							           param.IsShared);
						}
					}
				}
				
			}
			
			values = values.Distinct().ToList();
			string path = @"C:\Affaires\01-Developpement\Projecyt\categories.csv";
			File.WriteAllLines(path,values.ToArray(),Encoding.UTF32);
			
		}
		
		private CategorySet CreateCategoryList(Document doc, Autodesk.Revit.ApplicationServices.Application app)
		{
			CategorySet myCategorySet = app.Create.NewCategorySet();
			Categories categories = doc.Settings.Categories;

			foreach (Category c in categories)
			{
				if (c.AllowsBoundParameters && c.CategoryType == CategoryType.Model)
				{
					myCategorySet.Insert(c);
				}
			}

			return myCategorySet;
		}
		
		public void ExportBuiltInCategories()
		{
			Document doc = this.ActiveUIDocument.Document;
			Categories categories = doc.Settings.Categories;
			
			List<string> categoriesList = new List<string>();
			categoriesList.Add("Rank;CategoryType;Name;Id;IsSubCategory;IsCuttable;IsTagCategory");
			
			foreach( Category c in categories )
			{
				
				categoriesList.Add(
					"1;"+c.CategoryType.ToString()+";"+
					c.Name+";"+c.Id+";"+";"+c.IsCuttable.ToString()+";"+
					c.IsTagCategory.ToString()
				);
				//Retrive sub categories
				foreach (Category subc in c.SubCategories) {
					categoriesList.Add(
						"2;"+subc.CategoryType.ToString()+";"+
						subc.Name+";"+subc.Id+";"+c.Name+";"+
						subc.IsCuttable.ToString()+";"+
						subc.IsTagCategory.ToString()
					);
				}
			}
			
			string path = @"C:\Google Drive\02 - BIM 42\01 - Posts\Classification in Revit\BuiltInCategories.csv";
			File.WriteAllLines(path,categoriesList.ToArray(),Encoding.UTF32);
		}
		
		public void ExportViewList()
		{
			string[] files = new string[]{
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\30-CVC\CVC-INF-F13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\30-CVC\CVC-P13-F13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\30-CVC\CVC-P14-F13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\30-CVC\CVC-P19-F13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\40-PLB\PLB-INF-F13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\40-PLB\PLB-P13-F13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\40-PLB\PLB-P14-F13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\40-PLB\PLB-P19-F13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\CFA-INF-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\CFA-P13-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\CFA-P14-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\CFA-P19-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\CFO-INF-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\CFO-P13-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\CFO-P14-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\CFO-P19-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\TER-INF-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\TER-P13-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\TER-P14-M13.rvt",
				@"P:\!COMMUN\$3D\AIG\00-CENTRAL\50-ELE\TER-P19-M13.rvt"};

			foreach (string file in files)
			{
				string fileNAme = System.IO.Path.GetFileNameWithoutExtension(file);
				ModelPath revitPath = ModelPathUtils.ConvertUserVisiblePathToModelPath(file);
				
				OpenOptions opOptions = new OpenOptions();
				opOptions.DetachFromCentralOption = DetachFromCentralOption.DetachAndPreserveWorksets;
				
				Document document = this.Application.OpenDocumentFile(revitPath,opOptions);
				
				IEnumerable<ViewSchedule> schedules = from elem in new FilteredElementCollector(document).OfClass(typeof(ViewSchedule))
					let schedule = elem as ViewSchedule
					where schedule.Name.Contains("Liste")
					select schedule;
				
				string folderPath = @"C:\Users\moreaus\Desktop\Courant\01-AIG\Automation_2014\Export";
				ViewScheduleExportOptions options = new ViewScheduleExportOptions();
				options.FieldDelimiter =";";
				options.HeadersFootersBlanks = false;
				
				foreach (ViewSchedule schedule in schedules)
				{
					schedule.Export(folderPath,fileNAme + "_" +schedule.Name + ".txt",options);
				}
				
				document.Close(false);
				
			}
		}
		
		public void ExportSchedules()
		{
			Document document = this.ActiveUIDocument.Document;

			IEnumerable<ViewSchedule> schedules = from elem in new FilteredElementCollector(document).OfClass(typeof(ViewSchedule))
				let schedule = elem as ViewSchedule
				where schedule.Name.Contains("Num-")
				select schedule;
			
			string folderPath = @"P:\!COMMUN\$3D\AIG\04-BIM\70-RENDUS\03-Métré";
			ViewScheduleExportOptions options = new ViewScheduleExportOptions();
			
			foreach (ViewSchedule schedule in schedules)
			{
				schedule.Export(folderPath,schedule.Name + ".txt",options);
			}
		}
		
		public void Categories()
		{
			Document doc = this.ActiveUIDocument.Document;
			Categories categories = doc.Settings.Categories;
			
			List<string> categoriesList = new List<string>();
			categoriesList.Add("Rank;CategoryType;Name;Id;IsSubCategory;IsCuttable;IsTagCategory");
			
			foreach( Category c in categories )
			{
				if (c.AllowsBoundParameters && c.CategoryType == CategoryType.Model)
				{
					categoriesList.Add("1;"+c.CategoryType.ToString()+";"+c.Name+";"+c.Id+";"+";"+c.IsCuttable.ToString()+";"+c.IsTagCategory.ToString());

				}
				
			}
			
			string path = @"C:\Travail\BIM42\Time Stamper\categories.csv";
			File.WriteAllLines(path,categoriesList.ToArray(),Encoding.GetEncoding("iso-8859-1"));
		}
		
		private const string client_id = "hl94XJLXaQe3ogX";
		private const string client_secret = "ZbwjiwgwWHAwcBj";

		public Token AuthorizedApp()
		{
			Uri baseUrl = new Uri("https://api.bimsync.com");
			
			// Creates a redirect URI using an available port on the loopback address.
			string redirectURI = string.Format("http://{0}:{1}/", IPAddress.Loopback,"63842");// GetRandomUnusedPort());

			// Creates an HttpListener to listen for requests on that redirect URI.
			var http = new HttpListener();
			http.Prefixes.Add(redirectURI);
			http.Start();
			
			// Creates the OAuth 2.0 authorization request.
			string authorizationRequest = string.Format("{0}/oauth2/authorize?response_type=code&redirect_uri={1}&client_id={2}&state={3}",
			                                            baseUrl.OriginalString,
			                                            System.Uri.EscapeDataString(redirectURI),
			                                            client_id,
			                                            "1");
			
			// Opens request in the browser.
			System.Diagnostics.Process.Start(authorizationRequest);

			// Waits for the OAuth authorization response.
			var context = http.GetContext();

			// Brings this app back to the foreground.
			
			// Sends an HTTP response to the browser.
			var response = context.Response;
			string responseString = string.Format("<html><head><meta http-equiv='refresh' content='10;url=https://www.bimsync.com'></head><body>Please return to the app.</body></html>");
			var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
			response.ContentLength64 = buffer.Length;
			var responseOutput = response.OutputStream;
			System.Threading.Tasks.Task responseTask = responseOutput.WriteAsync(buffer, 0, buffer.Length)
				.ContinueWith((task) =>
				              {
				              	responseOutput.Close();
				              	http.Stop();
				              	Console.WriteLine("HTTP server stopped.");
				              });
			
			// extracts the code
			var code = context.Request.QueryString.Get("code");
			var incoming_state = context.Request.QueryString.Get("state");
			
			RestClient client = new RestClient("https://api.bimsync.com");
			
			//Request the access token
			RestRequest accessTokenRequest = new RestRequest("oauth2/token",Method.POST);
			//accessTokenRequest.AddHeader("Accept", "application/json");
			accessTokenRequest.AddHeader("Content-Type", "application/x-www-form-urlencoded");
			
			accessTokenRequest.AddParameter("code",code);
			accessTokenRequest.AddParameter("grant_type","authorization_code");
			accessTokenRequest.AddParameter("client_id",client_id);
			accessTokenRequest.AddParameter("client_secret",client_secret);
			accessTokenRequest.AddParameter("redirect_uri",redirectURI);
			
			IRestResponse<Token> responseToken = client.Execute<Token>(accessTokenRequest);
			
			return responseToken.Data;
		}
		
		public string SerializeObject<T>(T toSerialize)
		{
			XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

			using(StringWriter textWriter = new StringWriter())
			{
				xmlSerializer.Serialize(textWriter, toSerialize);
				return textWriter.ToString();
			}
		}
		
		public T DeserializeObject<T>(string testData)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(T));
			
			using (TextReader reader = new StringReader(testData))
			{
				T result = (T) serializer.Deserialize(reader);
				return result;
			}
		}
		
		public Token RefrechToken(Token token)
		{
			RestClient client = new RestClient("https://api.bimsync.com");
			
			//Refresh token
			RestRequest refrechTokenRequest = new RestRequest("oauth2/token",Method.POST);
			//refrechTokenRequest.AddHeader("Authorization", "Bearer " + token.access_token);
			
			refrechTokenRequest.AddParameter("refresh_token",token.refresh_token);
			refrechTokenRequest.AddParameter("grant_type","refresh_token");
			refrechTokenRequest.AddParameter("client_id",client_id);
			refrechTokenRequest.AddParameter("client_secret",client_secret);
			
			IRestResponse<Token> responseToken = client.Execute<Token>(refrechTokenRequest);
			
			return responseToken.Data;
		}
		
		public void exportTobimsync()
		{
			//Export the project as IFC
			
			Document doc = this.ActiveUIDocument.Document;
			
			IFCExportOptions IFCOptions = new IFCExportOptions();
			IFCOptions.ExportBaseQuantities = true;
			IFCOptions.FileVersion = IFCVersion.IFC2x3;
			IFCOptions.WallAndColumnSplitting = true;
			IFCOptions.SpaceBoundaryLevel = 1;
			
			string folder = System.IO.Path.GetTempPath();
			string name = DateTime.Now.ToString("yyyyMMddHHmmss")+ "_" + System.IO.Path.GetFileNameWithoutExtension(doc.PathName);
			
			//            using (Transaction tx = new Transaction(doc)) {
			//                tx.Start("Export to IFC");
//
			//                doc.Export(folder,name,IFCOptions);
//
			//                tx.Commit();
			//            }
			
			//
			
			string filePath = @"C:\Google Drive\05 - Travail\Revit Dev\bimsync\token.txt";

			Token savedToken = DeserializeObject<Token>(File.ReadAllText(filePath));
			
			//Token token = AuthorizedApp();
			Token token = RefrechToken(savedToken);
			
			//Save the token in a file
			string value = SerializeObject<Token>(token);
			File.WriteAllText(filePath,value);
			
			string access_token = token.access_token;
			
			RestClient client = new RestClient("https://api.bimsync.com");
			
			//Get users projects
			RestRequest projectsRequest = new RestRequest("v2/projects",Method.GET);
			projectsRequest.AddHeader("Authorization", "Bearer " + access_token);
			
			IRestResponse<List<Project>> iresponse = client.Execute<List<Project>>(projectsRequest);

			string projectId = iresponse.Data.Where(x => x.name.Contains("Test Bouygues Immobilier")).FirstOrDefault().id;
			
			//List all models in the project
			RestRequest modelsRequest = new RestRequest("v2/projects/"+projectId+"/models",Method.GET);
			modelsRequest.AddHeader("Authorization", "Bearer " + access_token);
			
			IRestResponse<List<Model>> modelsResponse = client.Execute<List<Model>>(modelsRequest);
			
			string modelId = modelsResponse.Data.Where(x => x.name.Contains("Test")).FirstOrDefault().id;
			
			RestRequest revisionRequest = new RestRequest("v2/projects/"+projectId+"/revisions",Method.POST);
			revisionRequest.AddHeader("Authorization", "Bearer " + access_token);
			revisionRequest.AddHeader("Content-Type", "application/ifc");
			revisionRequest.AddHeader("Bimsync-Params", "{\"callbackUrl\": \"http://127.0.0.1:51335/\",\"comment\": \"Add windows 2\",\"filename\": \"Duplex_A_20110907.ifc\",\"model\": \"" + modelId+ "\"}");
			
			string path = @"C:\Google Drive\05 - Travail\Revit Dev\bimsync\Project1.ifc";
			byte[] data = File.ReadAllBytes(path);
			revisionRequest.AddParameter("application/ifc", data, RestSharp.ParameterType.RequestBody);
			
			var reponsetest = client.Execute(revisionRequest);
//
		}
		
		public void StoreConnection(Element project, Token token)
		{
			using (Transaction tx = new Transaction(project.Document) ){
				tx.Start("Store Data");
				
				//Find the existing schema
				Schema schema = Schema.ListSchemas().Where(x => x.SchemaName == "bimsync").FirstOrDefault();
				
				if (schema == null) //Create a new schema
				{
					Guid guid = new Guid();
					SchemaBuilder schemaBuilder = new SchemaBuilder(guid);
					schemaBuilder.SetReadAccessLevel(AccessLevel.Public); // allow anyone to read the object
					schemaBuilder.SetWriteAccessLevel(AccessLevel.Vendor); // restrict writing to this vendor only
					schemaBuilder.SetVendorId("BM42"); // required because of restricted write-access
					schemaBuilder.SetSchemaName("bimsync");
					
					// create a field to store the access_token
					FieldBuilder access_tokenBuilder = schemaBuilder.AddSimpleField("access_token", typeof(string));
					access_tokenBuilder.SetDocumentation("The bimsync access token.");
					
					// create a field to store the refresh_token
					FieldBuilder refresh_tokenBuilder = schemaBuilder.AddSimpleField("refresh_token", typeof(string));
					refresh_tokenBuilder.SetDocumentation("The bimsync refresh token.");
					
					schema = schemaBuilder.Finish();
				}
				
				//Find the entity in this schema
				Entity entity = project.GetEntity(schema);
				
				if (entity == null) //Create an entity to store the values
				{
					entity = new Entity(schema); // create an entity (object) for this schema (class)
				}
				
				if (entity.SchemaGUID != schema.GUID)
				{
					throw new Exception("SchemaID of found entity does not match the SchemaID passed to GetEntity.");
				}
				
				// get the field from the schema
				Field access_tokenField = schema.GetField("access_token");
				// set the value for this entity
				entity.Set<string>(access_tokenField, token.access_token, DisplayUnitType.DUT_GENERAL);
				project.SetEntity(entity); // store the entity in the element
				
				// get the field from the schema
				Field refresh_tokenField = schema.GetField("refresh_token");
				// set the value for this entity
				entity.Set<string>(refresh_tokenField, token.refresh_token, DisplayUnitType.DUT_GENERAL);
				project.SetEntity(entity); // store the entity in the element

				tx.Commit();
				
			}
		}
		
		public static int GetRandomUnusedPort()
		{
			var listener = new System.Net.Sockets.TcpListener(IPAddress.Loopback, 0);
			listener.Start();
			var port = ((IPEndPoint)listener.LocalEndpoint).Port;
			listener.Stop();
			return port;
		}
	}
	
	public class Project
	{
		public string id { get; set; }
		public string name { get; set; }
		public string description { get; set; }
		public string createdAt { get; set; }
		public string updatedAt { get; set; }
	}
	
	public class Model
	{
		public string id { get; set; }
		public string name { get; set; }
	}

	public class User
	{
		public string createdAt { get; set; }
		public string id { get; set; }
		public string name { get; set; }
		public string username { get; set; }
	}

	public class Revision
	{
		public string comment { get; set; }
		public string createdAt { get; set; }
		public string id { get; set; }
		public Model model { get; set; }
		public User user { get; set; }
		public int version { get; set; }
	}

	public class Token
	{
		public string access_token { get; set; }
		public string token_type { get; set; }
		public int expires_in { get; set; }
		public string refresh_token { get; set; }
	}
}