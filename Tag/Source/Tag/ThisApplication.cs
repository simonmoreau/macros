/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 8/2/2014
 * Time: 8:31 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Tag
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("65ADB6E9-212C-4927-B24E-AED02F4AF8EA")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}
		
		

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		
		public void TagRevisionCloud()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			
			//Find all view
			IEnumerable<ViewPlan> viewEnum = from elem in new FilteredElementCollector(_doc).OfClass(typeof(ViewPlan))
				let view = elem as ViewPlan
				where view.IsTemplate == false
				where view.Name.Contains("R_Niveau")
				select view;
			
			using (Transaction tx = new Transaction(_doc)) {
				
				
				tx.Start("Tag all revision cloud");
				
				foreach (ViewPlan vienP in viewEnum) {
					
					//Find all revision cloud
					IList<Element> RevisionClouds = new FilteredElementCollector(_doc,vienP.Id).OfCategory(BuiltInCategory.OST_RevisionClouds).ToElements();
					
					foreach (Element revisionCloud in RevisionClouds) {

						//Retrive the boundary
						ElementId id = new ElementId(revisionCloud.Id.IntegerValue - 1 );
						
						Sketch boundary = _doc.GetElement( id ) as Sketch;
						
						if( null != boundary )
						{
							CurveArrArray curBoundaries = boundary.Profile;
							
							foreach (CurveArray curBoundary in curBoundaries) {
								
								XYZ result = new XYZ(0,0,0);
								double XMax = curBoundary.get_Item(0).GetEndPoint( 0 ).X;
								
								//Retrive baracenter
								foreach (Curve crv in curBoundary) {
									XYZ corner = crv.GetEndPoint( 0 );
									result = result + corner;
									if (corner.X > XMax)
									{
										XMax = corner.X;
									}
								}
								
								int size = curBoundary.Size;
								double sizeInverse = Convert.ToDouble(1) / Convert.ToDouble(size);
								
								XYZ bayacenter = result.Multiply(sizeInverse);
								
								
								//Retrive cloud size
								double Xpos = (XMax - bayacenter.X)*1.5;
								
								XYZ point = bayacenter + new XYZ(Xpos,0,0);
								
								_doc.Create.NewTag(vienP,revisionCloud,true, TagMode.TM_ADDBY_CATEGORY, TagOrientation.Horizontal,point);
							}
							
							
						}
						
						
					}
					
				}
				
				tx.Commit();
			}
			
		}
		
		public void AlignTag()
		{
			// Get the handle of current document.
			UIDocument uidoc = this.ActiveUIDocument;
			Document doc = this.ActiveUIDocument.Document;

			// Get the element selection of current document.
			Selection selection = uidoc.Selection;
			ICollection<ElementId> collection = selection.GetElementIds();
			
			List<IndependentTag> tags = new List<IndependentTag>();
			
			
			foreach (ElementId id in collection) {
				Element e = doc.GetElement(id);
				IndependentTag tag = e as IndependentTag;
				
				if (tag != null)
				{
					tags.Add(tag);
				}
			}
			
			//Align top
			
			View currentView = doc.ActiveView;
			
			XYZ upDir = currentView.UpDirection;
			
			//Get the max in upDir
			XYZ maxDir = new XYZ();
			XYZ headPoint = new XYZ();
			
			foreach (IndependentTag intag in tags)
			{
				headPoint = intag.TagHeadPosition;
				
				//upDir projection
				if (upDir.Multiply( headPoint.DotProduct(upDir)).GetLength() > maxDir.GetLength())
				{
					maxDir = upDir.Multiply( headPoint.DotProduct(upDir));
				}
			}
			
			using (Transaction tx = new Transaction(doc)) {
				tx.Start("Align Top");
				
				//Move each tag
				foreach (IndependentTag intag in tags)
				{
					headPoint = intag.TagHeadPosition;
					Transform tr = Transform.CreateTranslation(maxDir - upDir.Multiply( headPoint.DotProduct(upDir)));
					
					intag.TagHeadPosition = tr.OfPoint(headPoint);
					
				}
				
				tx.Commit();
			}
			

			
			

		}
		public void InsulationTag()
		{
			UIDocument uidoc = this.ActiveUIDocument;
			Document doc = uidoc.Document;
			
			Reference r = null;
			
			//Retrive the two symbols
			FamilySymbol rectangularSymbol = null;
			FamilySymbol roundSymbol = null;
			ICollection<Element> collection = new FilteredElementCollector(doc).OfClass(typeof(FamilySymbol)).OfCategory(BuiltInCategory.OST_DetailComponents).ToElements();
			foreach (Element element in collection)
			{
				FamilySymbol current = element as FamilySymbol;
				// This NewFamilyInstance overload requires a curve based family
				if (current.Family.Name == "DuctInsulationAnnotationRectangular")
				{
					rectangularSymbol = current;
				}
				else if (current.Family.Name == "DuctInsulationAnnotationRound")
				{
					roundSymbol = current;
				}
			}
			

			
			try
			{
				while (true) {
					
					using( Transaction t = new Transaction( doc ) )
					{
						t.Start( "Annotate Insulations" );

						r = uidoc.Selection.PickObject(ObjectType.Element, new SelectionFilter(),"Pick a duct");
						
						if (r != null)
						{
							MEPCurve mepCurve = doc.GetElement(r.ElementId) as MEPCurve;
							//doc.ActiveView.GetElementOverrides
							//Get the middle of the duct
							LocationCurve locCurve = mepCurve.Location as LocationCurve;
							XYZ insertionPoint = locCurve.Curve.Evaluate(0.5,true);
							//Get the angle
							XYZ vector = locCurve.Curve.GetEndPoint(1) - locCurve.Curve.GetEndPoint(0);
							double angle = new XYZ(1,0,0).AngleTo(vector);
							
							//Get the insulation
							ICollection<ElementId> insulationIds = InsulationLiningBase.GetInsulationIds(doc,r.ElementId);
							
							if (insulationIds.Count !=0)
							{
								InsulationLiningBase insulation = doc.GetElement(insulationIds.First()) as InsulationLiningBase;
								
								if (insulation != null)
								{
									FamilySymbol symbol = null;
									double widht;
									try {
										widht = insulation.Width;
										//Select the detail familly
										symbol = rectangularSymbol;
									} catch (Autodesk.Revit.Exceptions.InvalidOperationException) {
										Parameter outerDiameter = mepCurve.get_Parameter(BuiltInParameter.RBS_PIPE_OUTER_DIAMETER);
										if (outerDiameter != null)
										{
											widht = insulation.Thickness*2 + outerDiameter.AsDouble();
										}
										else
										{
											widht = insulation.Diameter;
										}
										
										//Select the detail familly
										symbol = roundSymbol;
									}
									
									//Create the annotation
									FamilyInstance annotation = doc.Create.NewFamilyInstance(insertionPoint,symbol,doc.ActiveView);
									
									//Change the witdh
									annotation.GetParameters("Width").First().Set(widht);
									
									//rotate the component
									annotation.Location.Rotate(Line.CreateUnbound(insertionPoint,new XYZ(0,0,1)),-angle);
								}
							}
						}

						t.Commit();
					}
				}
				
			}
			catch( Autodesk.Revit.Exceptions.OperationCanceledException )
			{
			}
		}
		public void MatchFilledRegion()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//Find all Filled Region Type, and create a dictonary with it
			Dictionary<string,FilledRegionType> modelFilledRegionTypes =
				new FilteredElementCollector(doc).OfClass(typeof(FilledRegionType)).ToElements().Cast<FilledRegionType>().ToDictionary(e => e.Name);
			
			//Find all loaded families
			IList<Element> elements = new FilteredElementCollector(doc).OfClass(typeof(Family)).ToElements();
			
			//Get Detail Item category id.
			ElementId detailItemCategoryId = doc.Settings.Categories.get_Item(BuiltInCategory.OST_DetailComponents).Id;
			

			
			//Loop on all loaded families
			foreach (Element familyElement in elements) {
				
				Family family = familyElement as Family;
				
				//Exit the families loop if it isn't a Detail Item Familly
				if (family.FamilyCategory.Id != detailItemCategoryId) continue;

				//Open the family
				Document familyDoc = doc.EditFamily(family);
				string familyPath = Path.Combine(Path.GetTempPath(),family.Name+".rfa");
				
				bool familyEdited = false;
				
				//Find all Filled Region Type in the family
				IList<Element> filledRegionTypes = new FilteredElementCollector(familyDoc).OfClass(typeof(FilledRegionType)).ToElements();
				
				using (Transaction famTx = new Transaction(familyDoc))
				{
					famTx.Start("Edit Filled Region Type");
					
					//Loop on all Filled Region types in the family
					foreach (Element filledRegionTypeElement in filledRegionTypes)
					{
						FilledRegionType filledRegionType = filledRegionTypeElement as FilledRegionType;
						if (modelFilledRegionTypes.ContainsKey(filledRegionType.Name))
						{
							FilledRegionType refFilledRegion = modelFilledRegionTypes[filledRegionType.Name];
							//Change the color
							if (filledRegionType.Color.Red != refFilledRegion.Color.Red
							    || filledRegionType.Color.Blue != refFilledRegion.Color.Blue
							    || filledRegionType.Color.Green != refFilledRegion.Color.Green)
							{
								filledRegionType.Color = refFilledRegion.Color;
								familyEdited = true;
							}
							
							//Change the Background
							if (filledRegionType.Background != refFilledRegion.Background)
							{
								filledRegionType.Background = refFilledRegion.Background;
								familyEdited = true;
							}
							
							//Change line weight
							if (filledRegionType.LineWeight != refFilledRegion.LineWeight)
							{
								filledRegionType.LineWeight = refFilledRegion.LineWeight;
								familyEdited = true;
							}
							
						}
						
					}
					
					famTx.Commit();
				}
				
				if (familyEdited)
				{
					familyDoc.LoadFamily(doc, new FamilyOption());
					familyDoc.Close( false );
				}
				else
				{
					familyDoc.Close( false );
				}

			}
			

		}
		public void ChangeLineStyle()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//find all line style
			Dictionary<string,GraphicsStyle> styles = new FilteredElementCollector(doc).OfClass(typeof(GraphicsStyle)).ToElements().Cast<GraphicsStyle>().ToDictionary(e => e.Name);
			
			//Find all detail lines
			IList<Element> elements = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Lines).WhereElementIsNotElementType().ToElements();
			
			foreach (Element element in elements) {
				
				DetailCurve detailCurve = element as DetailCurve;

				switch (detailCurve.LineStyle.Name)
				{
					case "line style 1":
						detailCurve.LineStyle = styles["line style 2"];
						break;
					case "line style 3":
						detailCurve.LineStyle = styles["line style 5"];
						break;
					default:
						break;
				}
			}
		}
		public void CreateFilledRegionStyle()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			string path = @"S:\BB1651-MONTROUGE\01-ENTREES\95-SYN ARC\20151015 - Couleurs de synthèse architecturale\styledeLines.txt";
			string[] lines = File.ReadAllLines(path);
			
			//Find the first Filled Region Type
			FilledRegionType baseFilledRegionType = new FilteredElementCollector(doc).OfClass(typeof(FilledRegionType)).ToElements().Cast<FilledRegionType>().FirstOrDefault();

			using (Transaction docTransaction = new Transaction(doc, "Create filled region styles"))
			{
				docTransaction.Start();
				
				foreach (string line in lines)
				{
					string newSubCatName = line.Split(';')[0];
					
					Color newSubCatColor = new Color(
						Convert.ToByte(line.Split(';')[1]),
						Convert.ToByte(line.Split(';')[2]),
						Convert.ToByte(line.Split(';')[3]));  //Red
					
					ElementType newType = baseFilledRegionType.Duplicate(newSubCatName);
					FilledRegionType newFilledRegionType = newType as FilledRegionType;
					
					newFilledRegionType.Color = newSubCatColor;
				}
				
				docTransaction.Commit();
			}
		}
		
		public void CreateLineStyle()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			string path = @"S:\BB1651-MONTROUGE\01-ENTREES\95-SYN ARC\20151015 - Couleurs de synthèse architecturale\styledeLines.txt";
			string[] lines = File.ReadAllLines(path);
			
			Category lineCat = doc.Settings.Categories.get_Item(BuiltInCategory.OST_Lines);
			Category lineSubCat;

			using (Transaction docTransaction = new Transaction(doc, "Create line styles"))
			{
				docTransaction.Start();
				
				foreach (string line in lines)
				{
					string newSubCatName = line.Split(';')[0];
					Color newSubCatColor = new Color(
						Convert.ToByte(line.Split(';')[1]),
						Convert.ToByte(line.Split(';')[2]),
						Convert.ToByte(line.Split(';')[3]));  //Red
					
					lineSubCat = doc.Settings.Categories.NewSubcategory(lineCat, newSubCatName);
					lineSubCat.LineColor = newSubCatColor;
				}
				
				docTransaction.Commit();
			}
		}
		
		public void CreateDetailItemsStyle()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			string path = @"S:\BB1651-MONTROUGE\01-ENTREES\95-SYN ARC\20151015 - Couleurs de synthèse architecturale\styledeLines.txt";
			string[] lines = File.ReadAllLines(path);
			
			Category lineCat = doc.Settings.Categories.get_Item(BuiltInCategory.OST_DetailComponents);
			Category lineSubCat;

			using (Transaction docTransaction = new Transaction(doc, "Create line styles"))
			{
				docTransaction.Start();
				
				foreach (string line in lines)
				{
					string newSubCatName = line.Split(';')[0];
					Color newSubCatColor = new Color(
						Convert.ToByte(line.Split(';')[1]),
						Convert.ToByte(line.Split(';')[2]),
						Convert.ToByte(line.Split(';')[3]));  //Red
					
					lineSubCat = doc.Settings.Categories.NewSubcategory(lineCat, newSubCatName);
					lineSubCat.LineColor = newSubCatColor;
				}
				
				docTransaction.Commit();
			}
		}
		
		public void CreateTextType()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			string path = @"S:\BB1651-MONTROUGE\01-ENTREES\95-SYN ARC\20151015 - Couleurs de synthèse architecturale\styledeLines.txt";
			string[] lines = File.ReadAllLines(path);
			
			// Get access to all the TextNote Elements
			FilteredElementCollector collectorUsed= new FilteredElementCollector(doc);

			
			//Arial 1.5 mm
			IEnumerable<Element> textNotesTypes = from element in collectorUsed.OfClass(typeof(TextNoteType)).ToElements()
				where element.Name == "Arial 1.5 mm"
				select element;
			
			TextNoteType textNoteType = textNotesTypes.FirstOrDefault() as TextNoteType;

			using (Transaction docTransaction = new Transaction(doc, "Create text styles"))
			{
				docTransaction.Start();
				
				foreach (string line in lines)
				{
					string newSubCatName = line.Split(';')[0];
					Color newSubCatColor = new Color(
						Convert.ToByte(line.Split(';')[1]),
						Convert.ToByte(line.Split(';')[2]),
						Convert.ToByte(line.Split(';')[3]));  //Red
					
					
					// Create a duplicate
					TextNoteType noteType =textNoteType.Duplicate("Arial 1.5 mm - " + newSubCatName) as TextNoteType;
					
					if (null != noteType)
					{
						
						Parameter pTextColor = noteType.get_Parameter(BuiltInParameter.TEXT_COLOR);
						if (pTextColor != null)
						{
							pTextColor.Set(newSubCatColor.ToString());
						}
					}
					
				}
				
				docTransaction.Commit();

			}
			
			
		}
	}

	public class SelectionFilter : ISelectionFilter
	{
		#region ISelectionFilter Members

		public bool AllowElement(Element elem)
		{

			if (elem.Category.Id.IntegerValue == (int)BuiltInCategory.OST_DuctCurves) return true;
			if (elem.Category.Id.IntegerValue == (int)BuiltInCategory.OST_PipeCurves) return true;

			return false;
		}

		public bool AllowReference(Reference refer, XYZ pos)
		{
			if (refer.ElementReferenceType == ElementReferenceType.REFERENCE_TYPE_NONE) return false;

			if (refer.ElementReferenceType == ElementReferenceType.REFERENCE_TYPE_SURFACE) return true;
			if (refer.ElementReferenceType == ElementReferenceType.REFERENCE_TYPE_LINEAR) return true;

			return false;
		}

		#endregion
	}

	public class FamilyOption : IFamilyLoadOptions
	{
		public bool OnFamilyFound(bool familyInUse,out bool overwriteParameterValues )
		{
			overwriteParameterValues = true;
			return true;
		}
		
		public bool OnSharedFamilyFound(Family sharedFamily,bool familyInUse, out FamilySource source,out bool overwriteParameterValues )
		{
			source = FamilySource.Family;
			overwriteParameterValues = true;
			return true;
		}
	}
}