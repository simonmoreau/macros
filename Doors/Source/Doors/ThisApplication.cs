/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 24/06/2015
 * Time: 19:32
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace Doors
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("D2DBE731-52AB-4E34-83E4-08998CED0AB2")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		
		public void CreateDoorVoid()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			string path = @"C:\Affaires\04-TGI\Portes\ErrorLog\errorLog.txt";
			
			//Retrive the reservation familly
			IEnumerable<Family> reservationFamillies = from elem in new FilteredElementCollector(_doc).OfClass(typeof(Family))
				let type = elem as Family
				where type.Name == "SYN_Reservation_Rectangulaire-M"
				select type;
			
			Family reservationFamilly = null;
			if (reservationFamillies.Count() != 0)
			{
				reservationFamilly = reservationFamillies.First();
			}
			else
			{
				throw new Exception("No reservation familly types found.");
			}
			FamilySymbol reservationFamillySymbol = _doc.GetElement(reservationFamilly.GetFamilySymbolIds().FirstOrDefault()) as FamilySymbol;
			
			
			//Find the linked doc where we find our doors
			Document d = null;
			foreach (Document tempd in _doc.Application.Documents) {
				if (tempd.PathName.Contains("ARC"))
				{
					d = tempd;
					break;
				}
			}
			
			if (null != reservationFamillySymbol)
			{
				//Find all doors in linked model
				IEnumerable<FamilyInstance > doorsEnum = from elem in new FilteredElementCollector(_doc).OfClass(typeof(FamilyInstance))
					let door = elem as FamilyInstance
					where (BuiltInCategory)door.Category.Id.IntegerValue == BuiltInCategory.OST_Doors
					select door;
				
				using (Transaction tx = new Transaction(_doc)) {
					tx.Start("Reservations");
					
					foreach (FamilyInstance door in doorsEnum) {
						
						try {
							
							if (door != null)
							{
								if (door.Host != null)
								{
									Wall hostWall = door.Host as Wall;
									XYZ orientation = hostWall.Orientation.Normalize();
									LocationCurve wallCurve = hostWall.Location as LocationCurve;
									XYZ direction = wallCurve.Curve.GetEndPoint(1) - wallCurve.Curve.GetEndPoint(0);
									LocationPoint doorLocPoint = door.Location as LocationPoint;
									
									Face face = null;
									Options geomOptions = new Options();
									geomOptions.ComputeReferences = true;
									GeometryElement wallGeom = hostWall.get_Geometry(geomOptions);

									foreach (GeometryObject geomObj in wallGeom)
									{
										Solid geomSolid = geomObj as Solid;
										if (null != geomSolid)
										{
											foreach (Face geomFace in geomSolid.Faces)
											{
												PlanarFace plFace = geomFace as PlanarFace;
												XYZ faceOrient = plFace.FaceNormal;
												if (faceOrient.IsAlmostEqualTo(orientation,0.001))
												{
													face = geomFace;
												}
											}
										}
									}
									
									//Retrive door parameters
									BoundingBoxXYZ doorBBox = door.get_BoundingBox(_doc.ActiveView);
									double doorHeight = doorBBox.Max.Z - doorBBox.Min.Z;
									
									
									XYZ location = doorLocPoint.Point + new XYZ(0,0,doorHeight/2);
									
									ElementId typeId = door.GetTypeId();
									
									//Parameter doorParam = _doc.GetElement(typeId).get_Parameter("Largeur brute");
									Parameter doorParam = door.Symbol.get_Parameter(BuiltInParameter.FAMILY_ROUGH_WIDTH_PARAM);
									//Element doorType = door.ObjectType();
									string doorWidthString = doorParam.AsValueString();
									double doorWidth = doorParam.AsDouble(); // Convert.ToDouble(doorWidthString);
									
									if (doorWidth == 0)
									{
										doorWidth = 1;
										
									}
									
									
									if (face != null)
									{
										
										
										BoundingBoxUV bboxUV = face.GetBoundingBox();
										UV center = (bboxUV.Max + bboxUV.Min) / 2.0;
										XYZ normal = face.ComputeNormal(center);
										XYZ refDir = normal.CrossProduct(XYZ.BasisZ);

										

//									//Create a new FamilyInstanceCreationData on the door
//									Autodesk.Revit.Creation.FamilyInstanceCreationData fiCreationData = new Autodesk.Revit.Creation.FamilyInstanceCreationData(face,location, refDir,reservationFamillySymbol);
//
//									//And add it to the list
//									if (null != fiCreationData)
//									{
//										fiCreationDatas.Add(fiCreationData);
//									}
										
										FamilyInstance instance = _doc.Create.NewFamilyInstance(face, location, refDir, reservationFamillySymbol);
										
										
										//Set wall thickness
										Parameter param = instance.GetParameters("SYN_Reservation_Profondeur").FirstOrDefault();
										param.Set(hostWall.Width);
										
										//Set door dimension
										param = instance.GetParameters("SYN_Reservation_Hauteur").FirstOrDefault();
										param.Set(doorHeight);
										
										//Set wall thickness
										param = instance.GetParameters("SYN_Reservation_Longueur").FirstOrDefault();
										param.Set(doorWidth);



										
									}
									else
									{
										//Print debug
									}
								}
								
							}
						}
						catch (Exception ex) {
							
							using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
							{
								sw.Write("Door ID" + door.Id + " failed ; " + ex.Message);
							}
						}
						
						
					}
					

					
					tx.Commit();
					
				}
			}
			else
			{
				throw new Exception("No reservation familly types found.");
			}
		}

		public void AddLevelOnDoor()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			//Find all doors in model
			IEnumerable<FamilyInstance > doorsEnum = from elem in new FilteredElementCollector(_doc).OfClass(typeof(FamilyInstance))
				let door = elem as FamilyInstance
				where (BuiltInCategory)door.Category.Id.IntegerValue == BuiltInCategory.OST_Doors
				select door;
			
			using (Transaction tx = new Transaction(_doc)) {
				
				tx.Start("Level Door");
				
				foreach (FamilyInstance door in doorsEnum) {
					
					
					//Get wall GOE_Etage
					Element doorHost = door.Host;
					
					if (doorHost != null)
					{
						Parameter wallParam = doorHost.GetParameters("GOE_Etage").FirstOrDefault();
						string GOE_Etage = wallParam.AsString();
						
						//Set Door GOE_Etage
						Parameter doorParam = door.GetParameters("GOE_Etage").FirstOrDefault();
						doorParam.Set(GOE_Etage);
					}

				}
				
				
				tx.Commit();
			}
		}
		
		public void AddWallTypeOnDoor()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			//Find all doors in model
			IEnumerable<FamilyInstance > doorsEnum = from elem in new FilteredElementCollector(_doc).OfClass(typeof(FamilyInstance))
				let door = elem as FamilyInstance
				where (BuiltInCategory)door.Category.Id.IntegerValue == BuiltInCategory.OST_Doors
				select door;
			
			using (Transaction tx = new Transaction(_doc)) {
				
				tx.Start("Level Door");
				
				foreach (FamilyInstance door in doorsEnum) {
					
					
					//Get wall GOE_Etage
					Element doorHost = door.Host;
					
					if (doorHost != null)
					{
						Parameter wallParam = _doc.GetElement( doorHost.GetTypeId()).GetParameters("Description").FirstOrDefault();
						string GOE_Etage = wallParam.AsString();
						
						//Set Door GOE_Etage
						Parameter doorParam = door.GetParameters("SP_TypeCloison").FirstOrDefault();
						doorParam.Set(GOE_Etage);
					}

				}
				
				
				tx.Commit();
			}
		}
	}
}