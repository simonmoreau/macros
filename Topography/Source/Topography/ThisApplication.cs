/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 02/09/2015
 * Time: 23:09
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Autodesk.Revit.DB.Architecture;
using System.IO;

namespace Topography
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("17B647A4-D6A8-4EA0-BD1B-205317DC4528")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		public void TopoToFloor()
		{
			UIDocument uidoc = this.ActiveUIDocument;
			Autodesk.Revit.DB.Document doc = uidoc.Document;

			//Create a selection filter on rooms
			ISelectionFilter filter = new CategorySelectionFilter(BuiltInCategory.OST_Topography);
			//Select a topo
			TopographySurface topographySurface = doc.GetElement(uidoc.Selection.PickObject(ObjectType.Element, filter).ElementId) as TopographySurface;
			
			using( TransactionGroup transGroup = new TransactionGroup( doc ) )
			{
				transGroup.Start("Create Floor from Toposurface" );
				
				using (Transaction tx = new Transaction(doc))
				{
					tx.Start("Create Floor from Toposurface");
					
					CurveArray curveArray = GetClosedLoop(topographySurface.GetBoundaryPoints());

					Floor floor = doc.Create.NewFloor(curveArray,false);
					
					tx.Commit();
					
					tx.Start();

					//Retrive the floor points
					SlabShapeEditor shapeEditor = floor.SlabShapeEditor;

					if (!shapeEditor.IsEnabled)
					{
						shapeEditor.Enable();
					}

					tx.Commit();

					tx.Start();

					SlabShapeVertexArray vertexArray = shapeEditor.SlabShapeVertices;

					//Set the elevation of each boundary points
					foreach (XYZ point in topographySurface.GetBoundaryPoints()) {

						XYZ tempPoint = new XYZ(point.X,point.Y,0);
						SlabShapeVertex nearestVertex = vertexArray.get_Item(0);
						double distance = tempPoint.DistanceTo(new XYZ(nearestVertex.Position.X,nearestVertex.Position.Y,0));
						//Find nearest vertex
						foreach (SlabShapeVertex vertex in vertexArray) {
							if (tempPoint.DistanceTo(new XYZ(vertex.Position.X,vertex.Position.Y,0)) < distance)
							{
								nearestVertex = vertex;
								distance = tempPoint.DistanceTo(new XYZ(vertex.Position.X,vertex.Position.Y,0));
							}
						}

						shapeEditor.ModifySubElement(nearestVertex,point.Z-nearestVertex.Position.Z);
					}

					//Create a vertex for each innner point
					foreach (XYZ point in topographySurface.GetInteriorPoints()) {
						shapeEditor.DrawPoint(point);
					}

					tx.Commit();
				}
				
				transGroup.Assimilate();
			}
		}
		
		private CurveArray GetClosedLoop(IList<XYZ> points)
		{
			CurveArray curveArray = new CurveArray();
			
			List<XYZ> boundaryPoints = new List<XYZ>(points);
			double elevation = boundaryPoints.Min(x => x.Z);
			boundaryPoints.Clear();
			
			foreach (XYZ boundaryPoint in points)
			{
				boundaryPoints.Add(new XYZ(boundaryPoint.X,boundaryPoint.Y,elevation));
				Debug.WriteLine(new XYZ(boundaryPoint.X,boundaryPoint.Y,elevation).ToString());
			}
			
			// get leftmost point
			XYZ startingPoint = boundaryPoints.Where(p => p.X == boundaryPoints.Min(min => min.X)).First();
			
			XYZ nextpoint = boundaryPoints[1];
			XYZ referenceVector = -XYZ.BasisX;
			IList<XYZ> remaningPoints= new List<XYZ>(boundaryPoints);
			remaningPoints.Remove(startingPoint);
			
			while (remaningPoints.Count > 1) {
				
				double angle = Math.Abs(referenceVector.AngleTo(nextpoint-startingPoint));
				
				for (int j = 0; j < remaningPoints.Count; j++) {
					
					if (remaningPoints[j] != startingPoint)
					{
						if (Math.Abs(referenceVector.AngleTo(remaningPoints[j]-startingPoint))< angle)
						{
							nextpoint = remaningPoints[j];
							angle = Math.Abs(referenceVector.AngleTo(remaningPoints[j]-startingPoint));
						}
					}
				}
				
				if (startingPoint.DistanceTo(nextpoint) < Application.ShortCurveTolerance)
				{
					remaningPoints.Remove(nextpoint);
					nextpoint = remaningPoints.FirstOrDefault();
				}
				else
				{
					Line line = Line.CreateBound(startingPoint,nextpoint);
					curveArray.Append(line);
					Debug.WriteLine(line.GetEndPoint(0).ToString() + ";" + line.GetEndPoint(1).ToString());
					referenceVector = nextpoint-startingPoint;
					
					remaningPoints.Remove(startingPoint);
					startingPoint = nextpoint;
					nextpoint = remaningPoints.FirstOrDefault();
				}

			}
			
			//Close the loop
			Line closingLine = Line.CreateBound(nextpoint,boundaryPoints.Where(p => p.X == boundaryPoints.Min(min => min.X)).First());
			curveArray.Append(closingLine);
			
			return curveArray;
		}
		
		public void AlignTopoToFloor()
		{
			UIDocument uidoc = this.ActiveUIDocument;
			Autodesk.Revit.DB.Document doc = uidoc.Document;

			//Create a selection filter on Topography
			ISelectionFilter filter = new CategorySelectionFilter(BuiltInCategory.OST_Topography);
			//Select a Topography
			TopographySurface topographySurface = doc.GetElement(uidoc.Selection.PickObject(ObjectType.Element, filter).ElementId) as TopographySurface;
			
			//Get the list of points for this TopographySurface
			IList<XYZ> topoPoints = topographySurface.GetPoints();
			
			//Create a selection filter on Floor
			filter = new CategorySelectionFilter(BuiltInCategory.OST_Floors);
			//Select a floor
			Floor floor = doc.GetElement(uidoc.Selection.PickObject(ObjectType.Element, filter).ElementId) as Floor;

			//Create a point list
			IList<XYZ> points = new List<XYZ>();
			
			using( TransactionGroup transGroup = new TransactionGroup( doc ) )
			{
				transGroup.Start( "Transaction Group" );
				
				using (Transaction tx = new Transaction(doc))
				{
					tx.Start("Edit shape");
					
					//Retrive the floor points
					SlabShapeEditor shapeEditor = floor.SlabShapeEditor;
					
					if (!shapeEditor.IsEnabled)
					{
						shapeEditor.Enable();
					}
					
					tx.Commit();
					
					tx.Start();
					
					SlabShapeVertexArray vertexArray = shapeEditor.SlabShapeVertices;
					
					foreach (SlabShapeVertex vertex in vertexArray) {
						if (vertex.VertexType != SlabShapeVertexType.Invalid)
						{
							points.Add(vertex.Position);
						}
					}
					
					SlabShapeCreaseArray creaseArray = shapeEditor.SlabShapeCreases;
					foreach (SlabShapeCrease crease in creaseArray) {
						if (crease.CreaseType != SlabShapeCreaseType.Invalid)
						{
							points.Add((crease.Curve.GetEndPoint(0) + crease.Curve.GetEndPoint(1))/2);
						}
					}
					
					shapeEditor.ResetSlabShape();
					
					tx.Commit();
				}

				transGroup.RollBack();

			}

			using (TopographyEditScope topoEditScope = new TopographyEditScope(doc,"Align Toposurface to Floor"))
			{
				topoEditScope.Start(topographySurface.Id);

				using (Transaction tx = new Transaction(doc))
				{
					tx.Start("Align Toposurface to Floor");
					
					topographySurface.AddPoints(points);

					tx.Commit();
				}
				
				topoEditScope.Commit(new myFailuresPreprocessor());

			}
		}

		private XYZ IsPointPermited(XYZ inputPoint)
		{
			XYZ outputPoint = new XYZ();
			
			return inputPoint;
		}
		
		public void TopoToCSV()
		{
			
			UIDocument uidoc = this.ActiveUIDocument;
			Autodesk.Revit.DB.Document doc = uidoc.Document;

			//Create a selection filter on Topography
			ISelectionFilter filter = new CategorySelectionFilter(BuiltInCategory.OST_Topography);
			//Select a Topography
			TopographySurface topographySurface = doc.GetElement(uidoc.Selection.PickObject(ObjectType.Element, filter).ElementId) as TopographySurface;
			
			//Get the list of points for this TopographySurface
			IList<XYZ> topoPoints = topographySurface.GetPoints();
			
			//Create a string list to export
			List<string> pointsList = new List<string>();
			double feet = 0.3048;
			
			foreach (XYZ point in topoPoints)
			{
				//point = UnitUtils.Convert(point, DisplayUnitType.DUT_DECIMAL_FEET, DisplayUnitType.DUT_METERS);
				XYZ meterPoint = point * feet;
				pointsList.Add(meterPoint.ToString().Remove(0,1));
			}
			
			string path = @"C:\Google Drive\05 - Travail\Option BIM ESTP\Le BIM en Conception\Model\MNT.csv";
			File.WriteAllLines(path,pointsList.ToArray());
		}
		
		public void AlignFloorToFloor()
		{
			UIDocument uidoc = this.ActiveUIDocument;
			Autodesk.Revit.DB.Document doc = uidoc.Document;
			
			if (uidoc.ActiveView.ViewType == ViewType.ThreeD)
			{
				View3D activeView = uidoc.ActiveView as View3D;
				
				//Create a selection filter on Topography
				ISelectionFilter filter = new CategorySelectionFilter(BuiltInCategory.OST_Floors);
				
				//Select a floor to be edited
				Floor editedFloor = doc.GetElement(uidoc.Selection.PickObject(ObjectType.Element, filter).ElementId) as Floor;
				
				//Select a reference floor
				Floor referenceFloor = doc.GetElement(uidoc.Selection.PickObject(ObjectType.Element, filter).ElementId) as Floor;

				//Create my reference intersector on the reference floor
				ReferenceIntersector refIntersector = new ReferenceIntersector(referenceFloor.Id, FindReferenceTarget.Element, activeView);
				
				//create a direction for the reference intersector
				XYZ direction = XYZ.BasisZ;
				
				using( TransactionGroup transGroup = new TransactionGroup( doc ) )
				{
					transGroup.Start( "Edit Floor shape" );
					
					using (Transaction tx = new Transaction(doc))
					{
						tx.Start("Edit shape");
						
						//Retrive the floor points
						SlabShapeEditor shapeEditor = editedFloor.SlabShapeEditor;
						
						if (!shapeEditor.IsEnabled)
						{
							shapeEditor.Enable();
						}
						
						tx.Commit();
						
						tx.Start();
						
						SlabShapeVertexArray vertexArray = shapeEditor.SlabShapeVertices;
						
						foreach (SlabShapeVertex vertex in vertexArray) {
							
							if (vertex.VertexType != SlabShapeVertexType.Invalid)
							{
								//Fire down or up
								ReferenceWithContext referencesWithContextPositive = refIntersector.FindNearest(vertex.Position, direction);
								ReferenceWithContext referencesWithContextNegative = refIntersector.FindNearest(vertex.Position, -direction);
								Reference reference = null;
								
								if (referencesWithContextPositive != null)
								{
									reference = referencesWithContextPositive.GetReference();
								}
								else if (referencesWithContextNegative != null)
								{
									reference = referencesWithContextNegative.GetReference();
								}
								
								if (reference != null)
								{
									XYZ hitPoint = reference.GlobalPoint;
									
									shapeEditor.ModifySubElement(vertex,hitPoint.Z-vertex.Position.Z);
								}
							}
						}
						
						tx.Commit();
					}

					transGroup.Commit();

				}
			}
			else
			{
				//please launch this command in a 3D view
			}

		}
	}

	public class myFailuresPreprocessor : IFailuresPreprocessor
	{
		public FailureProcessingResult PreprocessFailures(FailuresAccessor failuresAccessor)
		{
			return FailureProcessingResult.Continue;
		}
	}

	public class CategorySelectionFilter : ISelectionFilter
	{
		private BuiltInCategory _builtInCat;

		public CategorySelectionFilter(BuiltInCategory builtInCat)
		{
			_builtInCat = builtInCat;
		}

		public bool AllowElement(Element element)
		{
			if (element.Category.Id.IntegerValue == (int)_builtInCat)
			{
				return true;
			}
			return false;
		}

		public bool AllowReference(Reference refer, XYZ point)
		{
			return false;
		}
	}
}