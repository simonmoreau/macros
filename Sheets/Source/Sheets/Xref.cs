﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: moreaus
 * Date: 18/12/2013
 * Heure: 15:01
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Mechanical;
using System.IO;
using Autodesk.Revit;


namespace Sheets
{
	/// <summary>
	/// Description of Xref.
	/// </summary>
	public class Xref
	{

		private string _path;
		public string Path
		{
			get { return _path; }
		}

		private string _name;
		public string Name
		{
			get { return _name; }
		}
		
		private string _receptionDate;
		public string ReceptionDate
		{
			get {return _receptionDate;}
		}
		
		private string _phase;
		public string Phase
		{
			get {return _phase;}
		}
		
				private string _indice;
		public string Indice
		{
			get {return _indice;}
		}
		
		public Xref(string Path, string Name, string ReceptionDate,string Phase,string Indice)
		{
			_path = Path;
			_name = Name;
			_receptionDate = ReceptionDate;
			_phase = Phase;
			_indice = Indice;
		}
		
	}
	
	public class LinkedXrefs
	{
		private List<Xref> _Xrefs;
		public List<Xref> Xrefs
		{
			get { return _Xrefs; }
		}
		
		private string _path;
		public string Path
		{
			get { return _path; }
		}
		
		public LinkedXrefs(string path)
		{
			_path = path;
			_Xrefs = new List<Xref>();
			
			//Get the cartouche.txt file
			string[] cartoucheTxtLines = File.ReadAllLines(path);

			string standardxreflist = null;
			string document_xref = null;

			//Loop on all lines
			foreach (string cartoucheTxtLine in cartoucheTxtLines)
			{
				if (cartoucheTxtLine.Contains("STANDARDXREFLIST:"))
				{
					standardxreflist = cartoucheTxtLine.Replace("STANDARDXREFLIST: ", "");
				}
				else if (cartoucheTxtLine.Contains("DOCUMENT_XREF :"))
				{
					document_xref = cartoucheTxtLine.Replace("DOCUMENT_XREF : ", "");
				}
			}

			if (standardxreflist != null)
			{
				//Retrive file path and Agora name
				string[] dwgPaths = standardxreflist.Split(';');
				string[] dwgNames;
				
				if (document_xref != null)
				{
					dwgNames = document_xref.Split(';');
				}
				else
				{
					dwgNames = new string[dwgPaths.Length];
				}
				
				for (int i = 0; i < dwgPaths.Length; i++) {
					
					string date = Tools.FieldFromPath(dwgPaths[i],8);
					date = date.Substring(4,date.Length -4);
					
					string phase = Tools.FieldFromPath(dwgPaths[i],6);
					
					string indice = Tools.FieldFromPath(dwgPaths[i],7);
					
					_Xrefs.Add(new Xref(dwgPaths[i],dwgNames[i],date,phase,indice));
				}
			}
		}
	}
}