/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 24/06/2015
 * Time: 18:54
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace Sheets
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("72653F55-6B9A-4393-BB35-900954740224")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		
		public void BatchPrint()
		{

			
			Document document = this.ActiveUIDocument.Document;
			
			//Find all sheets
			IEnumerable<ViewSheet> sheets = from elem in new FilteredElementCollector(document).OfClass(typeof(ViewSheet))
				let sheet = elem as ViewSheet
				where sheet.Name.Contains("nom")
				select sheet;
			
			PrintManager pMgr  = document.PrintManager;
			
			//Autodesk.Revit.DB.Transaction newTran = new Autodesk.Revit.DB.Transaction(this.ActiveUIDocument.Document, "ViewPrinter");
			//newTran.Start();
			
			pMgr.PrintRange = PrintRange.Select;

			ViewSet printedView = new ViewSet();
			
			foreach (ViewSheet sheet in sheets) {
				printedView.Insert(sheet);
			}
			ViewSheetSetting m_viewSheetSetting = pMgr.ViewSheetSetting;
			
			IViewSheetSet viewSheetSet = m_viewSheetSetting.CurrentViewSheetSet;
			viewSheetSet.Views = printedView;
			m_viewSheetSetting.Save();
			
			pMgr.SubmitPrint();
			
		}
		
		public void DrawingNumberAnnotation()
		{
			Document doc = this.ActiveUIDocument.Document;
			string modelName = Path.GetFileNameWithoutExtension(doc.PathName);
			modelName = modelName.Substring(0,11);
			
			//Find all Drawing limit annotations
			
			List<AnnotationSymbol> ann= new List<AnnotationSymbol>();

			ElementClassFilter filter1= new ElementClassFilter(typeof( FamilyInstance ) );

			FilteredElementCollector coll1= new FilteredElementCollector( doc );

			coll1.WherePasses( filter1 );

			foreach( Element el in coll1 )
			{
				AnnotationSymbol anns= el as AnnotationSymbol;

				if( anns != null )
				{
					if (anns.Name == "Limite de porte")
					{
						ann.Add( el as AnnotationSymbol );
					}
				}
			}
			
			//Get the drawing list into two dictionary
			string drawingNamePath = @"P:\!COMMUN\$3D\AIG\06-ORGANISATION\AIG-Nomenclatures\AnnotationLimiteDeVue\ViewList.txt";
			string[] viewMatching = System.IO.File.ReadAllLines(drawingNamePath);
			
			Dictionary<string, string> leftDrawing = new Dictionary<string, string>();
			Dictionary<string, string> rightDrawing = new Dictionary<string, string>();
			
			
			foreach (string line in viewMatching) {
				string[] splitedLine = line.Split(';');
				if (splitedLine[0].Substring(0,11) == modelName)
				{
					leftDrawing.Add(splitedLine[1],splitedLine[2]);
					rightDrawing.Add(splitedLine[1],splitedLine[3]);
				}

			}
			
			//Create the log file
			string logPath = @"P:\!COMMUN\$3D\AIG\06-ORGANISATION\AIG-Nomenclatures\AnnotationLimiteDeVue\AnnotationLog.txt";
			List<string> logLines = new List<string>();
			logLines.AddRange(File.ReadAllLines(logPath).ToList());
			
			//Create the Transaction
			using (Transaction tx = new Transaction(doc))
			{

				tx.Start("Add Drawing Number Label");
				
				//Loop on each annotation the set the drawing label
				foreach (AnnotationSymbol drawingLimit in ann)
				{
					//Get the current view name
					View currentView = doc.GetElement(drawingLimit.OwnerViewId) as View;
					
					//Get the "Gauche" property
					Parameter sideParam = drawingLimit.GetParameters("Gauche").FirstOrDefault();
					int side = sideParam.AsInteger();
					
					//Get the "Libellé" property
					Parameter labelParam = drawingLimit.GetParameters("Libellé").FirstOrDefault();
					
					//Declare the side drawing name
					string sideDrawingName;
					
					if (side ==0)
					{
						if (rightDrawing.ContainsKey(currentView.Name))
						{
							sideDrawingName = rightDrawing[currentView.Name];
							labelParam.Set(sideDrawingName);
							logLines.Add(currentView.Name + ";Right;"+sideDrawingName+";"+modelName);
						}
						else
						{
							labelParam.Set("A saisir manuellement");
							logLines.Add(currentView.Name + ";Right;Key Not Found"+";"+modelName);
						}
					}
					else if (side == 1)
					{
						if (leftDrawing.ContainsKey(currentView.Name))
						{
							sideDrawingName = leftDrawing[currentView.Name];
							labelParam.Set(sideDrawingName);
							logLines.Add(currentView.Name + ";Left;"+sideDrawingName+";"+modelName);
						}
						else
						{
							labelParam.Set("A saisir manuellement");
							logLines.Add(currentView.Name + ";Left;Key Not Found"+";"+modelName);
						}
					}
					else
					{
						labelParam.Set("A saisir manuellement");
						logLines.Add(currentView.Name + ";Side Not Found;Key Not Found"+";"+modelName);
					}
				}
				
				tx.Commit();
			}
			
			//Write the log file
			File.WriteAllLines(logPath,logLines);
		}
		
		public void InserFromAgora()
		{
			//Get the config file for the current project
			string configFile = Tools.GetFilePath("txt");
			
			string[] configFileLines = File.ReadAllLines(configFile, Encoding.GetEncoding("iso-8859-1"));
			
			foreach (string line in configFileLines) {

				if (line.Length >2 ){
					if (line.Substring(0,2) != "**" & line.Contains(';'))
					{
						string[] currentModelPaths = line.Split(';');
						
						if (currentModelPaths.Length == 3) {
							
							//Get the location of the document
							ModelPath activeModelPath = ModelPathUtils.ConvertUserVisiblePathToModelPath(currentModelPaths[0]);
							
							if (activeModelPath.IsValidObject == true)
							{
								//Get the cartouche.txt file
								string[] cartoucheTxtLines = File.ReadAllLines(currentModelPaths[1]);
								
								string standardxreflist = null;
								string document_xref = null;
								
								//Loop on all lines
								foreach (string cartoucheTxtLine in cartoucheTxtLines) {
									if (cartoucheTxtLine.Contains("STANDARDXREFLIST:"))
									{
										standardxreflist = cartoucheTxtLine.Replace("STANDARDXREFLIST: ","");
									}
									else if (line.Contains("DOCUMENT_XREF :") )
									{
										document_xref = cartoucheTxtLine.Replace("DOCUMENT_XREF : ","") ;
									}
								}
								
								if (standardxreflist != null & document_xref!=null)
								{
									//Retrive file path and Agora name
									string[] dwgPaths = standardxreflist.Split(';');
									string[] dwgNames = document_xref.Split(';');
									
									// access transmission data in the given Revit file
									TransmissionData transData = TransmissionData.ReadTransmissionData(activeModelPath);

									if (transData != null)
									{
										// collect all (immediate) external references in the model
										ICollection<ElementId> externalReferences = transData.GetAllExternalFileReferenceIds();

										// find every reference that is a link
										foreach (ElementId refId in externalReferences)
										{
											ExternalFileReference extRef = transData.GetLastSavedReferenceData(refId);
											string ExtRefLevel = null;
											string ExtRefTrade = null;

											if (extRef.ExternalFileReferenceType == ExternalFileReferenceType.CADLink)
											{
												string path = ModelPathUtils.ConvertModelPathToUserVisiblePath(extRef.GetAbsolutePath());
												
												//Solve in case of a dummy dwg
												if (Path.GetFileNameWithoutExtension(path).Substring(0,10) == "INGP-TEMP_")
												{
													//get the level code
													ExtRefLevel = Path.GetFileNameWithoutExtension(path).Split('_')[2];
													
													//get the trade
													ExtRefTrade = Path.GetFileNameWithoutExtension(path).Split('_')[1];
												}
												//Solve the case of Agora file
												else
												{
													//get the level code
													ExtRefLevel = Tools.FieldFromPath(path,3);
													
													//get the trade
													ExtRefTrade = Tools.FieldFromPath(path,1);
												}
												
												ModelPath dwgModelPath = null;
												
												foreach (string dwgPath in dwgPaths) {
													string dwgLevel = Tools.FieldFromPath(dwgPath.Replace('/','\\'),3);
													string dwgTrade = Tools.FieldFromPath(dwgPath.Replace('/','\\'),1);
													
													if (dwgLevel == ExtRefLevel & dwgTrade == ExtRefTrade) {
														//Get the dwg model path
														dwgModelPath = ModelPathUtils.ConvertUserVisiblePathToModelPath(dwgPath);
													}
												}
												
												if (dwgModelPath != null) {
													transData.SetDesiredReferenceData(refId,dwgModelPath, PathType.Relative,true);
												}
											}
										}

										// make sure the IsTransmitted property is set
										transData.IsTransmitted = true;

										// modified transmission data must be saved back to the model
										TransmissionData.WriteTransmissionData(activeModelPath, transData);
									}
									else
									{
										Autodesk.Revit.UI.TaskDialog.Show("Chargement des liens Agora", "The document does not have any transmission data");
									}
								}
								else
								{
									Autodesk.Revit.UI.TaskDialog.Show("Chargement des liens Agora", "Le chemin ne dirige pas vers un fichier cartouche.txt valide :" + currentModelPaths[1]);
								}
							}
							else
							{
								Autodesk.Revit.UI.TaskDialog.Show("Chargement des liens Agora", "Le chemin ne dirige pas vers un fichier Revit valide :" + currentModelPaths[0]);
							}

						}
					}
				}

			}


		}
		
		
		public void XrefList()
		{
			//Get the current doc
			Document doc = this.ActiveUIDocument.Document;
			
			//Get a plan view
			IEnumerable<ViewPlan> planViews = from elem in new FilteredElementCollector(doc).OfClass(typeof(ViewPlan))
				let view = elem as ViewPlan
				select view;
			ViewPlan planView = planViews.First();
			
			using( Transaction tx = new Transaction( doc ) )
			{
				tx.Start( "List Xref" );
				
				//Get the Xref Family
				Family xRefSymbol;
				// Retrieve the family if it is already present:
				IEnumerable<Family> xRefSymbols = from elem in new FilteredElementCollector(doc).OfClass(typeof(Family))
					let type = elem as Family
					where type.Name == "INGP_Xref"
					select type;
				
				if (xRefSymbols.Count() == 0)
				{
					//Extract the familly to a temp location
					string tempPath =System.IO.Path.GetTempPath();
					string FamilyPath = Path.Combine(tempPath,"INGP_Xref.rfa");
					
					if (!File.Exists(FamilyPath))
					{
						//extract the familly
						List<string> files = new List<string>();
						files.Add("INGP_Xref.rfa");
						//Tools.ExtractEmbeddedResource(tempPath,"DecodeTools.Icons",files);
					}
					
					// Load family from file:
					doc.LoadFamily( FamilyPath, out xRefSymbol );
				}
				else
				{
					xRefSymbol = xRefSymbols.First();
				}
				
				//Retrive the associated type
				FamilySymbol xRefSymbolType = null;
				foreach (ElementId symbolTypeId in xRefSymbol.GetFamilySymbolIds()) {
					
					xRefSymbolType = doc.GetElement(symbolTypeId) as FamilySymbol;
				}
				
				
				//Get the config file for the current project
				string cartoucheTxt = Tools.GetFilePath("txt");
				
				//Loop on all dwg
				LinkedXrefs linkedFiles = new LinkedXrefs(cartoucheTxt);
				ICollection<ElementId> xRefElementsIds = new List<ElementId>();
				
				if (linkedFiles.Xrefs.Count != 0)
				{
					foreach (Xref xref in linkedFiles.Xrefs) {
						//Insert it in the plan view
						FamilyInstance currentXrefSymbol = doc.Create.NewFamilyInstance(new XYZ(0,0,0),xRefSymbolType,planView);
						
						Parameter nameParam = currentXrefSymbol.GetParameters("Nom").FirstOrDefault();
						nameParam.Set(xref.Name);
						
						Parameter dateParam = currentXrefSymbol.GetParameters("Reçu le").FirstOrDefault();
						dateParam.Set(xref.ReceptionDate);
						
						Parameter phaseParam = currentXrefSymbol.GetParameters("Phase").FirstOrDefault();
						phaseParam.Set(xref.Phase);
						
						Parameter indiceParam = currentXrefSymbol.GetParameters("Indice").FirstOrDefault();
						indiceParam.Set(xref.Indice);
						
						xRefElementsIds.Add(currentXrefSymbol.Id);
					}
					
					//Hide Symbol in view
					planView.HideElements(xRefElementsIds);
					
					//Create a Note Blocks list
					ViewSchedule xRefListView = ViewSchedule.CreateNoteBlock(doc,xRefSymbol.Id);
					xRefListView.Name = "Liste des Xref";
					
					IList<SchedulableField> schedulableFields = null;
					
					//Get all schedulable fields from view schedule definition.
					schedulableFields = xRefListView.Definition.GetSchedulableFields();

					foreach (SchedulableField sf in schedulableFields)
					{
						bool fieldAlreadyAdded = false;
						//Get all schedule field ids
						IList<ScheduleFieldId> ids = xRefListView.Definition.GetFieldOrder();
						foreach (ScheduleFieldId id in ids)
						{
							//If the GetSchedulableField() method of gotten schedule field returns same schedulable field,
							// it means the field is already added to the view schedule.
							if (xRefListView.Definition.GetField(id).GetSchedulableField() == sf)
							{
								fieldAlreadyAdded = true;
								break;
							}
						}

						//If schedulable field doesn't exist in view schedule, add it.
						if (fieldAlreadyAdded == false)
						{
							xRefListView.Definition.AddField(sf);
						}
					}
				}
				
				tx.Commit();
			}
		}
		
		public void SaveAgora()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//Get the central file
			ModelPath mp = doc.GetWorksharingCentralModelPath();
			string worksharedname = ModelPathUtils.ConvertModelPathToUserVisiblePath(mp);
			
			//Copy it alongside
			string worksharednameTemp = Path.GetDirectoryName(worksharedname)+@"\"+Path.GetFileNameWithoutExtension(worksharedname)+"_temp"+Path.GetExtension(worksharedname);
			File.Copy(worksharedname,worksharednameTemp);
			
			//Open it detached
			ModelPath revitPath = ModelPathUtils.ConvertUserVisiblePathToModelPath(worksharednameTemp);
			
			OpenOptions opOptions = new OpenOptions();
			opOptions.DetachFromCentralOption = DetachFromCentralOption.DetachAndPreserveWorksets;
			
			Document document = this.Application.OpenDocumentFile(revitPath,opOptions);
			
			SaveAsOptions opt = new SaveAsOptions();
			opt.OverwriteExistingFile = true;
			
			document.SaveAs(worksharedname,opt);
			document.Close(false);
		}
		public void SheetProperties()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//Find all plan view
			IEnumerable<ViewPlan> viewsPlan = from elem in new FilteredElementCollector(doc).OfClass(typeof(ViewPlan))
				let view = elem as ViewPlan
				where view.IsTemplate == false
				select view;
			
			//Find all linked model id
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			List<ElementId> linkIds = collector.OfCategory(BuiltInCategory.OST_RvtLinks).WhereElementIsNotElementType().ToElementIds().ToList();
			
			//Create a list to export
			List<string> lines  = new List<string>();
			string firstLine = "Model.Name;View.Name;View.INGP-Statut;View.INGP-Groupe;Template.Name;CropBox.IsActive;CropBox.IsVisible;CropBox.Name;"+
				"ViewRange.TopName;ViewRange.TopOffSet;ViewRange.CutName;ViewRange.CutOffset;ViewRange.BottomName;ViewRange.BottomOffset;ViewRange.ViewDepthName;ViewRange.ViewDepthOffset;"+
				"Sheet.Number;Sheet.Name;Link.Name;Link.IsVisible;Link.Halftone";
			
			lines.Add(firstLine);
			
			
			foreach (ViewPlan viewPlan in viewsPlan) {
				
				//Get view status
				string status = ParameterValue("INGP-Statut",viewPlan);
				if (status == "Rendu")
				{
					//Template
					string templateValues = "";
					if (viewPlan.ViewTemplateId.IntegerValue != -1)
					{
						View viewTemplate = doc.GetElement(viewPlan.ViewTemplateId) as View;
						templateValues = string.Format("{0}",viewTemplate.ViewName);
					}
					else
					{
						templateValues = string.Format("{0}","No Template");
					}
					
					//Project browser
					status = ParameterValue("INGP-Statut",viewPlan) + ";" + ParameterValue("INGP-Groupe",viewPlan);
					
					
					//section box
					string sectionBoxValues = "";
					if (viewPlan.CropBoxActive == true)
					{
						sectionBoxValues = string.Format("{0};{1};{2}",
						                                 viewPlan.CropBoxActive.ToString(),
						                                 viewPlan.CropBoxVisible.ToString(),
						                                 ParameterValue(BuiltInParameter.VIEWER_VOLUME_OF_INTEREST_CROP,viewPlan));
					}
					else
					{
						sectionBoxValues = string.Format("{0};{1};{2}",
						                                 viewPlan.CropBoxActive.ToString(),
						                                 viewPlan.CropBoxVisible.ToString(),
						                                 "No Crop");
					}
					
					//View Range
					string viewRangeValues = "";
					PlanViewRange viewRange = viewPlan.GetViewRange();

					string top = GetRangeLevelValues(doc,viewRange, PlanViewPlane.TopClipPlane);
					string cut = GetRangeLevelValues(doc,viewRange, PlanViewPlane.CutPlane);
					string bottom = GetRangeLevelValues(doc,viewRange, PlanViewPlane.BottomClipPlane);
					string depth = GetRangeLevelValues(doc,viewRange, PlanViewPlane.ViewDepthPlane);
					
					
					viewRangeValues = string.Format("{0};{1};{2};{3}",
					                                top,
					                                cut,
					                                bottom,
					                                depth);

					//Sheet
					string sheetValues = "";
					sheetValues = string.Format("{0};{1}",
					                            ParameterValue(BuiltInParameter.VIEWPORT_SHEET_NUMBER,viewPlan),
					                            ParameterValue(BuiltInParameter.VIEWPORT_SHEET_NAME,viewPlan)
					                           );
					


					
					//Linked model
					foreach (ElementId id in linkIds) {
						
						string linkValues = "";
						Element elem = doc.GetElement(id);
						OverrideGraphicSettings overrides = viewPlan.GetElementOverrides(id);
						
						
						linkValues = string.Format("{0};{1};{2}",
						                           elem.Name,
						                           (!elem.IsHidden(viewPlan)).ToString(),
						                           overrides.Halftone.ToString());
						
						//Concatenation
						string line = string.Format("{0};{1};{2};{3};{4};{5};{6};{7}",
						                            Path.GetFileNameWithoutExtension(doc.PathName),
						                            viewPlan.Name,
						                            status,
						                            templateValues,
						                            sectionBoxValues,
						                            viewRangeValues,
						                            sheetValues,
						                            linkValues);
						
						lines.Add(line);
					}
				}
			}
			
			string exportPath = @"C:\Affaires\01-Developpement\DataMining\ViewTemplates\ViewTemplates_" + Path.GetFileNameWithoutExtension(doc.PathName) + ".csv";
			File.WriteAllLines(exportPath,lines.ToArray(),Encoding.UTF8);
			
		}
		
		private string GetRangeLevelValues(Document doc,PlanViewRange range,PlanViewPlane plane)
		{
			ElementId planeId = range.GetLevelId(plane);
			
			if (planeId.IntegerValue != -1)
			{
				Element level = doc.GetElement(planeId);
				double offset = range.GetOffset(plane);
				Units unit = new Units(UnitSystem.Metric);
				
				return level.Name + ";" + UnitFormatUtils.Format(unit,UnitType.UT_Length,offset,false,false);
			}
			else
			{
				return "No Level;000";
			}
		}
		
		private string ParameterValue(string paramName, Element element)
		{
			Parameter param = element.GetParameters(paramName).FirstOrDefault();
			return param.AsString();
		}
		
		private string ParameterValue(BuiltInParameter BIparam, Element element)
		{
			Parameter param = element.get_Parameter(BIparam);// .FirstOrDefault();
			FormatOptions opt = new FormatOptions();
			if (param.AsValueString() != null)
			{
				return param.AsValueString();
			}
			else
			{
				return param.AsString();
			}
			
		}
	}
}