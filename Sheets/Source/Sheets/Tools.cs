﻿/*
 * Revit Macro created by SharpDevelop
 * Utilisateur: moreaus
 * Date: 12/12/2013
 * Heure: 13:30
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using DB = Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using System.IO;

namespace Sheets
{
	class Tools
	{
		public static double ConverttoM(double feet)
		{
			return feet * 0.0254;
		}

		public static double ConvertFromM(double meter)
		{
			return meter * 3.28084;
		}

		public static string GetFilePath(string extension)
		{
			string myStream = null;
			OpenFileDialog openFileDialog1 = new OpenFileDialog();
			string filter;

			if (extension == "txt")
			{
				filter = "Fichier Texte (*.txt)|*.txt|All files (*.*)|*.*";
			}
			else if (extension == "bcfzip")
			{
				filter = "BCF files (*.bcfzip)|*.bcfzip|All files (*.*)|*.*";
			}
			else
			{
				filter = "All files (*.*)|*.*";
			}

			openFileDialog1.InitialDirectory = "c:\\";
			openFileDialog1.Filter = filter;
			openFileDialog1.FilterIndex = 0;
			openFileDialog1.Title = "Ouvrir un fichier de coordonnées de vues 3D";
			openFileDialog1.RestoreDirectory = true;

			//C:\Agora\GIZ0005-TEST ET FORMATIONS\XREF\FOR_FRM\ARC\FOR_FRM_XREF_APD_ARC_PLN_00001
			
			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				if ((myStream = openFileDialog1.FileName) != null)
				{
					return myStream;
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		public static string SaveFilePath()
		{
			// Displays a SaveFileDialog so the user can save the Image
			// assigned to Button2.
			SaveFileDialog saveFileDialog1 = new SaveFileDialog();
			saveFileDialog1.Filter = "Fichier RVD (*.rvd)|*.rvd|All files (*.*)|*.*";
			saveFileDialog1.FilterIndex = 0;
			saveFileDialog1.Title = "Sauvegarder les coordonnées des vues 3D";

			// If the file name is not an empty string open it for saving.
			if (saveFileDialog1.ShowDialog() == DialogResult.OK)
			{
				if (saveFileDialog1.FileName != "")
				{
					return saveFileDialog1.FileName;
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}

		}

		public static List<string> ListViewsNames(DB.Document doc)
		{
			//Get ViewList
			IEnumerable<DB.View> views = from elem in new DB.FilteredElementCollector(doc).OfClass(typeof(DB.View))
				let type = elem as DB.View
				select type;

			List<string> viewsNames = new List<string>();

			foreach (DB.View view in views)
			{
				viewsNames.Add(view.Name);
			}

			return viewsNames;
		}

		public static string Rename(ref List<string> names, string input)
		{
			string pattern = @"\(\d{1,3}\)$";

			System.Text.RegularExpressions.Regex replacmentPattern = new System.Text.RegularExpressions.Regex("[]{}:|;<>?'~]");
			input = replacmentPattern.Replace(input, "_").Replace("[", "_"); ;

			if (names.Contains(input) == false)
			{
				names.Add(input);
				return input;
			}
			else
			{
				if (System.Text.RegularExpressions.Regex.IsMatch(input, pattern))
				{
					int num = Convert.ToInt16(System.Text.RegularExpressions.Regex.Match(input, pattern).Value.Replace("(", "").Replace(")", "")) + 1;
					return Rename(ref names, input.Substring(0, input.Length - 3) + "(" + num + ")");
				}
				else
				{
					return Rename(ref names, input + "(1)");
				}
			}
		}
		
		public static string FieldFromPath(string fullPath, int rank)
		{
			string directoryName = Path.GetFileName(Path.GetDirectoryName(fullPath));
			if (directoryName.Contains('_'))
			{
				string[] fieldAgora = directoryName.Split('_');
				if (fieldAgora.Length > 4)
				{
					return fieldAgora[rank];
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}
		
		public static void ExtractEmbeddedResource(string outputDir, string resourceLocation, List<string> files)
		{
			foreach (string file in files)
			{
				using (System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceLocation + @"." + file))
				{
					using (System.IO.FileStream fileStream = new System.IO.FileStream(System.IO.Path.Combine(outputDir, file), System.IO.FileMode.Create))
					{
						for (int i = 0; i < stream.Length; i++)
						{
							fileStream.WriteByte((byte)stream.ReadByte());
						}
						fileStream.Close();
					}
				}
			}
		}
	}
}
