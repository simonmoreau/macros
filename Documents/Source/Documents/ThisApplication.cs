﻿/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 24/06/2015
 * Time: 18:50
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using Autodesk.Revit.DB.Analysis;

namespace Documents
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("F4F8200D-6F5A-4D85-8F56-339431B7D141")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		
		
		
		
		public void BatchConverter()
		{
			
			string[] revitFiles = Directory.GetFiles(@"S:\BB1457-AIG\00-CENTRAL\50-ELE","*.rvt",SearchOption.AllDirectories);
//			string[] revitFiles = new string[1];
//			revitFiles[0] = @"S:\BB1457-AIG\00-CENTRAL\10-ARC\ARC-ZDEF-A13.rvt";
			
			Autodesk.Revit.ApplicationServices.Application app = this.Application;
			
			OpenOptions opt = new OpenOptions();
			
			opt.Audit = true;
			opt.DetachFromCentralOption = DetachFromCentralOption.DetachAndPreserveWorksets;
			
			SaveAsOptions optSave = new SaveAsOptions();
			optSave.MaximumBackups = 3;
			optSave.OverwriteExistingFile = true;
			optSave.Compact = true;
			
			//Set worksharing option
			WorksharingSaveAsOptions workSharingOpt = new WorksharingSaveAsOptions();
			workSharingOpt.SaveAsCentral = true;
			workSharingOpt.OpenWorksetsDefault = SimpleWorksetConfiguration.AskUserToSpecify;
			workSharingOpt.ClearTransmitted = false;
			optSave.SetWorksharingOptions(workSharingOpt);
			
			//Create log file
			List<string> logFile = new List<string>();
			
			foreach (string filePath in revitFiles){
				
				try {
					if (filePath != @"S:\BB1457-AIG\00-CENTRAL\10-ARC\ARC-ZDEF-A13.rvt" | filePath != @"S:\BB1457-AIG\00-CENTRAL\10-ARC\LIGNE ENTRE ORIGINES MODELE ET AEROPORT.rvt")
					{
						ModelPath revitModelPath = ModelPathUtils.ConvertUserVisiblePathToModelPath(filePath);

						Document currentDoc = app.OpenDocumentFile(revitModelPath,opt);
						
						//Find the loadingView;
						IEnumerable<ViewDrafting> ModelViews = from elem in new FilteredElementCollector(currentDoc).OfClass(typeof(ViewDrafting))
							let view = elem as ViewDrafting
							select view;
						
						if (ModelViews.Count() != 0)
						{
							ElementId viewId = null;
							
							foreach (ViewDrafting view in ModelViews) {
								if (view.Name.ToLowerInvariant().Contains("enregistre"))
								{
									viewId = view.Id;
								}
								else if (view.Name.ToLowerInvariant().Contains("démarrage"))
								{
									viewId = view.Id;
								}
								else if (view.Name.ToLowerInvariant().Contains("demarrage"))
								{
									viewId = view.Id;
								}
							}
							
							if (viewId == null)
							{
								viewId = ModelViews.First().Id;
							}
							
							optSave.PreviewViewId = viewId;
						}
						
						deleteImportCategories(currentDoc);

						currentDoc.SaveAs(revitModelPath,optSave);
						
						currentDoc.Close();
					}
				}
				catch (Exception ex)
				{
					
					logFile.Add(string.Format("The file {0} has not been proceced, due to " + ex.Message,filePath));
				}
			}
			
			
			File.WriteAllLines(@"C:\Affaires\01-AIG\Revit\LogExports.txt",logFile.ToArray());
		}
		
		public void MoveElements()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			//Current View Id
			ElementId currentViewId = _doc.ActiveView.Id;
			
			//Retrive all element in view
			ICollection<ElementId> viewElementIds = new FilteredElementCollector(_doc,currentViewId).OfCategory(BuiltInCategory.OST_Walls).ToElementIds();
			
			//Create an move vector
			//XYZ transform = new XYZ(0,0,-0.25*3.28084);
			
			
			//Create an empty transformation
			Transform transform = Transform.CreateTranslation(new XYZ(0,0,0.25*3.28084));
			
			
			//Create copy Options
			CopyPasteOptions opt = new CopyPasteOptions();
			
			using (Transaction tx = new Transaction(_doc)) {
				tx.Start("Custom Move");
				
				
				ElementTransformUtils.CopyElements(_doc,viewElementIds,_doc,transform, opt);

				tx.Commit();
			}
			
		}
		
		public void CopyPasteElements()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			//Current View Id
			ElementId currentViewId = _doc.ActiveView.Id;
			
			//Retrive all element in view
			ICollection<ElementId> viewElementIds = new FilteredElementCollector(_doc,currentViewId).OfCategory(BuiltInCategory.OST_Walls).ToElementIds();
			
			//Retrive final doc
			Document resultDoc = null;
			
			foreach (Document linkedDoc in this.Application.Documents) {
				
				// Find all rooms in current doc
				if (linkedDoc.PathName.Contains("INGP_PJP_SYN_RESTO_MAC_CLO_smoreau"))
				{
					resultDoc = linkedDoc;
					break;
				}
			}
			
			if (resultDoc != null)
			{
				
				//Create an empty transformation
				Transform transform = Transform.CreateTranslation(new XYZ(0,0,0));
				
				//Create copy Options
				CopyPasteOptions opt = new CopyPasteOptions();
				
				using (Transaction tx = new Transaction(resultDoc)) {
					tx.Start("Custom Copy");
					
					ElementTransformUtils.CopyElements(_doc,viewElementIds,resultDoc,transform, opt);

					tx.Commit();
				}
			}
		}
		
		
		public void CopyPasteFloor()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			//Current View Id
			ElementId currentViewId = _doc.ActiveView.Id;
			
			//Retrive all element in view
			ICollection<ElementId> viewElementIds = new FilteredElementCollector(_doc).OfCategory(BuiltInCategory.OST_Floors).ToElementIds();
			
			//Retrive final doc
			Document resultDoc = null;
			
			foreach (Document linkedDoc in this.Application.Documents) {
				
				if (linkedDoc.PathName.Contains("INGP_PJP_SYN_SOCLE_BAS_SOL.rvt"))
				{
					resultDoc = linkedDoc;
					break;
				}
			}
			
			
			if (resultDoc != null)
			{
				
				//Create an empty transformation
				XYZ transformVector = -GetSurveyPoint(resultDoc,false)-GetSurveyPoint(_doc,false);
				Transform transform = Transform.CreateTranslation(transformVector);
				
				//Create copy Options
				CopyPasteOptions opt = new CopyPasteOptions();
				
				using (Transaction tx = new Transaction(resultDoc)) {
					tx.Start("Custom Copy");
					
					ElementTransformUtils.CopyElements(_doc,viewElementIds,resultDoc,transform, opt);

					tx.Commit();
				}
			}
		}
		
		private XYZ GetSurveyPoint(Document doc,bool IsShared)
		{
			//find orign point
			IList<Element> basePoints = new FilteredElementCollector(doc).OfClass(typeof(BasePoint)).ToElements();
			BasePoint surveyPoint = null;
			foreach (Element element in basePoints) {
				BasePoint point = element as BasePoint;
				if (point.IsShared == IsShared)
				{
					surveyPoint = point;
					break;
				}
			}
			if (surveyPoint != null)
			{
				double x = surveyPoint.get_Parameter(BuiltInParameter.BASEPOINT_EASTWEST_PARAM).AsDouble();
				double y = surveyPoint.get_Parameter(BuiltInParameter.BASEPOINT_NORTHSOUTH_PARAM).AsDouble();
				double elevation = surveyPoint.get_Parameter  (BuiltInParameter.BASEPOINT_ELEVATION_PARAM).AsDouble();
				
				
				XYZ surveyCoordinates = new XYZ (x,y,elevation);
				
				return surveyCoordinates;
			}
			else
			{
				return null;
			}
		}
		
		public void deleteImportCategories(Document doc)
		{
			IList<ElementId> categoryIds = new List<ElementId>();

			foreach (ImportInstance ii in  new FilteredElementCollector(doc)
			         .OfClass(typeof(ImportInstance))
			         .Cast<ImportInstance>()
			         .Where(i => i.IsLinked == false))
			{
				ElementId catId = ii.Category.Id;
				if (!categoryIds.Contains(catId))
					categoryIds.Add(catId);
			}

			using (Transaction t = new Transaction(doc,"Delete Import Categories"))
			{
				t.Start();
				doc.Delete(categoryIds);
				t.Commit();
			}
		}
		
		public void SaveModels()
		{
			string dirPath = @"C:\Users\moreaus\Desktop\Courant\Revit\Dev\";

			string[] files = Directory.GetFiles(dirPath,"*.rvt",SearchOption.AllDirectories);

			foreach (string file in files)
			{
				string fileNAme = System.IO.Path.GetFileName(file);
				ModelPath revitPath = ModelPathUtils.ConvertUserVisiblePathToModelPath(file);
				
				string newDirPath = @"C:\Users\moreaus\Desktop\Courant\Revit\";
				string newPath = file.Replace(dirPath,newDirPath);
				
				OpenOptions opOptions = new OpenOptions();
				opOptions.DetachFromCentralOption = DetachFromCentralOption.DetachAndPreserveWorksets;
				
				Document document = this.Application.OpenDocumentFile(revitPath,opOptions);
				
				document.SaveAs(newPath);
				document.Close(false);
			}
		}
		
		public void ReloadLink()
		{
//			UIApplication uiapp = this.a
//			UIDocument uidoc = uiapp.ActiveUIDocument;
			//Application app = this.Application;
			//Document doc = this.ActiveUIDocument.Document;
			
			//Where to find models to link
			string sDir = @"S:\PRO";
			
			FilePath location = new FilePath( @"C:\Users\moreaus\Desktop\Courant\TGI\LinkedModels\TGI-Main.rvt" );
			
			TransmissionData transData= TransmissionData.ReadTransmissionData(location );
			
			if( null != transData )
			{
				// Collect all (immediate) external references in the model
				
				ICollection<ElementId> externalReferences= transData.GetAllExternalFileReferenceIds();
				
				// Find every reference that is a link
				
				foreach( ElementId refId in externalReferences )
				{
					ExternalFileReference extRef= transData.GetLastSavedReferenceData(refId );

					string path = ModelPathUtils.ConvertModelPathToUserVisiblePath( extRef.GetPath() ) ;
					string modelFileName = Path.GetFileName(path);
					modelFileName = modelFileName.Replace("_CENTRAL","_DETACH");
					
					//Search reccursively for the file with the same name
					string[] results = Directory.GetFiles(sDir,modelFileName,SearchOption.AllDirectories);
					
					if (results.Count() > 0)
					{
						if( extRef.ExternalFileReferenceType == ExternalFileReferenceType.RevitLink )
						{
							// Change the path of the linked file, leaving everything else unchanged:
							
							transData.SetDesiredReferenceData( refId,new FilePath( results[0] ),extRef.PathType, false );
						}
					}

				}
				
				// Make sure the IsTransmitted property is set
				
				transData.IsTransmitted = true;
				
				// Modified transmission data must be saved back to the model
				
				TransmissionData.WriteTransmissionData(location, transData );
			}
			else
			{
				TaskDialog.Show( "Unload Links","The document does not have any transmission data" );
			}
		}
		
		public void UnloadRevitLinks()
			
			/// This method will set all Revit links to be unloaded the next time the document at the given location is opened.
			/// The TransmissionData for a given document only contains top-level Revit links, not nested links.
			/// However, nested links will be unloaded if their parent links are unloaded, so this function only needs to look at the document's immediate links.
		{
			
			ModelPath location = ModelPathUtils.ConvertUserVisiblePathToModelPath(@"C:\Users\moreaus\Desktop\Courant\TGI\LinkedModels\TGI-Main.rvt");
			
			
			// access transmission data in the given Revit file
			TransmissionData transData = TransmissionData.ReadTransmissionData(location);
			if (transData != null)
			{
				// collect all (immediate) external references in the model
				ICollection<ElementId> externalReferences = transData.GetAllExternalFileReferenceIds();
				// find every reference that is a link
				foreach (ElementId refId in externalReferences)
				{
					ExternalFileReference extRef = transData.GetLastSavedReferenceData(refId);
					if (extRef.ExternalFileReferenceType == ExternalFileReferenceType.RevitLink)
					{
						// we do not want to change neither the path nor the path-type
						// we only want the links to be unloaded (shouldLoad = false)
						transData.SetDesiredReferenceData(refId, extRef.GetPath(), extRef.PathType, false);
					}
				}
				
				// make sure the IsTransmitted property is set
				transData.IsTransmitted = true;
				// modified transmission data must be saved back to the model
				TransmissionData.WriteTransmissionData(location, transData);
			}
			else
			{
				Autodesk.Revit.UI.TaskDialog.Show("Unload Links", "The document does not have any transmission data");
			}
		}
		
		public void ModelTimeStamp()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			using (Transaction tx = new Transaction(doc)) {

				tx.Start("Model TimeStamp");

				//Create a list of category
				CategorySet myCategories = CreateCategoryList(doc, this.Application);

				//Retrive all model elements
				FilteredElementCollector collector = new FilteredElementCollector(doc);
				IList<ElementFilter> categoryFilters = new List<ElementFilter>();

				foreach (Category category in myCategories)
				{
					categoryFilters.Add(new ElementCategoryFilter(category.Id));
				}

				ElementFilter filter = new LogicalOrFilter(categoryFilters);

				IList<Element> elementList = collector.WherePasses(filter).WhereElementIsNotElementType().ToElements();

				//Add the value to all element
				if (elementList.Count > 0)
				{
					foreach (Element e in elementList)
					{
						WriteOnParam("Date", e, DateTime.Now.ToShortDateString());
						WriteOnParam("Version", e, "First Release");
						WriteOnParam("FileName", e, "SubContractors Model");
						WriteOnParam("Trade", e, "HVAC");
					}
				}

				tx.Commit();
			}

		}
		
		private void WriteOnParam(string paramName, Element e, string value)
		{
			IList<Parameter> parameters = e.GetParameters(paramName);
			if (parameters.Count != 0)
			{
				Parameter p = parameters.FirstOrDefault();
				if (!p.IsReadOnly)
				{
					p.Set(value);
				}
			}
		}
		
		private CategorySet CreateCategoryList(Document doc, Autodesk.Revit.ApplicationServices.Application app)
		{
			CategorySet myCategorySet = app.Create.NewCategorySet();
			Categories categories = doc.Settings.Categories;

			foreach (Category c in categories)
			{
				if (c.AllowsBoundParameters && c.CategoryType == CategoryType.Model)
				{
					myCategorySet.Insert(c);
				}
			}

			return myCategorySet;
		}
		public void GetSharedParamFilePath()
		{
			Autodesk.Revit.ApplicationServices.Application app = this.Application;
			
			string previousSharedParam = app.SharedParametersFilename;
			//Save the previous shared param file, if an
			app.SharedParametersFilename = "";
			
			//app.SharedParametersFilename = previousSharedParam;
		}
		
		public void deleteImportCategories()
		{
			Document doc = this.ActiveUIDocument.Document;

			IList<ElementId> categoryIds = new List<ElementId>();

			foreach (ImportInstance ii in  new FilteredElementCollector(doc)
			         .OfClass(typeof(ImportInstance))
			         .Cast<ImportInstance>()
			         .Where(i => i.IsLinked == false))
			{
				ElementId catId = ii.Category.Id;
				if (!categoryIds.Contains(catId))
					categoryIds.Add(catId);
			}

			using (Transaction t = new Transaction(doc,"Delete Import Categories"))
			{
				t.Start();
				doc.Delete(categoryIds);
				t.Commit();
			}
		}
		
		public void ListLinkedFiles()
		{
			List<string> values = new List<string>();
			string path = @"C:\Affaires\01-Developpement\ModelUpgrade\ListLinks\List.csv";
			Document doc = this.ActiveUIDocument.Document;
			
			//find all link
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			ICollection<Element> rvtLinks = collector.OfCategory(BuiltInCategory.OST_RvtLinks).WhereElementIsNotElementType().ToElements();
			
			foreach (Element element in rvtLinks) {
				
				RevitLinkInstance RVTLink = element as RevitLinkInstance;
				Document linkedDoc = RVTLink.GetLinkDocument();
				string line = "";
				if (linkedDoc == null)
				{
					line = string.Format("{0};{1}",
					                     RVTLink.Name,
					                     "null");
				}
				else
				{
					line = string.Format("{0};{1}",
					                     RVTLink.Name,
					                     linkedDoc.PathName);
				}
				

				
				values.Add(line);
			}
			
			File.WriteAllLines(path,values.ToArray(),Encoding.UTF8);
		}
		public void DeleteLineStyle()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//find all link
			FilteredElementCollector CADLinkTypesCollector = new FilteredElementCollector(doc);
			List<ElementId> CADLinkTypes = CADLinkTypesCollector.OfClass(typeof(CADLinkType)).ToElementIds().ToList();
			
			//find all link
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			List<Element> linePatternElements = collector.OfClass(typeof(LinePatternElement)).ToElements().ToList();
			
			using (Transaction tx  = new Transaction(doc)) {
				tx.Start("Remove DWG");

				List<ElementId> ids = new List<ElementId>();
				ids.AddRange(CADLinkTypes);
				
				foreach (Element linePatternElement in linePatternElements) {
					if (linePatternElement.Name.Contains("IMPORT-"))
					{
						ids.Add(linePatternElement.Id);
					}
				}
				
				doc.Delete(ids);
				
				tx.Commit();
			}
		}
		
		
		public void GetBounndingBoxWithAVF()
		{
			UIDocument UIDoc = this.ActiveUIDocument;
			
			// Get the handle of current document.
			Document doc = UIDoc.Document;
			ICollection<ElementId> selectedIds = UIDoc.Selection.GetElementIds();

			if(selectedIds.Count == 0)
			{

				IList<Reference> selectedReferences = UIDoc.Selection.PickObjects(ObjectType.Element, "Pick elements to be aligned");
				selectedIds = RevitReferencesToElementIds(doc, selectedReferences);
				UIDoc.Selection.SetElementIds(selectedIds);
			}
			
			View currentView = doc.ActiveView;


			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Create Spheres");
				
				
				foreach (ElementId id in UIDoc.Selection.GetElementIds()) {

					Element element = doc.GetElement(id);

					BoundingBoxXYZ BBox = element.get_BoundingBox(currentView);
					
					Transform tr = BBox.Transform;
					
					Solid max = CreateSphereAt(BBox.Max, 0.5 );
					
					Solid min = CreateSphereAt(BBox.Min, 0.5);
					
					
					
					PaintSolid( max, currentView );
					PaintSolid( min, currentView );
				}
				
				tx.Commit();
			}
			

			
			
		}
		
		/// <summary>
		/// Create and return a solid sphere with
		/// a given radius and centre point.
		/// </summary>
		static public Solid CreateSphereAt(XYZ centre,double radius )
		{
			// Use the standard global coordinate system
			// as a frame, translated to the sphere centre.
			
			Frame frame = new Frame( centre,XYZ.BasisX, XYZ.BasisY, XYZ.BasisZ );
			
			// Create a vertical half-circle loop;
			// this must be in the frame location.
			
			Arc arc = Arc.Create( centre - radius * XYZ.BasisZ,centre + radius * XYZ.BasisZ,centre + radius * XYZ.BasisX );
			
			Line line = Line.CreateBound(arc.GetEndPoint( 1 ),arc.GetEndPoint( 0 ) );
			
			CurveLoop halfCircle = new CurveLoop(); halfCircle.Append( arc );halfCircle.Append( line );
			
			List<CurveLoop> loops = new List<CurveLoop>( 1 );
			
			loops.Add( halfCircle );
			
			return GeometryCreationUtilities.CreateRevolvedGeometry(frame, loops, 0, 2 * Math.PI );
		}
		
		/// <summary>
		/// Schema Id
		/// </summary>
		private int m_schemaId = -1;
		
		/// <summary>
		/// Paint solid by AVF
		/// </summary>
		/// <param name="solid">Solid to be painted</param>
		/// <param name="view">The view that shows solid</param>
		private void PaintSolid(Solid solid, View view)
		{
			String viewName = view.Name;
			SpatialFieldManager sfm = SpatialFieldManager.GetSpatialFieldManager(view);
			if (sfm == null) sfm = SpatialFieldManager.CreateSpatialFieldManager(view, 1);

			if (m_schemaId != -1)
			{
				IList<int> results = sfm.GetRegisteredResults();

				if (!results.Contains(m_schemaId))
				{
					m_schemaId = -1;
				}
			}

			// set up the display style
			if (m_schemaId == -1)
			{
				AnalysisResultSchema resultSchema1 = new AnalysisResultSchema(
					"PaintedSolid " + viewName  + Guid.NewGuid(), "Description");

				AnalysisDisplayStyle displayStyle = AnalysisDisplayStyle.CreateAnalysisDisplayStyle(
					this.ActiveUIDocument.Document, "Real_Color_Surface"  + viewName + Guid.NewGuid(),
					new AnalysisDisplayColoredSurfaceSettings(), new AnalysisDisplayColorSettings(), new AnalysisDisplayLegendSettings());

				resultSchema1.AnalysisDisplayStyleId = displayStyle.Id;

				m_schemaId = sfm.RegisterResult(resultSchema1);
			}

			// get points of all faces in the solid
			FaceArray faces = solid.Faces;
			Transform trf = Transform.Identity;
			foreach (Face face in faces)
			{
				int idx = sfm.AddSpatialFieldPrimitive(face, trf);
				IList<UV> uvPts = null;
				IList<ValueAtPoint> valList = null;
				ComputeValueAtPointForFace(face, out uvPts, out valList, 1);

				FieldDomainPointsByUV pnts = new FieldDomainPointsByUV(uvPts);
				FieldValues vals = new FieldValues(valList);
				sfm.UpdateSpatialFieldPrimitive(idx, pnts, vals, m_schemaId);
			}
		}
		
		/// <summary>
		/// Compute values at point for face
		/// </summary>
		/// <param name="face">Give face</param>
		/// <param name="uvPts">UV points</param>
		/// <param name="valList">Values at point</param>
		/// <param name="measurementNo"></param>
		private void ComputeValueAtPointForFace(Face face, out IList<UV> uvPts,
		                                        out IList<ValueAtPoint> valList, int measurementNo)
		{
			List<double> doubleList = new List<double>();
			uvPts = new List<UV>();
			valList = new List<ValueAtPoint>();
			BoundingBoxUV bb = face.GetBoundingBox();
			for (double u = bb.Min.U; u < bb.Max.U + 0.0000001; u = u + (bb.Max.U - bb.Min.U) / 1)
			{
				for (double v = bb.Min.V; v < bb.Max.V + 0.0000001; v = v + (bb.Max.V - bb.Min.V) / 1)
				{
					UV uvPnt = new UV(u, v);
					uvPts.Add(uvPnt);
					XYZ faceXYZ = face.Evaluate(uvPnt);
					// Specify three values for each point
					for (int ii = 1; ii <= measurementNo; ii++)
						doubleList.Add(faceXYZ.DistanceTo(XYZ.Zero) * ii);
					valList.Add(new ValueAtPoint(doubleList));
					doubleList.Clear();
				}
			}
		}
		
		
		public static ICollection<ElementId> RevitReferencesToElementIds(Document doc, IList<Reference> selectedReferences)
		{
			return selectedReferences.Select(x => doc.GetElement(x).Id).ToList();
		}
		public void GetBoundingBoxWithDirectShape()
		{
			
			UIDocument UIDoc = this.ActiveUIDocument;
			
			// Get the handle of current document.
			Document doc = UIDoc.Document;
			ICollection<ElementId> selectedIds = UIDoc.Selection.GetElementIds();

			if(selectedIds.Count == 0)
			{

				IList<Reference> selectedReferences = UIDoc.Selection.PickObjects(ObjectType.Element, "Pick elements to be aligned");
				selectedIds = RevitReferencesToElementIds(doc, selectedReferences);
				UIDoc.Selection.SetElementIds(selectedIds);
			}
			
			View currentView = doc.ActiveView;


			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Create Direct Shapes Spheres");
				
				
				foreach (ElementId id in UIDoc.Selection.GetElementIds()) {

					Element element = doc.GetElement(id);

					BoundingBoxXYZ BBox = element.get_BoundingBox(currentView);
					
					XYZ maxPoint = BBox.Max;
					XYZ minPoint = BBox.Min;
					
					Transform tr = BBox.Transform;
					
					Solid max = CreateSphereAt(maxPoint, 0.5 );
					
					Solid min = CreateSphereAt(minPoint, 0.5);
					
					CreateDirectShape(doc,max,new Color(255,0,0),element.Name + "_" +currentView.Name + "_Max");
					
					CreateDirectShape(doc,min, new Color(0,255,0),element.Name + "_" +currentView.Name + "_Min");
					
				}
				
				tx.Commit();
			}
			
		}
		
		private void CreateDirectShape(Document doc, Solid solid, Color color, string paramValue)
		{
			OverrideGraphicSettings ogs = new OverrideGraphicSettings();
			ogs.SetProjectionFillColor(color); //new Color(0,255,0)
			ogs.SetProjectionFillPatternId(new ElementId(4));
			ogs.SetProjectionFillPatternVisible(true);
			
			// create direct shape and assign the sphere shape
			DirectShape dsmax = DirectShape.CreateElement(doc, new ElementId(BuiltInCategory.OST_GenericModel));
			
			dsmax.ApplicationId = "ApplicationID";
			dsmax.ApplicationDataId = "ApplicationDataId";
			
			dsmax.SetShape(new GeometryObject[] { solid});
			doc.ActiveView.SetElementOverrides(dsmax.Id, ogs);
			
			Parameter parameter = dsmax.get_Parameter(BuiltInParameter.ALL_MODEL_INSTANCE_COMMENTS);
			parameter.Set(paramValue);
		}
		
		public void DrawInViewCoordinates()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			View view = doc.ActiveView;
			

			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Draw in view coordinates");
				
				//0 of the view
				XYZ origin = new XYZ(0,0,0);
				//In the model reference
				XYZ viewOriginInodel = view.CropBox.Transform.OfPoint(origin);
				Solid originSolid = CreateSphereAt(viewOriginInodel, 0.1);
				CreateDirectShape(doc,originSolid,new Color(0,255,0),"View Origin");
				

				origin = new XYZ(1,0,0);
				//In the model reference
				viewOriginInodel = view.CropBox.Transform.OfPoint(origin);
				originSolid = CreateSphereAt(viewOriginInodel, 0.1);
				CreateDirectShape(doc,originSolid,new Color(0,255,0),"View X");
				
				origin = new XYZ(0,1,0);
				//In the model reference
				viewOriginInodel = view.CropBox.Transform.OfPoint(origin);
				originSolid = CreateSphereAt(viewOriginInodel, 0.1);
				CreateDirectShape(doc,originSolid,new Color(0,255,0),"View Y");
				
				origin = new XYZ(0,0,1);
				//In the model reference
				viewOriginInodel = view.CropBox.Transform.OfPoint(origin);
				originSolid = CreateSphereAt(viewOriginInodel, 0.1);
				CreateDirectShape(doc,originSolid,new Color(0,255,0),"View Z");
				
				
				tx.Commit();
				
			}
		}
		
		
		
		public void DrawInView()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			View view = doc.ActiveView;
			

			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Draw in view");
				
				
				// Create a geometry plane
				
				XYZ point = new XYZ(0,0,0);
				
				XYZ origin = view.CropBox.Transform.Inverse.OfPoint(point);
				XYZ normal = view.ViewDirection;

				Plane geomPlane = Plane.CreateByNormalAndOrigin(normal, origin);

				//Create a circle
				Arc geomCircle = Arc.Create(origin, 1, 0, 2.0 * Math.PI, geomPlane.XVec, geomPlane.YVec);
				
				//Create a line
				Line geomLineX = Line.CreateBound(origin,origin + geomPlane.XVec);
				Line geomLineY = Line.CreateBound(origin,origin + geomPlane.YVec);

				// Create a sketch plane in current document
				SketchPlane sketch = SketchPlane.Create(doc,geomPlane );

				// Create a DetailLine element using the
				// newly created geometry line and sketch plane
				DetailLine line = doc.Create.NewDetailCurve(view, geomCircle) as DetailLine;
				DetailLine lineX = doc.Create.NewDetailCurve(view, geomLineX) as DetailLine;
				DetailLine lineY = doc.Create.NewDetailCurve(view, geomLineY) as DetailLine;
				
				tx.Commit();
			}

			

		}
	}
}