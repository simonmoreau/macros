﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: moreaus
 * Date: 22/11/2013
 * Heure: 10:44
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Mechanical;
using System.IO;
using Autodesk.Revit;

namespace Views
{
	/// <summary>
	/// Description of DrawingProperties.
	/// </summary>
	public class DrawingProperties
	{
		
		private Document _doc;
		
		//Document number
		private string _INGP_EMETTEUR;
		private string _INGP_SPECIALITE; //(Lot)
		private string _INGP_ZONE;
		private string _INGP_NIVEAU;
		private string _INGP_TYPE;
		private string _INGP_NUMERO;
		
		
		//Document properties
		private ElementId _revisionId;
		private string _INGP_NOM_DE_FICHIER;
		private string _INGP_FORMAT;
		private string _INGP_ECHELLE;
		public string Echelle
		{
			get { return _INGP_ECHELLE; }
			set {_INGP_ECHELLE = value;}
		}
		
		
		//Sheet properties
		private string _INGP_TITRE1;
		private string _INGP_TITRE2;
		private string _INGP_TITRE3;
		public string INGP_TITRE3
		{
			get { return INGP_TITRE3; }
			set{_INGP_TITRE3 = value;}
		}
		
		
		public DrawingProperties(Document document)
		{
			this._doc =document;
			
			//Retrive the revision
			IEnumerable<Element> revisions = from elem in new FilteredElementCollector(_doc).OfCategory(BuiltInCategory.OST_Revisions)
				let type = elem as Element
				select type;
			_revisionId = revisions.First().Id;
			
			//Document properties
			this._INGP_NOM_DE_FICHIER = Path.GetFileName(_doc.PathName);
			this._INGP_FORMAT = "A3";
			this._INGP_ECHELLE = "-";
			
			//Document number
			this._INGP_EMETTEUR = "INGP";
			this._INGP_SPECIALITE = "SYN";
			this._INGP_ZONE = "SO";
			this._INGP_NIVEAU = "00";
			this._INGP_TYPE = "FSL";
			this._INGP_NUMERO = ProcessDrawingNumber();
			
			//Document tittle
			this._INGP_TITRE1 = "SYNTHESE ARCHITECTURALE";
			this._INGP_TITRE2 = "FICHE DE SYNTHESE PAR LOCAL";
			this._INGP_TITRE3 = "RoomName";
		}
		
		
		private string ProcessDrawingNumber()
		{

			string levelDigit;
			
			switch (_INGP_NIVEAU) {
				case "S2":
					levelDigit = "02";
					break;
				case "00":
					levelDigit = "04";
					break;
				default:
					levelDigit = "00";
					break;
			}
			
			string zoneDigit;
			
			switch (_INGP_ZONE) {
				case "BA":
					zoneDigit = "1";
					break;
				case "SO":
					zoneDigit = "2";
					break;
				default:
					zoneDigit = "0";
					break;
			}
			
			string roomNumb = "XXX";
			
			return zoneDigit + levelDigit + roomNumb;
		}
		
		public void CompleteSharedParameter(ViewSheet currentSheet,int PageNumber,int Scale)
		{
			Parameter currentParam;
			
			string scaleString;
			
			if (Scale == 0)
			{
				scaleString = "-";
			}
			else
			{
				scaleString = "1:" + Scale.ToString();
			}
			
			//Setup the revision
			IList<ElementId> revisionIds = new List<ElementId>();
			revisionIds.Add(_revisionId);
			
			currentSheet.SetAdditionalRevisionIds(revisionIds);
			
			//Fill Document properties
			currentParam = currentSheet.GetParameters("INGP_NOM DE FICHIER").FirstOrDefault();
			currentParam.Set(_INGP_NOM_DE_FICHIER);
			currentParam = currentSheet.GetParameters("INGP_FORMAT").FirstOrDefault();
			currentParam.Set(_INGP_FORMAT);
			currentParam = currentSheet.GetParameters("INGP_ECHELLE").FirstOrDefault();
			currentParam.Set(scaleString);
			
			//Fill Document number
			currentParam = currentSheet.GetParameters("INGP_EMETTEUR").FirstOrDefault();
			currentParam.Set(_INGP_EMETTEUR);
			currentParam = currentSheet.GetParameters("INGP_SPECIALITE").FirstOrDefault();
			currentParam.Set(_INGP_SPECIALITE);
			currentParam = currentSheet.GetParameters("INGP_ZONE").FirstOrDefault();
			currentParam.Set(_INGP_ZONE);
			currentParam = currentSheet.GetParameters("INGP_IDENTIFIANT/ NIVEAU").FirstOrDefault();
			currentParam.Set(_INGP_NIVEAU);
			currentParam = currentSheet.GetParameters("INGP_TYPE").FirstOrDefault();
			currentParam.Set(_INGP_TYPE);
			currentParam = currentSheet.GetParameters("INGP_NUMERO").FirstOrDefault();
			currentParam.Set(_INGP_NUMERO);
			
			//Fill document tittle
			currentParam = currentSheet.GetParameters("INGP_TITRE1").FirstOrDefault();
			currentParam.Set(_INGP_TITRE1);
			currentParam = currentSheet.GetParameters("INGP_TITRE2").FirstOrDefault();
			currentParam.Set(_INGP_TITRE2);
			currentParam = currentSheet.GetParameters("INGP_TITRE3").FirstOrDefault();
			currentParam.Set(_INGP_TITRE3);
			
			//Fill page number
			currentParam = currentSheet.GetParameters("INGP_FOLIO").FirstOrDefault();
			currentParam.Set(PageNumber.ToString());
			
			
		}
	}
}
