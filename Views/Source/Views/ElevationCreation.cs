﻿/*
 * Revit Macro created by SharpDevelop
 * Utilisateur: moreaus
 * Date: 29/10/2013
 * Heure: 10:25
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Mechanical;
using System.IO;
using Autodesk.Revit;

namespace Views
{
	/// <summary>
	/// Description of ElevationCreation.
	/// </summary>
	public class ElevationCreation
	{
		private Document _doc;
		private ElementId _titleBlockId;
		private ElementId _mainTitleBlockId;
		private ElementId _sectionViewTypeId;
		private ElementId _planViewTypeId;
		private ElementId _ceilingPlanViewTypeId;
		private SpatialElementBoundaryOptions _spacialBoundaryOptions;
		private ViewPlan _currentPlanView;
		private ElementId _currentLevelId;
		private View _planViewTemplate;
		private View _ceillingPlanViewTemplate;
		private View _sectionViewTemplate;
		private DrawingProperties _drawingProp;
		private int _pageNumber;
		
		//Constructor
		//Retrive all element
		public ElevationCreation(Document document)
		{
			//Retrive current document
			_doc = document;
			
			//Create a room properties summary
			_drawingProp = new DrawingProperties(document);
			
			//Retrive current TitleBlock Id
			// Retrieve the family if it is already present:
			IEnumerable<Family> titleBlocFamilies = from elem in new FilteredElementCollector(_doc).OfClass(typeof(Family))
				let type = elem as Family
				where type.Name == "ING_PJP_FOLIO_A3"
				select type;
			Family titleBlockFamily = titleBlocFamilies.First();
			_titleBlockId = titleBlockFamily.GetFamilySymbolIds().FirstOrDefault();
			
			//Retrive the front page tittleBlock
			IEnumerable<Family> mainTitleBlocFamilies = from elem in new FilteredElementCollector(_doc).OfClass(typeof(Family))
				let type = elem as Family
				where type.Name == "INGP_PJP_Cartouche_A3"
				select type;
			Family mainTitleBlockFamily = mainTitleBlocFamilies.First();
			_mainTitleBlockId = mainTitleBlockFamily.GetFamilySymbolIds().FirstOrDefault();
			
			
			// Find a section view type
			IEnumerable<ViewFamilyType> viewFamilyTypes = from elem in new FilteredElementCollector(_doc).OfClass(typeof(ViewFamilyType))
				let type = elem as ViewFamilyType
				where type.ViewFamily == ViewFamily.Section
				select type;
			_sectionViewTypeId = viewFamilyTypes.First().Id;
			

			// Find a plan view type
			viewFamilyTypes = from elem in new FilteredElementCollector(_doc).OfClass(typeof(ViewFamilyType))
				let type = elem as ViewFamilyType
				where type.ViewFamily == ViewFamily.FloorPlan
				select type;
			_planViewTypeId = viewFamilyTypes.First().Id;
			
			// Find a ceiling plan view type
			viewFamilyTypes = from elem in new FilteredElementCollector(_doc).OfClass(typeof(ViewFamilyType))
				let type = elem as ViewFamilyType
				where type.ViewFamily == ViewFamily.CeilingPlan
				select type;
			_ceilingPlanViewTypeId = viewFamilyTypes.First().Id;
			
			//Find a plan view template
			// Filter views that match the required criteria
			FilteredElementCollector collector= new FilteredElementCollector(_doc).OfClass(typeof(View));
			
			// Get the specific view(s)
			IEnumerable<View> views= from Autodesk.Revit.DB.View f in collector
				where (f.ViewType == ViewType.FloorPlan && f.IsTemplate && f.ViewName == "PlanViewTemplate")
				select f;
			_planViewTemplate = views.First();
			
			// Get the specific view(s)
			views= from Autodesk.Revit.DB.View f in collector
				where (f.ViewType == ViewType.CeilingPlan && f.IsTemplate && f.ViewName == "CeilingPlanViewTemplate")
				select f;
			_ceillingPlanViewTemplate = views.First();
			
			// Get the specific view(s)
			views= from Autodesk.Revit.DB.View f in collector
				where (f.ViewType == ViewType.Section && f.IsTemplate && f.ViewName == "SectionViewTemplate")
				select f;
			_sectionViewTemplate = views.First();

			//Define room boundary option
			_spacialBoundaryOptions = new SpatialElementBoundaryOptions();
			_spacialBoundaryOptions.SpatialElementBoundaryLocation = SpatialElementBoundaryLocation.Finish;
			
			//Get current view
			_currentPlanView = _doc.ActiveView as ViewPlan;
			
			//Get current level
			Level _currentLevel = _currentPlanView.GenLevel;
			
			if (_currentLevel != null) {
				_currentLevelId = _currentLevel.Id;
			}
		}
		
		//Main
		public void CreatingFolioFromRoom()
		{

			//Get a list of room to process
			List<RoomViews> RoomsViews = new List<RoomViews>();
			//List<Room> rooms = TempRooms();
			List<Room> rooms = RoomsInModels();
			
			using (Transaction tx = new Transaction(_doc))
			{
				tx.Start("Create Folio");

				//Loop on all selected rooms
				foreach (Room _room in rooms) {
					
					
					RoomViews currentRoomElevation = new RoomViews(_room);
					
					//Create elevation on each room
					CreateSectionsFromRoom(_room, ref currentRoomElevation);
					
					//Creation plan view on each room
					CreatePlanFromRoom(_room, ref currentRoomElevation);
					
					//Apply a filter on these view
					CreateAndApplyViewFilter(currentRoomElevation);
					
					RoomsViews.Add(currentRoomElevation);
				}
				
				tx.Commit();
			}
			
			using (Transaction tx = new Transaction(_doc))
			{
				tx.Start("Create Sheet");
				
				foreach (RoomViews rmv in RoomsViews) {
					//Creating sheets
					if (rmv.SectionList.Count != 0)
					{
						_drawingProp.INGP_TITRE3 = rmv.Room.Number;
						_pageNumber = 1;
						
						CreateFrontPage(rmv);
						CreatePlanSheet(rmv);
						CreateSectionSheet(rmv);
					}
					
				}

				
				tx.Commit();
			}
			

		}
		
		//get room temp
		public List<Room> TempRooms()
		{
			IEnumerable<Room> tempsrooms = from elem in new FilteredElementCollector(_doc).OfClass(typeof(SpatialElement))
				let room = elem as Room
				select room;
			List<Room> tempRooms = new List<Room>();
			
			foreach (Room tempRoom in tempsrooms) {
				tempRooms.Add(tempRoom);
			}
			
			return tempRooms;
		}
		
		//get rooms
		public List<Room> RoomsInModels()
		{
			//Find all rooms in liked files
			List<Room> selectedRooms = new List<Room>();

			foreach (Document d in _doc.Application.Documents) {
				if (d.PathName.Contains("RPBW"))
				{
					IEnumerable<Room> tempsrooms = from elem in new FilteredElementCollector(d).OfClass(typeof(SpatialElement))
						let room = elem as Room
						select room;
					
					foreach (Room tempRoom in tempsrooms) {
						if (tempRoom != null)
						{
							if (tempRoom.Number != null)
							{
								//if (tempRoom.Number == "B.S2.K.001" | tempRoom.Number == "B.S2.Q.018" | tempRoom.Number == "B.S2.D.004" )
								
								if ( tempRoom.Number == "S.00.C.006")
								{
									selectedRooms.Add(tempRoom);
								}
							}
						}
						
					}
				}
			}
			
			return selectedRooms;
		}
		
		private int AdaptedScale(BoundingBoxXYZ bBox)
		{
			int scale;
			
			double currentUWidth = (bBox.Max.X - bBox.Min.X) /20;
			double currentVWidth = (bBox.Max.Y - bBox.Min.Y) /20;
			
			if (currentUWidth  > 0.38*3.28084 | currentVWidth > 0.18*3.28084 )
			{
				scale = 50;
			}
			else
			{
				scale = 20;
			}
			
			return scale;
		}
		
		private void CreatePlanFromRoom(Room currentRoom, ref RoomViews currentRoomViews)
		{
			//Get boundary Box of the room
			BoundingBoxXYZ bBox = currentRoom.get_BoundingBox(_currentPlanView);
			
			//Extend it
			double extensionFactor = 0.6 * 3.28084;
			XYZ extension = new XYZ(extensionFactor,extensionFactor,extensionFactor);
			bBox.Max = bBox.Max + extension;
			bBox.Min = bBox.Min -extension;
			
			int scale = AdaptedScale(bBox);
			
			//Create a floor plan
			ViewPlan floorPlan = ViewPlan.Create(_doc,_planViewTypeId,_currentLevelId);
			floorPlan.ApplyViewTemplateParameters(_planViewTemplate);
			floorPlan.CropBox = bBox;
			floorPlan.Scale = scale;
			floorPlan.Name = "Local "+ currentRoom.Number + " - Plan";
			
			Parameter cropView = floorPlan.get_Parameter(BuiltInParameter.VIEWER_CROP_REGION);
			cropView.Set(1);
			Parameter annotationCrop = floorPlan.get_Parameter(BuiltInParameter.VIEWER_ANNOTATION_CROP_ACTIVE);
			annotationCrop.Set(1);
			Parameter cropRegionVisible = floorPlan.get_Parameter(BuiltInParameter.VIEWER_CROP_REGION_VISIBLE);
			cropRegionVisible.Set(1);
			
			currentRoomViews.ViewPlan = floorPlan;

			//Create a ceilling plan
			ViewPlan ceilingPlan = ViewPlan.Create(_doc,_ceilingPlanViewTypeId,_currentLevelId);
			ceilingPlan.ApplyViewTemplateParameters(_ceillingPlanViewTemplate);
			ceilingPlan.CropBox = bBox;
			ceilingPlan.Scale =scale;
			ceilingPlan.Name = "Local "+ currentRoom.Number + " - Faux-Plafond";

			cropView = ceilingPlan.get_Parameter(BuiltInParameter.VIEWER_CROP_REGION);
			cropView.Set(1);
			annotationCrop = ceilingPlan.get_Parameter(BuiltInParameter.VIEWER_ANNOTATION_CROP_ACTIVE);
			annotationCrop.Set(1);
			cropRegionVisible = ceilingPlan.get_Parameter(BuiltInParameter.VIEWER_CROP_REGION_VISIBLE);
			cropRegionVisible.Set(1);
			
			currentRoomViews.ViewCeiling = ceilingPlan;
		}
		
		private void CreateSectionsFromRoom(Room currentRoom, ref RoomViews currentRoomElevation)
		{
			//define an extension value (m)
			double extention = 0.2 * 3.28084;
			
			//Get contour polyline
			IList<Autodesk.Revit.DB.BoundarySegment> boundSegList = currentRoom.GetBoundarySegments(_spacialBoundaryOptions).First();
			
			if (boundSegList.Count != 0)
			{
				int i = 1;
				Parameter heightParam = currentRoom.get_Parameter(BuiltInParameter.ROOM_HEIGHT);
				double height = heightParam.AsDouble() + extention*3;
				
				currentRoomElevation.CurveList = CleanBoundary(boundSegList.ToList(),ref currentRoomElevation);
				
				foreach (Autodesk.Revit.DB.Curve curve in currentRoomElevation.CurveList)
				{
					double extend = curve.Length/2 + extention*2;
					//Create the bounding box
					//The transformation orient the bounding box
					Transform transform = Transform.Identity;
					
					Transform curveTransform = curve.ComputeDerivatives(0.5, true);
					XYZ origin = curveTransform.Origin;
					XYZ viewDirection = curveTransform.BasisX.Normalize(); // tangent vector along the location curve
					XYZ normal = viewDirection.CrossProduct(XYZ.BasisZ).Normalize(); // location curve normal @ mid-point

					transform.Origin = origin;
					transform.BasisX = XYZ.BasisZ.CrossProduct(normal);
					transform.BasisY = XYZ.BasisZ;
					transform.BasisZ = normal;
					
					BoundingBoxXYZ sectionBox = new BoundingBoxXYZ();
					
					sectionBox.Transform = transform;
					sectionBox.Min = new XYZ(-extend,-extention*3,-extention);
					sectionBox.Max = new XYZ(extend,height,extention);
					
					ViewSection wallElevation = ViewSection.CreateSection(_doc,_sectionViewTypeId,sectionBox);
					wallElevation.ApplyViewTemplateParameters(_sectionViewTemplate);
					
					Parameter annotationCrop = wallElevation.get_Parameter(BuiltInParameter.VIEWER_ANNOTATION_CROP_ACTIVE);
					annotationCrop.Set(1);
					Parameter cropRegionVisible = wallElevation.get_Parameter(BuiltInParameter.VIEWER_CROP_REGION_VISIBLE);
					cropRegionVisible.Set(1);
					
					
					wallElevation.Name = "Local "+ currentRoom.Number + " - Section " + i;
					wallElevation.Scale = 50;
					
					currentRoomElevation.SectionList.Add(new Section(wallElevation,i));
					
					i++;
				}
			}
			
		}
		
		private void CreateSectionSheet(RoomViews currentElevations)
		{
			double limitLenght = 60*3.28084*0.001;
			List<Section> viewsToInsert = new List<Section>();
			
			foreach (Section section in currentElevations.SectionList) {
				
				section.GetViewWitdh();
				double viewWidth = section.ViewWidth;
				double margin = 40*3.28084*0.001;
				double A3Width = 420*3.28084*0.001;
				
				int index = currentElevations.SectionList.IndexOf(section);
				
				if (limitLenght + viewWidth + margin > A3Width | index == currentElevations.SectionList.Count -1) {
					
					if (index == currentElevations.SectionList.Count -1)
					{
						viewsToInsert.Add(section);
					}
					
					AddViewsToSheet(currentElevations.Room.Number,viewsToInsert);
					
					//Reset values
					limitLenght = margin;
					viewsToInsert.Clear();
					
					viewsToInsert.Add(section);
					limitLenght = limitLenght + viewWidth + margin;
				}
				else
				{
					viewsToInsert.Add(section);
					limitLenght = limitLenght + viewWidth + margin;
				}
				
			}

		}
		
		private void AddViewsToSheet(string name,List<Section> SectionsToInsert)
		{
			//Define the spacing
			double margin = 0;
			double lenght = 0;
			
			//Create the sheet name
			string sheetName;
			if (SectionsToInsert.Count == 1)
			{
				sheetName = "Section " + SectionsToInsert[0].ViewIndex;
			}
			else
			{
				sheetName = "Section " + SectionsToInsert[0].ViewIndex;
				for (int i = 2; i < SectionsToInsert.Count; i++) {
					sheetName = sheetName +"," + SectionsToInsert[i-1].ViewIndex;
				}
				sheetName = sheetName + " et " + SectionsToInsert[SectionsToInsert.Count-1].ViewIndex;
			}
			
			foreach (Section insertedSection in SectionsToInsert) {
				lenght = lenght + insertedSection.ViewWidth;
			}
			
			margin = (420*3.28084*0.001 - lenght) / (SectionsToInsert.Count + 1);
			
			//Define the stating position
			double xLocation = margin;
			double yLocation = (((0.297-0.021)/2)+0.021)*3.28084;
			
			
			//Create the reciving sheet
			ViewSheet sheet = ViewSheet.Create(_doc, _titleBlockId);
			sheet.Name = name+ " - " + sheetName;
			
			//Add the sheet param
			_drawingProp.CompleteSharedParameter(sheet,_pageNumber,50);
			_pageNumber = _pageNumber +1;
			
			foreach (Section insertedSection in SectionsToInsert) {
				
				XYZ location = new XYZ(xLocation + insertedSection.ViewWidth/2 ,yLocation,0);
				
				//Add the section view
				Viewport.Create(_doc, sheet.Id,insertedSection.ViewSection.Id , location);
				
				//Change the detail number
				Parameter detailNum = insertedSection.ViewSection.get_Parameter(BuiltInParameter.VIEWPORT_DETAIL_NUMBER);
				string elevationNumString = insertedSection.ViewIndex.ToString();;
				detailNum.Set(elevationNumString);
				
				xLocation = xLocation + insertedSection.ViewWidth + margin;
			}
		}
		
		private void CreatePlanSheet(RoomViews currentElevations)
		{
			//Create reciving sheets
			ViewSheet planSheet = ViewSheet.Create(_doc, _titleBlockId);
			planSheet.Name = currentElevations.Room.Name + " - Plan";
			
			//Add the sheet param
			_drawingProp.CompleteSharedParameter(planSheet,_pageNumber,currentElevations.ViewPlan.Scale);
			_pageNumber = _pageNumber +1;
			
			ViewSheet ceilingSheet = ViewSheet.Create(_doc, _titleBlockId);
			ceilingSheet.Name = currentElevations.Room.Name + " - Faux-Plafond";
			
			//Add the sheet param
			_drawingProp.CompleteSharedParameter(ceilingSheet,_pageNumber,currentElevations.ViewCeiling.Scale);
			_pageNumber = _pageNumber +1;
			
			Location loc = currentElevations.Room.Location;
			LocationPoint lp = loc as LocationPoint;
			XYZ planLocation = ( null == lp ) ? XYZ.Zero : lp.Point;
			
			XYZ sheetCenter = new XYZ(0.42*3.28084/2,(((0.297-0.021)/2)+0.021)*3.28084 ,0);
			
			planLocation = sheetCenter;// - planLocation/20;//new XYZ(0.1*3.28084,0.1*3.28084,0); //
			
			//Add the plan view
			Viewport.Create(_doc, planSheet.Id, currentElevations.ViewPlan.Id, planLocation);
			
			//Add the ceiling view
			Viewport.Create(_doc, ceilingSheet.Id, currentElevations.ViewCeiling.Id, planLocation);
			
		}
		
		private void CreateFrontPage(RoomViews currentElevations)
		{
			//Create front page sheet
			ViewSheet frontSheet = ViewSheet.Create(_doc, _mainTitleBlockId);
			frontSheet.Name = currentElevations.Room.Name + " - Présentation";
			
			//Add the sheet param
			_drawingProp.CompleteSharedParameter(frontSheet,_pageNumber,0);
			_pageNumber = _pageNumber +1;
		}
		
		private List<Autodesk.Revit.DB.Curve> CleanBoundary(List<Autodesk.Revit.DB.BoundarySegment> boundSegList,ref RoomViews currentRoomElevation)
		{
			List<Autodesk.Revit.DB.Curve> sortedCurves = new List<Autodesk.Revit.DB.Curve>();
			List<Autodesk.Revit.DB.XYZ> pointList = new List<XYZ>();
			List<Autodesk.Revit.DB.XYZ> sortedPointList = new List<XYZ>();
			
			
			foreach (Autodesk.Revit.DB.BoundarySegment boundSeg in boundSegList)
			{
				Curve c = boundSeg.GetCurve();
				pointList.Add(c.GetEndPoint(0));
				//Element e = _doc.GetElement();
				currentRoomElevation.BoundaryElementsIds.Add(boundSeg.ElementId);
			}
			
			foreach (XYZ currentPoint in pointList) {
				int currentIndex = pointList.IndexOf(currentPoint);
				XYZ nextPoint = pointList[WrapList(pointList.Count,currentIndex + 1)];
				XYZ previousPoint = pointList[WrapList(pointList.Count,currentIndex - 1)];
				
				//Check collinearity between vectors
				XYZ previousVector = previousPoint - currentPoint;
				XYZ nextVector = currentPoint - nextPoint;
				
				double isParallel = previousVector.CrossProduct(nextVector).GetLength();
				
				if (isParallel > 0.0001)
				{
					sortedPointList.Add(currentPoint);
				}
			}
			
			foreach (XYZ point in sortedPointList) {
				int currentIndex = sortedPointList.IndexOf(point);
				XYZ nextPoint = sortedPointList[WrapList(sortedPointList.Count,currentIndex + 1)];
				Curve currentCurve = Line.CreateBound(point,nextPoint) as Curve;
				if (currentCurve.Length > 0.1 *3.28084)
				{
					sortedCurves.Add(currentCurve);
				}
			}
			
			return sortedCurves;
		}
		
		private int WrapList(int count,int index)
		{
			if (index >= count) {
				index = index - count;
			}
			else if (index < 0) {
				index = count + index;
			}
			else
			{
			}
			if (index >=  0 && index < count) {
				return index;
			}
			else
			{
				return WrapList(count,index);
			}
		}
		
		private void CreateAndApplyViewFilter(RoomViews currentRoomElevation)
		{
			//Create a sections lines filter
			List<ElementId> categories = new List<ElementId>();
			categories.Add(new ElementId(BuiltInCategory.OST_Sections));
			ParameterFilterElement parameterFilterElement = ParameterFilterElement.Create(_doc,currentRoomElevation.Room.Number, categories);

			FilteredElementCollector parameterCollector = new FilteredElementCollector(_doc);
			Parameter parameter = parameterCollector.OfClass(typeof(ViewSection)).FirstElement().get_Parameter(BuiltInParameter.VIEW_NAME);

			List<FilterRule> filterRules = new List<FilterRule>();
			filterRules.Add(ParameterFilterRuleFactory.CreateNotContainsRule(parameter.Id,currentRoomElevation.Room.Number,true));
			parameterFilterElement.SetRules(filterRules);
			
			
			//Apply to the plan and ceilling view
			currentRoomElevation.ViewPlan.SetFilterVisibility(parameterFilterElement.Id,false);
			currentRoomElevation.ViewCeiling.SetFilterVisibility(parameterFilterElement.Id,false);
		}

		
		
	}
}
