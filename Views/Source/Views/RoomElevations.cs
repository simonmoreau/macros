﻿/*
 * Revit Macro created by SharpDevelop
 * Utilisateur: moreaus
 * Date: 28/10/2013
 * Heure: 17:47
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Mechanical;
using System.IO;
using Autodesk.Revit;

namespace Views
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class RoomViews
	{
		private Room _room;
		public Room Room
		{
			get { return _room; }
			set { _room = value; }
		}
		
		private ViewPlan _viewPlan;
		public ViewPlan ViewPlan
		{
			get { return _viewPlan; }
			set { _viewPlan = value; }
		}
		
		private ViewPlan _viewCeiling;
		public ViewPlan ViewCeiling
		{
			get { return _viewCeiling; }
			set { _viewCeiling = value; }
		}
		
		private List<Curve> _curveList;
		public List<Curve> CurveList
		{
			get { return _curveList; }
			set { _curveList = value; }
		}
		
		
		private List<Section> _sectionList;
		public List<Section> SectionList
		{
			get { return _sectionList; }
			set { _sectionList = value; }
		}
		
		private List<ElementId> _boundaryElementsIds;
		public List<ElementId> BoundaryElementsIds
		{
			get { return _boundaryElementsIds; }
			set { _boundaryElementsIds = value; }
		}
		
		public RoomViews(Room Room)
		{
			this._room = Room;
			this._sectionList = new List<Section>();
			this._boundaryElementsIds = new List<ElementId>();
		}
		
		public void TagWalls(Document _doc)
		{
			if (_boundaryElementsIds.Count != 0){
				
				// define tag mode and tag orientation for new tag
				TagMode tagMode = TagMode.TM_ADDBY_CATEGORY;
				TagOrientation tagorn = TagOrientation.Horizontal;

				foreach (ElementId id in _boundaryElementsIds) {
					
					Element e = _doc.GetElement(id);
					Wall bw = e as Wall;
					if (bw != null)
					{
						// Add the tag to the middle of the wall
						LocationCurve wallLoc = bw.Location as LocationCurve;
						XYZ wallStart = wallLoc.Curve.GetEndPoint(0);
						XYZ wallEnd = wallLoc.Curve.GetEndPoint(1);
						XYZ wallMid = wallLoc.Curve.Evaluate(0.5, true);
						
						IndependentTag planTag = _doc.Create.NewTag(_viewPlan, bw, true, tagMode, tagorn, wallMid);
						if (null == planTag)
						{
							throw new Exception("Create IndependentTag Failed.");
						}
						
						IndependentTag ceillingTag = _doc.Create.NewTag(_viewCeiling, bw, true, tagMode, tagorn, wallMid);
						if (null == ceillingTag)
						{
							throw new Exception("Create IndependentTag Failed.");
						}
					}
				}
			}
		}
		
		~RoomViews()
		{
			
		}
	}
	
	public class Section
	{
		private ViewSection _viewSection;
		public ViewSection ViewSection
		{
			get { return _viewSection; }
		}
		
		private int _viewIndex;
		public int ViewIndex
		{
			get { return _viewIndex; }
		}
		
		private double _viewWidth;
		public double ViewWidth
		{
			get { return _viewWidth; }
		}
		
		public Section(ViewSection CurrentView, int Index)
		{
			this._viewSection = CurrentView;
			this._viewIndex = Index;
			//this._viewWidth = GetViewWitdh(_viewSection);
		}
		
		public void GetViewWitdh()
		{
			BoundingBoxUV outline = _viewSection.Outline;
			_viewWidth = (outline.Max.U - outline.Min.U);
		}
		
	}
}
