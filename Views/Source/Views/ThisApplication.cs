/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 24/06/2015
 * Time: 18:53
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace Views
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("94D7D51D-E5AD-4B93-8DCC-A2927C660060")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		
		public void UnHideElement()
		{
			Document document = this.ActiveUIDocument.Document;
			FilteredElementCollector collector = new FilteredElementCollector(document);
			
			List<ElementId> elemSet = new List<ElementId>();
			

			
			List<BuiltInCategory> cats = new List<BuiltInCategory>();
			cats.Add(BuiltInCategory.OST_DuctFitting);
			cats.Add(BuiltInCategory.OST_DuctCurves);
			cats.Add(BuiltInCategory.OST_PipeFitting);
			cats.Add(BuiltInCategory.OST_PipeCurves);

			foreach (BuiltInCategory cat in cats) {
				IList<Element> elementList = collector.OfCategory(cat).OfClass(typeof(FamilyInstance)).ToList();
				if (elementList.Count > 0)
				{
					foreach (Element e in elementList) {
						elemSet.Add(e.Id);
					}
				}
			}

			
			using (Transaction tx = new Transaction(document)) {
				tx.Start("Unhide");
				
				document.ActiveView.UnhideElements(elemSet);
				
				tx.Commit();
			}
			
			
		}
		
		public void RemoveViewTemplate()
		{
			Document doc = this.ActiveUIDocument.Document;
			//Find a plan view template
			// Filter views that match the required criteria
			FilteredElementCollector collector = new FilteredElementCollector(doc).OfClass(typeof(Autodesk.Revit.DB.View));

			// Get view templates
			IEnumerable<Autodesk.Revit.DB.View> viewsTemplates = from Autodesk.Revit.DB.View f in collector
				where (f.IsTemplate)
				select f;
			
			// Get views
			IEnumerable<Autodesk.Revit.DB.View> views = from Autodesk.Revit.DB.View f in collector
				where (!f.IsTemplate)
				select f;
			
			List<ElementId> usedTemplate = new List<ElementId>();
			
			foreach (View view in views) {
				if (view.ViewTemplateId != null)
				{
					usedTemplate.Add(view.ViewTemplateId);
				}
				
			}
			
			ICollection<ElementId> ids = new List<ElementId>();
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Delete Template");
				
				foreach (View viewTemplate in viewsTemplates) {
					
					if (!usedTemplate.Contains(viewTemplate.Id))
					{
						ids.Add(viewTemplate.Id);
						
					}
					
				}
				
				doc.Delete(ids);
				
				tx.Commit();
			}
		}
		
		public void RemoveFilters()
		{
			Document doc = this.ActiveUIDocument.Document;
			//Find a plan view template
			// Filter views that match the required criteria
			FilteredElementCollector collector = new FilteredElementCollector(doc);

			//Retrive all existing filter.
			List<ElementId> filtersIds = collector.OfClass(typeof(Autodesk.Revit.DB.FilterElement)).ToElementIds().ToList();

			
			// Get views
			//List<View> views = collector.OfClass(typeof(View)).ToElements().Cast<View>().ToList();
			
			// Find all 3D view
			IEnumerable<View> views = from elem in new FilteredElementCollector(doc).OfClass(typeof(View))
				let view = elem as View
				where view.ViewType != ViewType.ProjectBrowser
				select view;
			
			List<ElementId> usedfiltersIds = new List<ElementId>();
			
			foreach (View view in views) {
				try {
					List<ElementId> inViewFiltersIds = view.GetFilters().ToList();
					foreach (ElementId id in inViewFiltersIds) {
						usedfiltersIds.Add(id);
					}
				} catch (Exception) {
					
				}

			}
			
			ICollection<ElementId> ids = new List<ElementId>();
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Delete filters");
				
				foreach (ElementId filterId in filtersIds) {
					
					if (!usedfiltersIds.Contains(filterId))
					{
						ids.Add(filterId);
						
					}
					
				}
				
				doc.Delete(ids);
				
				tx.Commit();
			}
		}
		
		public void TemplateList()
		{
			Document doc = this.ActiveUIDocument.Document;

			// Get view templates
			FilteredElementCollector collector = new FilteredElementCollector(doc).OfClass(typeof(Autodesk.Revit.DB.View));
			IEnumerable<Autodesk.Revit.DB.View> viewsTemplates = from Autodesk.Revit.DB.View f in collector
				where (f.IsTemplate)
				select f;
			
			//Create a list of string to be exported in csv
			List<string> templates = new List<string>();
			templates.Add("Template Name;Template Type;Filer Name");
			
			foreach (View viewTemplate in viewsTemplates) {
				
				string templateProperties = viewTemplate.Name + ";" + viewTemplate.ViewType + ";";
				
				List<ElementId> inTemplateFiltersIds = viewTemplate.GetFilters().ToList();
				
				foreach (ElementId filterId in inTemplateFiltersIds) {
					FilterElement filter = doc.GetElement(filterId) as FilterElement;
					
					templates.Add(templateProperties + filter.Name);
				}
			}
			
			string outpoutPath = @"C:\Affaires\00-Revit\ViewManagers\templates.csv";
			File.WriteAllLines(outpoutPath,templates.ToArray(),Encoding.GetEncoding("iso-8859-1"));
		}
		
		public void ApplyScopeBox()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			List<Element> scopeBoxes
				= new FilteredElementCollector(doc)
				.OfCategory( BuiltInCategory.OST_VolumeOfInterest )
				.WhereElementIsNotElementType().ToList();
			
			Dictionary<string,ElementId> scopeBoxesIds = new Dictionary<string, ElementId>();
			
			foreach (Element element in scopeBoxes) {

				string name  = element.Name.Replace(" ","").Split('-')[1];
				
				scopeBoxesIds.Add(name,element.Id);
			}
			
			//Find the loadingView;
			IEnumerable<ViewPlan> ModelViews = from elem in new FilteredElementCollector(doc).OfClass(typeof(ViewPlan))
				let view = elem as ViewPlan
				select view;
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("ApplyScopeBox");
				
				foreach (ViewPlan line in ModelViews) {
					
					Parameter scopeBaxParam = line.GetParameters("Zone de définition").FirstOrDefault();
					
					string scopeBoxName = line.Name.Split('-')[4];
					
					if (scopeBoxesIds.ContainsKey(scopeBoxName))
					{
						scopeBaxParam.Set(scopeBoxesIds[scopeBoxName]);
					}
					
				}
				
				tx.Commit();
			}
		}
		
		public void CreateOutView()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			//Find all Level
			List<ElementId> levelIds = new FilteredElementCollector(_doc).OfClass(typeof(Level)).ToElementIds().ToList();
			
			// Find a plan view type
			ElementId _planViewTypeId = ViewFamilyTypeId(ViewFamily.FloorPlan, _doc);
			
			//Find a view template
			IEnumerable<View> viewTemplates = from elem in new FilteredElementCollector(_doc).OfClass(typeof(View))
				let view = elem as View
				where view.Name == "R-250-Vue en plan"
				select view;
			
			if (viewTemplates.Count() == 0)
			{
				throw new Exception("No View Template");
			}
			
			View viewTemplate = viewTemplates.First();
			
			
			
			//Create a list of existing titleblock famillies
			IEnumerable<Family> loadedFamilies = from elem in new FilteredElementCollector(_doc).OfClass(typeof(Family))
				let family = elem as Family
				where family.Name == "INGP_PJP_Cartouche_A0"
				select family;
			
			ElementId _curentTittleBlockId = null;
			_curentTittleBlockId = loadedFamilies.First().GetFamilySymbolIds().FirstOrDefault();
			
			if (_curentTittleBlockId == null)
			{
				throw new Exception("No Tittleblock");
			}
			
			using (Transaction tx = new Transaction(_doc)) {
				
				tx.Start("Add View");

				foreach (ElementId levelId in levelIds) {

					//Retrive the level
					Level level = _doc.GetElement(levelId) as Level;
					
					//Create the view
					ViewPlan currentView = ViewPlan.Create(_doc,_planViewTypeId,levelId);
					currentView.Name = "R_" + level.Name;
					
					//Apply the view template
					currentView.ApplyViewTemplateParameters(viewTemplate);
					
					//create the sheet
					ViewSheet planSheet = ViewSheet.Create(_doc, _curentTittleBlockId);
					
					//Add the view on sheet
					XYZ location  = new XYZ(0,0,0);
					Viewport.Create(_doc, planSheet.Id,currentView.Id , location);
					
				}
				
				tx.Commit();
			}
		}
		
		public ElementId ViewFamilyTypeId(ViewFamily ViewType, Document doc)
		{
			IEnumerable<ViewFamilyType> viewFamilyTypes = from elem in new FilteredElementCollector(doc).OfClass(typeof(ViewFamilyType))
				let type = elem as ViewFamilyType
				where type.ViewFamily == ViewType
				select type;
			return viewFamilyTypes.First().Id;
		}
		
		public void ExportViewTemplatesList()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//find all view
			IEnumerable<View> views = from elem in new FilteredElementCollector(doc).OfClass(typeof(View))
				let view = elem as View
				select view;
			
			//Create a text file for exporting
			List<string> lines = new List<string>();
			//Add the first line
			lines.Add("TemplateName");
			
			foreach (View view in views) {
				if (view.IsTemplate)
				{
					lines.Add(view.Name);
				}
			}
			
			lines = lines.Distinct().ToList();
			string exportpath = @"C:\Users\smoreau\Documents\Travail\BIM42\ExportViewsList\templates.csv";
			File.WriteAllLines(exportpath,lines.ToArray(),Encoding.UTF8);
		}
		
		public void ExportFiltersList()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//find all filters
			IEnumerable<ParameterFilterElement> filters = from elem in new FilteredElementCollector(doc).OfClass(typeof(ParameterFilterElement))
				let filter = elem as ParameterFilterElement
				select filter;
			
			//Create a text file for exporting
			List<string> lines = new List<string>();
			//Add the first line
			lines.Add("FilterName");
			
			foreach (ParameterFilterElement filter in filters) {
				
				lines.Add(filter.Name);
			}
			
			lines = lines.Distinct().ToList();
			string exportpath = @"C:\Users\smoreau\Documents\Travail\BIM42\ExportViewsList\filters.csv";
			File.WriteAllLines(exportpath,lines.ToArray(),Encoding.UTF8);
		}
		
		public void ExportViewsList()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//find all view
			IEnumerable<View> views = from elem in new FilteredElementCollector(doc).OfClass(typeof(View))
				let view = elem as View
				select view;
			
			//Create a text file for exporting
			List<string> lines = new List<string>();
			//Add the first line
			lines.Add("ViewName;ViewType;IsTemplate;TemplateName;LevelName;FilterName");
			
			foreach (View v in views) {
				
				Level level = v.GenLevel;
				
				string levelName = "";
				if (level != null)
				{
					levelName  =  level.Name;
				}
				
				ICollection<ElementId> filterIds;
				//Get view filters
				try {
					filterIds = v.GetFilters();
				} catch (Autodesk.Revit.Exceptions.InvalidOperationException) {
					filterIds = new List<ElementId>() ;
				}
				
				string templateName = "";
				
				if (v.ViewTemplateId.IntegerValue != -1)
				{
					templateName  =  doc.GetElement(v.ViewTemplateId).Name;
				}
				
				string viewinfos =
					v.ViewName + ";" +
					v.ViewType + ";" +
					v.IsTemplate.ToString()  + ";" +
					templateName  + ";" +
					levelName;
				
				if (filterIds.Count != 0)
				{
					foreach (ElementId filterId in filterIds) {
						
						string filterName = doc.GetElement(filterId).Name;
						
						string line  = viewinfos   + ";" + filterName;
						
						lines.Add(line);
					}
				}
				else
				{
					lines.Add(viewinfos);
				}
				

			}
			
			string exportpath = @"C:\Users\smoreau\Documents\Travail\BIM42\ExportViewsList\views.csv";
			File.WriteAllLines(exportpath,lines.ToArray(),Encoding.UTF8);
		}
		
		public void ExportScopeBox()
		{
			Document doc = this.ActiveUIDocument.Document;
			View currentView = doc.ActiveView;
			
			List<Element> scopeBoxes
				= new FilteredElementCollector( doc, currentView.Id )
				.OfCategory( BuiltInCategory.OST_VolumeOfInterest )
				.WhereElementIsNotElementType().ToList();
			
			List<string> exportLines = new List<string>();
			
			foreach (Element scopeBox in scopeBoxes) {
				
				exportLines.Add(scopeBox.Name + ";" + scopeBox.Id);
			}
			
			File.WriteAllLines(@"C:\Affaires\09-POPB\scopeBoxes.txt",exportLines.ToArray());
		}
		public void RenameScopeBox()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			string[] lines = File.ReadAllLines(@"C:\Affaires\09-POPB\scopeNames.txt",Encoding.Default);
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("RenameScopeBox");
				
				foreach (string line in lines) {
					string[] currentLine = line.Split(';');
					
					Element elem = doc.GetElement(new ElementId(Convert.ToInt16(currentLine[0])));
					
					Parameter NameParam = elem.GetParameters("Nom").First();
					NameParam.Set(currentLine[1]);
				}
				
				tx.Commit();
			}
		}
		
		public void ViewFiltersDelete()
		{
			
			Document doc = this.ActiveUIDocument.Document;
			
			List<ElementId> oViewFiltersToDelete = new List<ElementId>();
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			FilteredElementIterator itor = collector.OfClass(typeof(Autodesk.Revit.DB.FilterElement)).GetElementIterator();
			itor.Reset();
			
			using (Transaction ts = new Transaction(doc,"toto"))
			{
				ts.Start("Remove Filters");
				
				// Iterate through each object found
				while (itor.MoveNext())
				{
					// Get the object
					FilterElement imp = itor.Current as FilterElement;
					if (imp.Name.Contains("_circulation ") == false)
					{
						if (imp.Name.Contains("MUR FUSIBLE") == false)
						{
							if (imp.Name.Contains("COUPES FOSSE ASC") == false)
							{
								if (imp.Name.Contains("Emergency Stairs") == false)
								{
									oViewFiltersToDelete.Add(imp.Id);
								}
							}
						}
						
					}
					
				}
				foreach (ElementId oElemID in oViewFiltersToDelete)
				{
					
					
					try
					{
						doc.Delete(oElemID);
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex.Message);
					}
				}
				ts.Commit();
				
			}
			

		}
		
		public void Sections()
		{
			ElevationCreation elevat = new ElevationCreation(this.ActiveUIDocument.Document);
			elevat.CreatingFolioFromRoom();
		}

	}
}