/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 15/04/2015
 * Time: 06:42
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Mechanical;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace RoomFinishing
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("D78844F8-0DB7-4E8D-AD40-3F43F337994A")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		private Document doc;
		
		public void CopyPasteRoomsAndRoomsSeparation()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			//Current View Id
			ElementId currentViewId = _doc.ActiveView.Id;
			
			//Retrive all element in view
			ICollection<ElementId> viewElementIds = new FilteredElementCollector(_doc).OfCategory(BuiltInCategory.OST_RoomSeparationLines).ToElementIds();
			viewElementIds.Concat(new FilteredElementCollector(_doc).OfCategory(BuiltInCategory.OST_Rooms).ToElementIds());
			
			//Retrive final doc
			Document resultDoc = null;
			
			foreach (Document linkedDoc in this.Application.Documents) {
				
				// Find all rooms in current doc
				if (linkedDoc.PathName.Contains("INGP_PJP_SYN_SOCLE_BAS_SOL.rvt"))
				{
					resultDoc = linkedDoc;
					break;
				}
			}
			
			if (resultDoc != null)
			{
				
				//Create an empty transformation
				Transform transform = Transform.CreateTranslation(new XYZ(0,0,0));
				
				//Create copy Options
				CopyPasteOptions opt = new CopyPasteOptions();
				
				using (Transaction tx = new Transaction(resultDoc)) {
					tx.Start("Custom Copy");
					
					ElementTransformUtils.CopyElements(_doc,viewElementIds,resultDoc,transform, opt);

					tx.Commit();
				}
			}
		}
		
		
		public void CopyPasteRooms()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			//Current View Id
			ElementId currentViewId = _doc.ActiveView.Id;
			
			//Retrive all element in view
			ICollection<ElementId> viewElementIds = new FilteredElementCollector(_doc).OfCategory(BuiltInCategory.OST_Rooms).ToElementIds();
			
			//Retrive final doc
			Document resultDoc = null;
			
			foreach (Document linkedDoc in this.Application.Documents) {
				
				// Find all rooms in current doc
				if (linkedDoc.PathName.Contains("INGP_PJP_SYN_SOCLE_BAS_SOL.rvt"))
				{
					resultDoc = linkedDoc;
					break;
				}
			}
			
			if (resultDoc != null)
			{
				
				//Create an empty transformation
				Transform transform = Transform.CreateTranslation(new XYZ(0,0,0));
				
				//Create copy Options
				CopyPasteOptions opt = new CopyPasteOptions();
				
				using (Transaction tx = new Transaction(resultDoc)) {
					tx.Start("Custom Copy");
					
					ElementTransformUtils.CopyElements(_doc,viewElementIds,resultDoc,transform, opt);

					tx.Commit();
				}
			}
		}
		
		private XYZ GetSurveyPoint(Document doc,bool IsShared)
		{
			//find orign point
			IList<Element> basePoints = new FilteredElementCollector(doc).OfClass(typeof(BasePoint)).ToElements();
			BasePoint surveyPoint = null;
			foreach (Element element in basePoints) {
				BasePoint point = element as BasePoint;
				if (point.IsShared == IsShared)
				{
					surveyPoint = point;
					break;
				}
			}
			if (surveyPoint != null)
			{
				double x = surveyPoint.get_Parameter(BuiltInParameter.BASEPOINT_EASTWEST_PARAM).AsDouble();
				double y = surveyPoint.get_Parameter(BuiltInParameter.BASEPOINT_NORTHSOUTH_PARAM).AsDouble();
				double elevation = surveyPoint.get_Parameter  (BuiltInParameter.BASEPOINT_ELEVATION_PARAM).AsDouble();
				
				
				XYZ surveyCoordinates = new XYZ (x,y,elevation);
				
				return surveyCoordinates;
			}
			else
			{
				return null;
			}
		}
		
		public void DeleteRooms()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			//Current View Id
			ElementId currentViewId = _doc.ActiveView.Id;
			
			//Retrive all element in view
			ICollection<ElementId> viewElementIds = new FilteredElementCollector(_doc).OfCategory(BuiltInCategory.OST_Rooms).ToElementIds();
			
			using (Transaction tx = new Transaction(_doc)) {
				
				tx.Start("Delete Rooms");
				
				_doc.Delete(viewElementIds);
				
				tx.Commit();
			}
			
		}
		
		public void CreateFreeVolume()
		{
			Document _doc = this.ActiveUIDocument.Document;
			
			//Retrive a floor type
			IEnumerable<FloorType> floorTypesEnum = from elem in new FilteredElementCollector(_doc).OfClass(typeof(FloorType))
				let type = elem as FloorType
				where type.Name == "FP"
				select type;
			
			Dictionary<string,FloorType> floorTypesDico = new Dictionary<string, FloorType>();
			
			foreach (FloorType ft in floorTypesEnum) {
				floorTypesDico.Add(ft.Name,ft);
			}
			
			//Retrive all rooms
			IEnumerable<Room> tempsrooms = from elem in new FilteredElementCollector(_doc).OfClass(typeof(SpatialElement))
				let room = elem as Room
				select room;
			
			List<string> logfile = new List<string>();
			
			using (Transaction tx = new Transaction(_doc)) {
				
				tx.Start("Test");
				
				foreach (Room tempRoom in tempsrooms) {
					if (tempRoom != null)
					{
						if (tempRoom.UnboundedHeight != 0)
						{
//							if (tempRoom.Number == "I.S2.J.020" | tempRoom.Number == "B.S2.Q.018" | tempRoom.Number == "B.S2.D.004" )
//								//if ( tempRoom.Number == "S.00.C.006")
//							{
							//selectedRooms.Add(tempRoom);
							string name = tempRoom.Name;
							
							SpatialElementBoundaryOptions opt  = new SpatialElementBoundaryOptions();
							
							IList<IList<Autodesk.Revit.DB.BoundarySegment>> boundarySegments = tempRoom.GetBoundarySegments(opt);
							
							CurveArray crvArray = new CurveArray();
							
							if (boundarySegments.Count != 0)
							{
//								foreach (IList<Autodesk.Revit.DB.BoundarySegment> BoundSegList in boundarySegments) {

								foreach (Autodesk.Revit.DB.BoundarySegment boundSeg in boundarySegments.First()) {
									crvArray.Append(boundSeg.GetCurve());
								}
//								}

								//Retrive room info
								Level rmLevel = _doc.GetElement(tempRoom.LevelId) as Level;
								Parameter param = tempRoom.get_Parameter(BuiltInParameter.ROOM_HEIGHT);
								double rmHeight = param.AsDouble();
								
								FloorType flType = floorTypesEnum.First();
								
								
								
								
								Floor floor = _doc.Create.NewFloor(crvArray,flType,rmLevel,false,new XYZ(0,0,1));
								
								//Change some param on the floor
								param = floor.get_Parameter(BuiltInParameter.FLOOR_HEIGHTABOVELEVEL_PARAM);
								param.Set(rmHeight);
							}
							else
							{
								logfile.Add("no boundary;" + tempRoom.Id.ToString());
							}
							

							
						}
//						}
					}
					
				}
				
				tx.Commit();
				
				//File.WriteAllLines(@"C:\Affaires\04-TGI\Volume Piece\roomVolume_log.txt",logfile);
				
			}
		}
		
		private FloorType CreateNewFloorType(Document _doc, double width)
		{
			FloorType flType = null;
			
			//Retrive a floor type
			IEnumerable<FloorType> floorTypesEnum = from elem in new FilteredElementCollector(_doc).OfClass(typeof(FloorType))
				let type = elem as FloorType
				where type.Name == "Base"
				select type;
			
			FloorType baseFloorType = floorTypesEnum.First();
			
			flType = baseFloorType.Duplicate(width.ToString()) as FloorType;
			
			CompoundStructure compoundStr = flType.GetCompoundStructure();
			
			int layerInd =0;
			
			foreach (CompoundStructureLayer layer in compoundStr.GetLayers()) {
				compoundStr.SetLayerWidth(layerInd,width);
				layerInd++;
			}

			flType.SetCompoundStructure(compoundStr);
			
			return flType;
		}
		
		public void RoomToSpace()
		{
			Document activeDocument = this.ActiveUIDocument.Document;
			
			//Get All linked instance
			FilteredElementCollector collector = new FilteredElementCollector(activeDocument);
			List<RevitLinkInstance> linkInstances = collector.OfCategory(BuiltInCategory.OST_RvtLinks).WhereElementIsNotElementType().ToElements().Cast<RevitLinkInstance>().ToList();
			
			//Get all levels
			collector = new FilteredElementCollector(activeDocument);
			List<Level> levels = collector.OfCategory(BuiltInCategory.OST_Levels).WhereElementIsNotElementType().ToElements().Cast<Level>().ToList();
			
			using (Transaction tx = new Transaction(activeDocument)) {
				tx.Start("Create Spaces");
				
				//Loop on all linked instance
				foreach (RevitLinkInstance linkInstance in linkInstances) {
					
					//Get linked document
					Document linkedDocument = linkInstance.GetLinkDocument();
					
					//Get linked instance position
					Transform t = linkInstance.GetTotalTransform();
					
					//Get rooms in the linkedDocument
					collector = new FilteredElementCollector(linkedDocument);
					List<Room> linkedRooms = collector.OfCategory(BuiltInCategory.OST_Rooms).ToElements().Cast<Room>().ToList();
					
					//Create a space for each room
					foreach (Room room in linkedRooms) {
						if (room.Area > 0)
						{
							LocationPoint locationPoint = room.Location as LocationPoint;
							XYZ roomLocationPoint = locationPoint.Point;
							roomLocationPoint = t.OfPoint(roomLocationPoint);
							
							if (roomLocationPoint != null)
							{
								Level level = GetNearestLevel(roomLocationPoint,levels);
								UV uv = new UV(roomLocationPoint.X, roomLocationPoint.Y);
								
								Space space = activeDocument.Create.NewSpace(level,uv);
								
								space.Number = room.Number;
								space.Name = room.Name;

								Parameter limitOffset = space.get_Parameter(BuiltInParameter.ROOM_UPPER_OFFSET);
								limitOffset.Set(room.get_Parameter(BuiltInParameter.ROOM_UPPER_OFFSET).AsDouble());
							}
						}
					}
				}
				
				tx.Commit();
			}
			
		}
		
		private Level GetNearestLevel(XYZ point,List<Level> levels)
		{
			Level nearestLevel = levels.FirstOrDefault();
			double delta = Math.Abs(nearestLevel.ProjectElevation - point.Z);
			
			foreach (Level currentLevel in levels) {
				if (Math.Abs(currentLevel.ProjectElevation - point.Z) < delta) {
					nearestLevel = currentLevel;
					delta = Math.Abs(currentLevel.ProjectElevation - point.Z);
				}
			}
			
			return nearestLevel;
		}
		
		public void RoomProperties()
		{
			Document document = this.ActiveUIDocument.Document;
			
			// get ready to filter across an entire document
			FilteredElementCollector coll =  new FilteredElementCollector( document );
			
			// Find all rooms
			IEnumerable<Room> rooms = from elem in new FilteredElementCollector(document).WherePasses( new RoomFilter() )
				let room = elem as Room
				select room;
			
			//Create a dictonary from the excel data
			IDictionary<string, string> dataMapping = new Dictionary<string, string>();
			
			//Read the text file and get the rooms properties
			string filePath = @"C:\Users\moreaus\Desktop\Courant\03-PLBM\DataExtraction\Rooms.txt";
			string[] roomsProperties = File.ReadAllLines(filePath);
			
			foreach (string line in roomsProperties)
			{
				string[] propertyLine = line.Split(',');
				if (dataMapping.ContainsKey(propertyLine[0]))
				{
					
				}
				else
				{
					dataMapping.Add(propertyLine[0],line);
				}
				
			}
			
			//Update room properties
			using (Transaction tx = new Transaction(document))
			{

				tx.Start("Add rooms properties");
				
				foreach (Room room in rooms)
				{
					//Get room service
					Parameter service = room.GetParameters("Service").FirstOrDefault();
					string serviceName = service.AsString();
					
					if (serviceName != null)
					{
						if (dataMapping.ContainsKey(serviceName))
						{
							string[] properties = dataMapping[serviceName].Split(',');
							
							//Apply parameters
							Parameter EFF = room.GetParameters("Occupation").FirstOrDefault();
							EFF.Set(properties[1]);
							
							Parameter HSP = room.GetParameters("Finition du plafond").FirstOrDefault();
							HSP.Set(properties[2]);
							
							Parameter NOM_PIECE = room.GetParameters("Nom").FirstOrDefault();
							NOM_PIECE.Set(properties[3]);
							
							Parameter PROGRAMME = room.GetParameters("Programme").FirstOrDefault();
							PROGRAMME.Set(properties[4]);
							
							Parameter SURFACE = room.GetParameters("Finition du sol").FirstOrDefault();
							SURFACE.Set(properties[5]);
						}
						else
						{
						}
					}
					else
					{
					}
				}
				
				tx.Commit();
			}
			
			

		}
		
		public void RoomLocationExport()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			// Filtering for Room elements throws an exception:
			// Input type is of an element type that exists in
			// the API, but not in Revit's native object model.
			// Try using Autodesk.Revit.DB.SpatialElement
			// instead, and then postprocessing the results to
			// find the elements of interest.
			
			//FilteredElementCollector a
			//  = new FilteredElementCollector( doc )
			//    .OfClass( typeof( Room ) );
			
			//Get a csv file
			string csvPath = @"C:\Affaires\04-TGI\FSL\Database\RoomsRenjaming\spacesLocation.csv";
			
			//Create a string list to be exproted
			List<string> roomsData = new List<string>();
			
			FilteredElementCollector a = new FilteredElementCollector( doc ).OfClass( typeof( SpatialElement ) );
			
			foreach( SpatialElement e in a )
			{
				Space room = e as Space;
				
				if( null != room )
				{
					Location loc = room.Location;
					LocationPoint lp = loc as LocationPoint;
					XYZ p = ( null == lp ) ? XYZ.Zero : lp.Point;
					string line = room.Name + ";" + room.Number + ";"+ p.X +";"+p.Y +";" +p.Z;

					roomsData.Add(line);
				}
			}
			
			File.WriteAllLines(csvPath,roomsData.ToArray());
		}
		
		public void NameSpaceFromLocation()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//Get a csv file
			string csvPath = @"C:\Affaires\04-TGI\FSL\Database\RoomsRenjaming\roomsLocation.csv";
			
			//Create a string list to be read
			List<string> roomsData = File.ReadAllLines(csvPath).ToList();
			
			Dictionary<XYZ,string> roomLocation = new Dictionary<XYZ,string>();
			
			foreach (string line in roomsData) {
				string[] data = line.Split(';');
				XYZ point = new XYZ(Convert.ToDouble(data[2]),Convert.ToDouble(data[3]),Convert.ToDouble(data[4]));
				roomLocation.Add(point,data[1]);
			}

			FilteredElementCollector a = new FilteredElementCollector( doc ).OfClass( typeof( SpatialElement ) );
			
//			XYZ translation = new XYZ(461.2227838,347.706376,-1.246719161);
//			XYZ ptOrign = new XYZ(0,0,0);
//			XYZ ptZAxis = XYZ.BasisZ;
//			double angle = 1.50534648;
//
//			Transform rotation = Transform.get_Rotation(ptOrign,ptZAxis,angle);

			using (Transaction tx = new Transaction(doc))
			{

				tx.Start("Rename Spaces");
				
				foreach( SpatialElement e in a )
				{
					Space space = e as Space;
					
					if( null != space )
					{
						Location loc = space.Location;
						Parameter localParam = space.GetParameters("Local").FirstOrDefault();
//
//						if (localParam.AsString() == "TEST1")
//						{
//							Console.WriteLine("toto");
//						}
						
//						LocationPoint lp = loc as LocationPoint;
//						XYZ p = ( null == lp ) ? XYZ.Zero : lp.Point;
//						p = p + translation;
//						p = rotation.OfPoint(p);
						
						
						foreach (XYZ roomLocationPoint in roomLocation.Keys) {
							if (space.IsPointInSpace(roomLocationPoint))
							{
								
								localParam.Set(roomLocation[roomLocationPoint]);
							}
							else
							{
								//localParam.Set("Not found");
							}
						}
					}
				}

				tx.Commit();
			}
		}
		
		public void FloorsOnUpperFaces()
		{
			doc = this.ActiveUIDocument.Document;
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Create insulations");
				
				//Get a floor type
				FloorType floorType = RetriveFloorType();
				
				//Get all room
				List<Element> rooms = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Rooms).ToElements().ToList();
				
				//Loop on every rooms
				foreach (Element e in rooms) {
					
					Room room = e as Room;
					
					if (rooms != null)
					{
						Level level = room.Level;
						
						//Retrive the geometry
						Options opt = new Options();
						GeometryElement geometry = e.get_Geometry(opt);
						Solid solid = geometry.First() as Solid;

						//Loop on every faces
						foreach (Face face in solid.Faces) {
							PlanarFace pf = face as PlanarFace;
							if (pf != null)
							{
								XYZ faceNormal = pf.FaceNormal;
								if (faceNormal.X == 0 && faceNormal.Y == 0)
								{
									if (pf.Origin.Z != level.ProjectElevation)
									{
										foreach (CurveLoop loop in face.GetEdgesAsCurveLoops()) {
											//Create a flat slab
											doc.Create.NewFloor(Curves(loop),floorType,level,false);

										}

									}
								}
								else if (faceNormal.Z ==0)
								{

//									//Create a slab with the correct profile
//									Floor floor = doc.Create.NewFloor(ProjetCurveArray(face),floorType,level,false);
//									tx.Commit();
//									tx.Start();
//									//Move it to the correct elevation
//									SlabShapeEditor shapeEditor = floor.SlabShapeEditor;
//									shapeEditor.Enable();
//									foreach (SlabShapeVertex vertex in shapeEditor.SlabShapeVertices) {
//										XYZ planeOrigin = pf.Origin;
//										XYZ oldPosition = vertex.Position;
//										XYZ normalVector = faceNormal.Multiply(-1/faceNormal.Z);
//										double d = normalVector.X*planeOrigin.X+normalVector.Y*planeOrigin.Y-planeOrigin.Z;
//										XYZ newPosition = new XYZ(oldPosition.X,oldPosition.Y,normalVector.X*oldPosition.X+normalVector.Y*oldPosition.Y-d);
//										shapeEditor.ModifySubElement(vertex,newPosition.Z - oldPosition.Z);
//									}
								}
							}
						}
					}
				}
				tx.Commit();
			}
		}
		
		public void WallsOnFaces()
		{
			doc = this.ActiveUIDocument.Document;
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Create insulations");
				
				//Get a wall type
				WallType wallType = RetriveWallType();
				WallType duplicatedWallType = CreateNewWallType(wallType);
				List<ElementId> addedWallsIds = new List<ElementId>();
				
				//Get all room
				List<Element> rooms = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Rooms).ToElements().ToList();
				
				//Loop on every rooms
				foreach (Element e in rooms) {
					
					Room room = e as Room;
					
					if (rooms != null)
					{
						Level level = room.Level;
						
						//Retrive the geometry
						Options opt = new Options();
						GeometryElement geometry = e.get_Geometry(opt);
						Solid solid = geometry.First() as Solid;

						//Loop on every faces
						foreach (Face face in solid.Faces) {
							PlanarFace pf = face as PlanarFace;
							if (pf != null)
							{
								XYZ faceNormal = pf.FaceNormal;
								if (faceNormal.Z ==0)
								{
									IList<Curve> curves = CurvesList(face);
									XYZ lowerPoint = LowestPoint(curves);
									if (lowerPoint.Z > level.ProjectElevation)
									{
										Wall addedWall = Wall.Create(doc,curves,duplicatedWallType.Id,level.Id,false,-faceNormal);
										
										addedWallsIds.Add(addedWall.Id);
										
										Parameter wallHeight = addedWall.get_Parameter(BuiltInParameter.WALL_BASE_OFFSET);
										wallHeight.Set(lowerPoint.Z-level.ProjectElevation);

										Parameter wallJustification = addedWall.get_Parameter(BuiltInParameter.WALL_KEY_REF_PARAM);
										wallJustification.Set(2);
									}

								}
							}
						}
					}
				}
				
				FailureHandlingOptions options = tx.GetFailureHandlingOptions();

				options.SetFailuresPreprocessor(new PlintePreprocessor());
				// Now, showing of any eventual mini-warnings will be postponed until the following transaction.
				tx.Commit(options);

				tx.Start("Create insulations");
				
				List<ElementId> addedWallsIds2 = new List<ElementId>();
				foreach (ElementId id in addedWallsIds) {
					
					if (doc.GetElement(id) != null)
					{
						addedWallsIds2.Add(id);
					}
				}

				Wall.ChangeTypeId(doc, addedWallsIds2, wallType.Id);

				foreach (ElementId addedWallId in addedWallsIds2)
				{
					Wall addedWall = doc.GetElement(addedWallId) as Wall;
					Parameter wallJustification = addedWall.get_Parameter(BuiltInParameter.WALL_KEY_REF_PARAM);
					wallJustification.Set(3);
				}

				doc.Delete(duplicatedWallType.Id);

				tx.Commit();
			}
		}
		
		public void CreateFaceAboveRoom()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			View3D view3D = doc.ActiveView as View3D;

			IList<ElementFilter> filters = new List<ElementFilter>();

			filters.Add(new ElementCategoryFilter(BuiltInCategory.OST_Floors));
			filters.Add(new ElementCategoryFilter(BuiltInCategory.OST_StructuralFraming));

			ElementFilter filter = new LogicalOrFilter(filters);


			//Retrive all rooms
			IEnumerable<Room> rooms = from elem in new FilteredElementCollector(doc).OfClass(typeof(SpatialElement))
				let room = elem as Room
				select room;
			
			//Get a floor type
			FloorType floorType = RetriveFloorType(doc);
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Create insulation");
				
				
				foreach (Room currentRoom in rooms)
				{
					if (currentRoom != null)
					{
						if (currentRoom.UnboundedHeight != 0)
						{
							Level level = currentRoom.Level;
							
							List<Face> faces = GetUpperFaces(currentRoom,view3D,filter,doc);
							
							foreach (Face face in faces) {
								
								try {
									doc.Create.NewFloor(Curves(face),floorType,level,false,face.ComputeNormal(new UV(0,0)));
									
								} catch (Exception) {
									
									//throw;
								}
							}
							
						}
					}
				}
				
				tx.Commit();
				
			}
		}
		
		private List<Face> GetUpperFaces(Room currentRoom, View3D view3D, ElementFilter filter, Document doc)
		{
			//Create an array of testing point
			XYZ maxBBox = currentRoom.get_BoundingBox(view3D).Max;
			XYZ minBBox = currentRoom.get_BoundingBox(view3D).Min;

			XYZ[,] arrayOfPoints = CreateArrayOfPoints(maxBBox, minBBox);
			XYZ direction = XYZ.BasisZ;

			//Create my reference intersector on structural slabs
			ReferenceIntersector refIntersector = new ReferenceIntersector(filter, FindReferenceTarget.Face, view3D);

			//Create a list of returnie values
			List<Face> upperFaces = new List<Face>();

			//Fire up a vertical laser beam for each point of the array

			foreach (XYZ point in arrayOfPoints)
			{

				if (currentRoom.IsPointInRoom(point))
				{
					ReferenceWithContext referencesWithContext = refIntersector.FindNearest(point, direction);

					if (referencesWithContext != null)
					{
						Reference reference = referencesWithContext.GetReference();
						
						if (reference.ElementReferenceType == ElementReferenceType.REFERENCE_TYPE_SURFACE)
						{
							Face hitFace = doc.GetElement(reference.ElementId).GetGeometryObjectFromReference(reference) as Face;
							upperFaces.Add(hitFace);
						}
					}
				}
			}
			
			upperFaces = upperFaces.Distinct().ToList();
			
			return upperFaces;
		}
		
		private CurveArray Curves(Face face)
		{
			CurveLoop loop = face.GetEdgesAsCurveLoops().First();
			CurveArray curves = new CurveArray();
			
			foreach (Curve curve in loop) {
				
				curves.Append(curve);
			}
			
			return curves;
		}
		
		private FloorType RetriveFloorType(Document doc)
		{
			IEnumerable<FloorType> floorTypes = from elem in new FilteredElementCollector(doc).OfClass(typeof(FloorType))
				let type = elem as FloorType
				select type;
			
			FloorType floorType = doc.GetElement(floorTypes.First().Id) as FloorType;

			return floorType;
		}
		
		private XYZ[,] CreateArrayOfPoints(XYZ max, XYZ min)
		{
			double step = 2;
			XYZ startingPoint = min;  // new XYZ(originPoint.X - step * (arraySize / 2 - 1), originPoint.Y - step * (arraySize / 2 - 1), originPoint.Z);

			int xSize = (int)Math.Round((max.X - min.X)/step) +1;
			int ySize = (int)Math.Round((max.Y - min.Y)/step) + 1;

			XYZ[,] arrayOfPoints = new XYZ[xSize, ySize];

			for (int i = 0; i < xSize; i++)
			{
				for (int j = 0; j < ySize; j++)
				{
					arrayOfPoints[i, j] = new XYZ(startingPoint.X + step * i, startingPoint.Y + step * j, startingPoint.Z);
				}
			}

			return arrayOfPoints;
		}
		
		private CurveArray Curves(CurveLoop loop)
		{

			CurveArray curves = new CurveArray();

			foreach (Curve curve in loop) {
				
				curves.Append(curve);
			}
			
			return curves;
		}
		
		private IList<Curve> CurvesList(Face face)
		{
			IList<Curve> curves = new List<Curve>();

			foreach (CurveLoop loop in face.GetEdgesAsCurveLoops()) {
				foreach (Curve curve in loop) {
					
					curves.Add(curve);
				}
			}
			
			return curves;
		}
		
		private XYZ LowestPoint(IList<Curve> curves)
		{
			XYZ point = curves.First().GetEndPoint(0);
			
			foreach (Curve curve in curves) {
				if (curve.GetEndPoint(0).Z < point.Z)
				{
					point = curve.GetEndPoint(0);
				}
			}
			
			return point;
		}
		
		private CurveArray ProjetCurveArray(Face inputFace)
		{
			CurveArray inputCurveArray = Curves(inputFace.GetEdgesAsCurveLoops().FirstOrDefault());
			CurveArray curves = new CurveArray();
			foreach (Curve curve in inputCurveArray) {
				//check if the curve is a line
				Line line = curve as Line;
				if (line != null)
				{
					XYZ tempPoint = line.GetEndPoint(0);
					XYZ endpoint1 = new XYZ(tempPoint.X,tempPoint.Y,0);
					tempPoint = line.GetEndPoint(1);
					XYZ endpoint2 = new XYZ(tempPoint.X,tempPoint.Y,0);
					if (endpoint1.DistanceTo(endpoint2)>0)
					{
						curves.Append(Line.CreateBound(endpoint1,endpoint2));
					}
					
				}
				Arc arc = curve as Arc;
				if (arc != null)
				{
					XYZ tempPoint = arc.GetEndPoint(0);
					XYZ endpoint1 = new XYZ(tempPoint.X,tempPoint.Y,0);
					tempPoint = arc.GetEndPoint(1);
					XYZ endpoint2 = new XYZ(tempPoint.X,tempPoint.Y,0);
					tempPoint = arc.Evaluate(0.5,true);
					XYZ midpoint = new XYZ(tempPoint.X,tempPoint.Y,0);
					if (endpoint1.DistanceTo(endpoint2)>0)
					{
						curves.Append(Arc.Create(endpoint1,endpoint2,midpoint));
					}
				}
			}
			
			return curves;
		}
		
		private FloorType RetriveFloorType()
		{
			IEnumerable<FloorType> floorTypes = from elem in new FilteredElementCollector(doc).OfClass(typeof(FloorType))
				let type = elem as FloorType
				select type;
			
			FloorType floorType = doc.GetElement(floorTypes.First().Id) as FloorType;

			return floorType;
		}
		
		private WallType RetriveWallType()
		{
			IEnumerable<WallType> wallTypes = from elem in new FilteredElementCollector(doc).OfClass(typeof(WallType))
				let type = elem as WallType
				select type;
			
			WallType wallType = doc.GetElement(wallTypes.First().Id) as WallType;

			return wallType;
		}
		
		private WallType CreateNewWallType(WallType wallType)
		{
			WallType newWallType;

			newWallType = wallType.Duplicate("newWallTypeName") as WallType;

			CompoundStructure cs = newWallType.GetCompoundStructure();

			IList<CompoundStructureLayer> layers = cs.GetLayers();
			int layerIndex = 0;

			foreach (CompoundStructureLayer csl in layers)
			{
				double layerWidth = csl.Width * 2;
				if (cs.GetRegionsAssociatedToLayer(layerIndex).Count == 1)
				{
					cs.SetLayerWidth(layerIndex, layerWidth);
				}
				
				layerIndex++;
			}

			newWallType.SetCompoundStructure(cs);

			return newWallType;
		}
	}
	
	/// <summary>
	/// Manage Warning in the Revit interface
	/// </summary>
	public class PlintePreprocessor : IFailuresPreprocessor
	{
		public FailureProcessingResult PreprocessFailures(FailuresAccessor failuresAccessor)
		{
			IList<FailureMessageAccessor> failList = new List<FailureMessageAccessor>();
			// Inside event handler, get all warnings
			failList = failuresAccessor.GetFailureMessages();
			foreach (FailureMessageAccessor failure in failList)
			{
				// check FailureDefinitionIds against ones that you want to dismiss,
				FailureDefinitionId failID = failure.GetFailureDefinitionId();
				// prevent Revit from showing Unenclosed room warnings
				if (failID == BuiltInFailures.OverlapFailures.WallsOverlap)
				{
					failuresAccessor.DeleteWarning(failure);
				}
			}

			return FailureProcessingResult.Continue;
		}
	}
}