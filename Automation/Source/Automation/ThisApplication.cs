﻿/*
 * Created by SharpDevelop.
 * User: smoreau
 * Date: 6/14/2014
 * Time: 12:26 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB.Structure;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Mechanical;
using CR = Autodesk.Revit.Creation;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Automation
{
	[Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
	[Autodesk.Revit.DB.Macros.AddInId("63CA5203-0A50-450B-A29C-37D459DB910E")]
	public partial class ThisApplication
	{
		private void Module_Startup(object sender, EventArgs e)
		{

		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
		
		public void GetFamiliesCategory()
		{
			string dirPath = @"S:\BB0000-Biblio\60-ELE\Equipements electriques\1-OBJETS REVIT 2D-3D";
			DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
			
			FileInfo[] familiesFiles = dirInfo.GetFiles("*.rfa",SearchOption.AllDirectories);
			List<string> lines = new List<string>();
			string sep = ";";
			lines.Add("Name"+sep+"Path"+sep+"Category");
			
			foreach (FileInfo familiesFile in familiesFiles) {
				
				Regex regex = new Regex(@"[\s\S]+.\d\d\d\d.rfa");
				Match match = regex.Match(familiesFile.Name);
				
				if (!match.Success)
				{
					try {
						Document familyDoc = this.Application.OpenDocumentFile(familiesFile.FullName);
						lines.Add(familiesFile.Name+sep+familiesFile.FullName+sep+familyDoc.OwnerFamily.FamilyCategory.Name);
						familyDoc.Close();
					} catch (Exception ex) {
						
						Console.WriteLine("error" + ex.Message);
					}

				}

			}
			
			string reportPath = @"S:\BB0000-Biblio\60-ELE\Equipements electriques\1-OBJETS REVIT 2D-3D\report2.csv";
			File.WriteAllLines(reportPath,lines.ToArray(),Encoding.UTF8);
		}
		
		public void ExportFamiliesSnapshots()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			List<Family> elements = new FilteredElementCollector(doc).OfClass(typeof(Family)).ToElements()
				.Cast<Family>().ToList();
			
			
			foreach (Family fam in elements) {
				
				if (fam.IsEditable)
				{
					FamilySnapshot(fam);
				}
			}
			
		}
		
		private void FamilySnapshot(Family family)
		{
			Document doc = this.ActiveUIDocument.Document;
			string imagePath = @"C:\Affaires\00-Revit\Bibliothèque\Images";
			string tempImagePath = Path.Combine(imagePath,family.Name);
			
			Document familyDoc = doc.EditFamily( family );
			
			IList<ElementId> views = new List<ElementId>();
			
			//Create Image export options
			ImageExportOptions imageExportOptions = new ImageExportOptions
			{
				FilePath = tempImagePath,
				FitDirection = FitDirectionType.Horizontal,
				HLRandWFViewsFileType = ImageFileType.PNG,
				ImageResolution = ImageResolution.DPI_300,
				ShouldCreateWebSite = false,
			};

			
			//Create a proper 3D view
			ViewFamilyType viewFamilyType = new FilteredElementCollector(familyDoc).OfClass(typeof(ViewFamilyType)).OfType<ViewFamilyType>()
				.Where( x =>x.ViewFamily == ViewFamily.ThreeDimensional ).FirstOrDefault();
			
			if (viewFamilyType != null)
			{
				
				using (Transaction tx = new Transaction(familyDoc)) {
					
					tx.Start("Create Snapshot");
					
					try
					{
						View3D view3D = View3D.CreateIsometric(familyDoc,viewFamilyType.Id);
						views.Add( view3D.Id );
						
						// Settings for best quality
						var graphicDisplayOptions= view3D.get_Parameter(BuiltInParameter.MODEL_GRAPHICS_STYLE );
						graphicDisplayOptions.Set( 6 );
						
						view3D.Scale = 20;
						
						imageExportOptions.ZoomType = ZoomFitType.FitToPage;
						imageExportOptions.ViewName = "tmp";
						
						if( views.Count > 0 )
						{
							imageExportOptions.SetViewsAndSheets( views );
							imageExportOptions.ExportRange = ExportRange.SetOfViews;
						}
						else
						{
							imageExportOptions.ExportRange = ExportRange.VisibleRegionOfCurrentView;
						}
						
						if( ImageExportOptions.IsValidFileName(tempImagePath ) )
						{
							familyDoc.ExportImage( imageExportOptions );
						}
						
					}
					catch (Exception) {
						
					}

					tx.Commit();
				}
				
				

			}
			
			familyDoc.Close( false );
		}
		
		private void FamilySnapshotFromDoc(Document familyDoc)
		{
			string imagePath = @"C:\Affaires\00-Revit\Bibliothèque\Images";
			string tempImagePath = Path.Combine(imagePath,familyDoc.Title);
			
			
			IList<ElementId> views = new List<ElementId>();
			
			//Create Image export options
			ImageExportOptions imageExportOptions = new ImageExportOptions
			{
				FilePath = tempImagePath,
				FitDirection = FitDirectionType.Horizontal,
				HLRandWFViewsFileType = ImageFileType.PNG,
				ImageResolution = ImageResolution.DPI_300,
				ShouldCreateWebSite = false,
			};

			
			//Create a proper 3D view
			ViewFamilyType viewFamilyType = new FilteredElementCollector(familyDoc).OfClass(typeof(ViewFamilyType)).OfType<ViewFamilyType>()
				.Where( x =>x.ViewFamily == ViewFamily.ThreeDimensional ).FirstOrDefault();
			
			if (viewFamilyType != null)
			{
				
				using (Transaction tx = new Transaction(familyDoc)) {
					
					tx.Start("Create Snapshot");
					
					try
					{
						View3D view3D = View3D.CreateIsometric(familyDoc,viewFamilyType.Id);
						views.Add( view3D.Id );
						
						// Settings for best quality
						var graphicDisplayOptions= view3D.get_Parameter(BuiltInParameter.MODEL_GRAPHICS_STYLE );
						graphicDisplayOptions.Set( 6 );
						
						view3D.Scale = 20;
						
						imageExportOptions.ZoomType = ZoomFitType.FitToPage;
						imageExportOptions.ViewName = "tmp";
						
						if( views.Count > 0 )
						{
							imageExportOptions.SetViewsAndSheets( views );
							imageExportOptions.ExportRange = ExportRange.SetOfViews;
						}
						else
						{
							imageExportOptions.ExportRange = ExportRange.VisibleRegionOfCurrentView;
						}
						
						if( ImageExportOptions.IsValidFileName(tempImagePath ) )
						{
							familyDoc.ExportImage( imageExportOptions );
						}
						
					}
					catch (Exception) {
						
					}

					tx.Commit();
				}
				
				

			}
			
//			familyDoc.Close( false );
		}
		
		public void EditFamiliesCategory()
		{
//			string dirPath = @"C:\Affaires\00-Revit\Familles Elec\Familles Neuro\Nouveau dossier\Equipement électrique";
//			DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
			
			string fileListPath = @"C:\Affaires\00-Revit\Familles Elec\Familles Neuro\Nouveau dossier\EditFamilies.csv";
			string[] lines = File.ReadAllLines(fileListPath,Encoding.GetEncoding("iso-8859-1"));

			
			foreach (string line in lines) {
				string[] family = line.Split(';');
				Document familyDoc = this.Application.OpenDocumentFile(family[0]);
				
				Categories categories = familyDoc.Settings.Categories;
				Category category = null;
				
				foreach (Category cat in categories) {
					if (cat.Name == family[1])
					{
						category = cat;
					}
				}
				if (category != null)
				{
					using (Transaction tx = new Transaction(familyDoc)) {
						tx.Start("Change Category");
						familyDoc.OwnerFamily.FamilyCategory = category;
						tx.Commit();
					}

					familyDoc.Save();
				}
				else
				{
					Console.WriteLine("test");
				}

				familyDoc.Close();
			}
		}
		
		public void CopyTotype()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			List<FamilyInstance> elements = new FilteredElementCollector(doc).OfClass(typeof(FamilyInstance)).ToElements()
				.Cast<FamilyInstance>().ToList();
			
			using (Transaction tx = new Transaction(doc))
			{
				tx.Start("Pass Param");
				
				foreach (FamilyInstance instance in elements) {
					
					IList<Parameter> parameters = instance.GetParameters("Commentaires");
					
					if (parameters.Count != 0)
					{
						Parameter param = parameters.FirstOrDefault();
						
						FamilySymbol symbol = instance.Symbol;
						
						IList<Parameter> symbolParameters = symbol.GetParameters("Description");
						
						if (symbolParameters.Count != 0)
						{
							Parameter symbolParam = symbolParameters.FirstOrDefault();
							
							symbolParam.Set(param.AsString());
						}
						
					}
				}
				
				tx.Commit();
				
			}
		}
		
		public void ListFamily()
		{
//			Dans MS-dos, faire la liste des familles:  dir s:\BB0000-AFFAIRE\01-FAMILLES\00-LOT\*.rfa /b/s | clip
			string fileListPath = @"C:\Affaires\01-Developpement\Projecyt\revitFamiliesList.csv";
			string[] lines = File.ReadAllLines(fileListPath,Encoding.GetEncoding("iso-8859-1"));

			string outputPath = @"C:\Affaires\01-Developpement\Projecyt\RevitFamilyDictionary.csv";
			List<string> familyList = new List<string>();
			familyList.Add("Path;File Name;Category;Family Name;Family Type;Family Placement Type;Description;IMG Path;IMG");
			File.AppendAllLines(outputPath,familyList.ToArray(),Encoding.GetEncoding("iso-8859-1"));

			
			foreach (string line in lines) {
				string[] inputLine = line.Split(';');
				string familyFileName = inputLine[0];
				
				if (File.Exists(familyFileName))
				{
					try {
						Document familyDoc = this.Application.OpenDocumentFile(familyFileName);
						
						familyList = new List<string>();
						
						Family family = familyDoc.OwnerFamily;
						
						FamilySnapshotFromDoc(familyDoc);
						string imagePath = @"C:\Affaires\00-Revit\Bibliothèque\Images";
						string tempImagePath = Path.Combine(imagePath,Path.GetFileNameWithoutExtension(familyFileName));
						IList<Parameter> parameters = family.GetParameters("Description");
						Parameter parameter = parameters.FirstOrDefault();
						
						if (family.GetFamilySymbolIds().Count != 0)
						{
							foreach (ElementId typeId in family.GetFamilySymbolIds()) {
								FamilySymbol symbol = familyDoc.GetElement(typeId) as FamilySymbol;
								
								familyList.Add(string.Format("{0};{1};{2};{3};{4};{5};{6}",
								                             familyFileName,
								                             family.FamilyCategory.Name,
								                             family.Name,
								                             symbol.Name,
								                             family.FamilyPlacementType,
								                             tempImagePath,
								                             ""
								                            ));
							}
						}
						else
						{
							if (parameter != null)
							{
								string description;
								if (parameter.AsString() == null)
								{
									description = "Non renseigné";
									
								}
								else
								{
									description = parameter.AsString();
								}
								
								
								familyList.Add(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}",
								                             familyFileName,
								                             Path.GetFileNameWithoutExtension(familyFileName),
								                             family.FamilyCategory.Name,
								                             family.Name,
								                             family.Name,
								                             family.FamilyPlacementType,
								                             description,
								                             tempImagePath,
								                             ""
								                            ));
							}
							else
							{
								familyList.Add(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}",
								                             familyFileName,
								                             Path.GetFileNameWithoutExtension(familyFileName),
								                             family.FamilyCategory.Name,
								                             family.Name,
								                             family.Name,
								                             family.FamilyPlacementType,
								                             "Non renseigné",
								                             tempImagePath,
								                             ""
								                            ));
							}
						}

						
						familyDoc.Close(false);
						
						File.AppendAllLines(outputPath,familyList.ToArray(),Encoding.GetEncoding("iso-8859-1"));
						
					} catch (Exception) {
						
					}
				}

			}
			
			
			//File.WriteAllLines(outputPath,familyList.ToArray(),Encoding.GetEncoding("iso-8859-1"));
		}
		public void ListFamiliesInModel()
		{
//			string fileListPath = @"C:\Affaires\01-Developpement\Projecyt\revitFamiliesList.csv";
//			string[] lines = File.ReadAllLines(fileListPath,Encoding.GetEncoding("iso-8859-1"));
			
			Document doc = this.ActiveUIDocument.Document;
			
			List<Family> elements = new FilteredElementCollector(doc).OfClass(typeof(Family)).ToElements()
				.Cast<Family>().ToList();
			string imagePath = @"C:\Affaires\00-Revit\Bibliothèque\Images";
			
			foreach (Family fam in elements)
			{
				string tempImagePath = Path.Combine(imagePath,fam.Name);
				
				if (fam.IsEditable)
				{
					FamilySnapshot(fam);
				}
			}
			
			List<FamilySymbol> symbols = new FilteredElementCollector(doc).WherePasses(new ElementClassFilter(typeof(FamilySymbol)))
				.Cast<FamilySymbol>().ToList();
			string outputPath = @"C:\Affaires\01-Developpement\Projecyt\RevitFamilyDictionary.csv";
			List<string> familyList = new List<string>();
			familyList.Add("Doc Title;File Name;Category;Family Name;Family Type;Family Placement Type;Description;IMG Path;IMG");
			
			foreach (FamilySymbol familySymbol in symbols)
			{
				Family familyFileName = familySymbol.Family;
				IList<Parameter> parameters = familySymbol.GetParameters("Description");
				Parameter parameter = parameters.FirstOrDefault();
				string tempPath = Path.Combine(imagePath,familySymbol.Family.Name);
				
				if (parameter != null)
				{
					string description;
					if (parameter.AsString() == null)
					{
						description = "Non renseigné";
						
					}
					else
					{
						description = parameter.AsString();
					}
					
					familyList.Add(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}",
					                             doc.Title,
					                             familyFileName.Name,
					                             familyFileName.FamilyCategory.Name,
					                             familyFileName.Name,
					                             familySymbol.Name,
					                             familyFileName.FamilyPlacementType,
					                             description,
					                             tempPath,
					                             "IMG"
					                            ));
				}
				else
				{
					familyList.Add(string.Format("{0};{1};{2};{3};{4};{5};{6}",
					                             doc.Title,
					                             familyFileName.Name,
					                             familyFileName.FamilyCategory.Name,
					                             familyFileName.Name,
					                             familySymbol.Name,
					                             familyFileName.FamilyPlacementType,
					                             "Non renseigné",
					                             tempPath,
					                             "IMG"
					                            ));
				}
			}
			
			
			File.WriteAllLines(outputPath,familyList.ToArray(),Encoding.GetEncoding("iso-8859-1"));
		}
		
		public void MakeMarksUnique()
		{
			Document document = this.ActiveUIDocument.Document;
			FilteredElementCollector collector = new FilteredElementCollector(document);
			
			foreach (BuiltInCategory cat in Enum.GetValues(typeof(BuiltInCategory))) {
				IList<Element> elementList = collector.OfCategory(cat).OfClass(typeof(FamilyInstance)).ToList();
				if (elementList.Count > 0)
				{
					IList<string> markValues = new List<string>();
					using (Transaction t = new Transaction(document,"Modify Mark values"))
					{
						t.Start();
						foreach (Element e in elementList)
						{
							Parameter p = e.GetParameters("Identifiant").FirstOrDefault();
							if (markValues.Contains(p.AsString()))
								p.Set(p.AsString() + "*");
							else
								markValues.Add(p.AsString());
						}
						t.Commit();
					}
				}
			}
		}
		
		public void CreateFamillies()
		{
			Document doc = this.ActiveUIDocument.Document;

			//Get all family loaded in model
			Dictionary<string,FamilySymbol> symbols = GetFamilySymbols(doc);
			
			//Read the csv file for informations
			Dictionary<ElementId,List<CustomFamilySymbols>> roomList = RetriveRoomsEquipements(symbols);
			
			//Create the FamilyInstanceCreationData list for batch creation
			List<CR.FamilyInstanceCreationData> dataList = new List<CR.FamilyInstanceCreationData>();
			
			//loop on all room
			foreach (ElementId roomId in roomList.Keys) {
				
				Room room = doc.GetElement(roomId) as Room;
				Element levelElem = doc.GetElement(room.LevelId);
				
				if (room.Area != 0)
				{
					Level level = levelElem  as Level;
					
					
					List<XYZ> pointsInRoom = CreateArrayInRoom(room);
					int rank = 0;

					//Create famillies in this room
					foreach (CustomFamilySymbols symbol in roomList[roomId]) {
						if (symbol.Offset == null)
						{
							Console.WriteLine("test");
						}
						if (symbol.Symbol == null)
						{
							Console.WriteLine("test");
						}
						if (level == null)
						{
							Console.WriteLine("test");
						}
						CR.FamilyInstanceCreationData creationData = Application.Create.NewFamilyInstanceCreationData(
							pointsInRoom[rank] + symbol.Offset,symbol.Symbol,level,StructuralType.NonStructural);

						
						dataList.Add(creationData);
						rank++;
					}
				}

			}
			
			using (Transaction tx = new Transaction(doc)) {
				tx.Start("Create Equipement");
				//Batch create the famillies
				doc.Create.NewFamilyInstances2(dataList);
				tx.Commit();
			}

			
			//
		}
		
		private List<XYZ> CreateArrayInRoom(Room room)
		{
			List<XYZ> points = new List<XYZ>();
			LocationPoint locPoint = room.Location as LocationPoint;
			XYZ roomCentroid = locPoint.Point;
			//Remove Z value
			roomCentroid = new XYZ(roomCentroid.X,roomCentroid.Y,0);
			points.Add(roomCentroid);

			XYZ[] offset = new XYZ[] { new XYZ(1,0,0), new XYZ(0,1,0), new XYZ(-1,0,0), new XYZ(0,-1,0)};
			
			int i= 0;
			//double step = 1;
			
			double x=0;
			double y = 0;
			double dx = 0;
			double dy = -1;
			
			while (i<500) {
				
				if (x == y | (x<0 & x == -y) | (x > 0 & x == 1-y)) {
					double temp = dx;
					dx= -dy;
					dy = temp;
				}
				
				x = x+dx;
				y = y+dy;
				
				points.Add(roomCentroid + new XYZ(x,y,0));
				
				i++;
			}
			
			return points;
		}
		
		private Dictionary<string,FamilySymbol> GetFamilySymbols(Document doc)
		{
			//Find all symbols
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			IList<Element> elementList = collector.OfClass(typeof(FamilySymbol)).ToElements().ToList();
			
			Dictionary<string,FamilySymbol> symbols = new Dictionary<string, FamilySymbol>();
			
			foreach (Element element in elementList) {
				FamilySymbol symbol = element as FamilySymbol;
				//only add the first symbol of a given name
				if (!symbols.ContainsKey(symbol.Name))
				{
					symbols.Add(symbol.Name,symbol);
				}
			}
			
			return symbols;
		}
		
		private Dictionary<ElementId,List<CustomFamilySymbols>> RetriveRoomsEquipements(Dictionary<string,FamilySymbol> symbols)
		{
			//Create a dictionary for storing room equipements
			Dictionary<ElementId,List<CustomFamilySymbols>> roomList = new Dictionary<ElementId, List<CustomFamilySymbols>>();
			
			//Read the equipement list file
			String equipementListPath = @"C:\Affaires\15-NEURO\Import Familles ELE\equipements.csv";
			string[] lines = File.ReadAllLines(equipementListPath);
			
			//Read first line
			string[] headers = lines[0].Split(';');
			FamilySymbol[] headerSymbols = new FamilySymbol[headers.Length];
			for (int i = 1; i < headers.Length; i++) {
				if (symbols.ContainsKey(headers[i]))
				{
					headerSymbols[i] = symbols[headers[i]];
				}
			}
			
			//Read Second lines
			string[] heights = lines[1].Split(';');
			double[] headerHeights = new double[heights.Length];
			for (int i = 1; i < heights.Length; i++) {
				headerHeights[i] = Convert.ToDouble(heights[i]);
			}
			
			//loop on all lines
			for (int i = 2; i < lines.Length; i++) {
				string[] equipements = lines[i].Split(';');
				
				ElementId roomId  = new ElementId(Convert.ToInt32(equipements[0]));
				List<CustomFamilySymbols> roomSymbols = new List<CustomFamilySymbols>();
				
				for (int j = 1; j < equipements.Length; j++) {
					int number;
					if (equipements[j] == "") {number = 0;} else {number = Convert.ToInt32(equipements[j]);}
					CustomFamilySymbols customSymbol = new CustomFamilySymbols(headerSymbols[j],number,headerHeights[j]);
					roomSymbols.AddRange(customSymbol.Symbols);
				}
				
				roomList.Add(roomId,roomSymbols);
			}
			
			return roomList;
		}
		
		public void MEPFittingCreator()
		{
			//String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2014\Libraries\France\MEP Mécanique","*.rfa",SearchOption.AllDirectories);
			//String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2014\Libraries\France\MEP Plomberie","*.rfa",SearchOption.AllDirectories);
			//String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2014\Libraries\France\MEP Tuyauterie","*.rfa",SearchOption.AllDirectories);
			//String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2014\Libraries\France\MEP Protection contre les incendies","*.rfa",SearchOption.AllDirectories);
			//String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2014\Libraries\France\MEP Gaine","*.rfa",SearchOption.AllDirectories);
			String[] RFAPaths = Directory.GetFiles(@"C:\Travail\Revit Dev\MEPFittingCreator\Silencieux","*.rfa",SearchOption.AllDirectories);
			//C:\Travail\Revit Dev\MEPFittingCreator\Silencieux
			
			
			List<string> logFile = new List<string>();
			
			
			Document doc = this.ActiveUIDocument.Document;
			View currentView = doc.ActiveView;

			FilteredElementCollector collector = new FilteredElementCollector(doc);
			ICollection<Element> collection = collector.OfClass(typeof(Level)).ToElements();
			Level positionLevel = collection.First() as Level;
			
			//Get duct types
			DuctType rectangularDuctType = doc.GetElement(new ElementId(136700)) as DuctType;
			DuctType roundDuctType = doc.GetElement(new ElementId(136701)) as DuctType;
			DuctType ovalDuctType = doc.GetElement(new ElementId(136702)) as DuctType;

			//List created instance
			List<FamilyInstance> instances = new List<FamilyInstance>();
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("MEPFittingCreator");
				
				double space = 14;
				int i = 0;
				
				foreach (string RFAPath in RFAPaths) {
					
					Family currentFamily;
					
					doc.LoadFamily(RFAPath,out currentFamily);
					
					
					if (currentFamily != null)
					{
						FamilySymbol currentFamilySymbol = (FamilySymbol)doc.GetElement( currentFamily.GetFamilySymbolIds().First());
						
						BuiltInCategory enumCategory = (BuiltInCategory)currentFamilySymbol.Category.Id.IntegerValue;
						
						//Find the position on the grid
						double lineNum = Math.Truncate(Convert.ToDouble(i/23));
						double columnNum = i - lineNum*23;
						
						if (!currentFamilySymbol.IsActive) currentFamilySymbol.Activate();
						instances.Add(doc.Create.NewFamilyInstance(new XYZ(columnNum*space,lineNum*space,0),
						                                           currentFamilySymbol,positionLevel,StructuralType.NonStructural));
						
						
						i++;
					}
					else
					{
						logFile.Add(RFAPath);
					}
				}
				
				tx.Commit();
				tx.Start();
				
				foreach (FamilyInstance instance in instances) {
					if (instance.MEPModel != null)
					{
						//MechanicalFitting fitting = instance.MEPModel as MechanicalFitting;
						MEPModel fitting = instance.MEPModel;

						if (fitting != null)
						{
							foreach (Connector connector in fitting.ConnectorManager.Connectors)
							{
								if (connector.Domain == Domain.DomainHvac)
								{
									if (connector.Shape == ConnectorProfileType.Rectangular)
									{
										CreateDuct(connector,rectangularDuctType,doc);
									}
									else if (connector.Shape == ConnectorProfileType.Round)
									{
										CreateDuct(connector,roundDuctType,doc);
									}
									else if (connector.Shape == ConnectorProfileType.Oval)
									{
										CreateDuct(connector,ovalDuctType,doc);
									}
								}
							}
						}
					}
				}
				
				
				tx.Commit();
			}
			
			
			File.WriteAllLines(@"C:\Travail\Revit Dev\Files\LogFamilyLoader.csv",logFile.ToArray(),Encoding.UTF32);
		}
		
		private void CreateDuct(Connector connector, DuctType ductType, Document doc)
		{
			double lenght = 3.28084;
			XYZ point = connector.Origin + lenght*connector.CoordinateSystem.BasisZ;
			
			Duct duct = doc.Create.NewDuct(point, connector,ductType);
			
			if (connector.Shape == ConnectorProfileType.Round)
			{
				double radius = 3.28084*0.1;
				try {
					connector.Radius = radius;
				} catch (Exception) {
					radius = connector.Radius;
				}
				Parameter param = duct.get_Parameter(BuiltInParameter.RBS_CURVE_DIAMETER_PARAM);
				param.Set(radius*2);
			}
			else
			{
				double height = 3.28084*0.1;
				double width = 3.28084*0.1;
				try {
					connector.Height = height;
					connector.Width = width;
				} catch (Exception) {
					height = connector.Height;
					width = connector.Width;
				}
				
				Parameter param = duct.get_Parameter(BuiltInParameter.RBS_CURVE_HEIGHT_PARAM);
				param.Set(height);
				param = duct.get_Parameter(BuiltInParameter.RBS_CURVE_WIDTH_PARAM);
				param.Set(width);
			}

		}
		
		public void FamillyLoader()
		{
			//String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2014\Libraries\France\MEP Mécanique","*.rfa",SearchOption.AllDirectories);
			//String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2014\Libraries\France\MEP Plomberie","*.rfa",SearchOption.AllDirectories);
			//String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2014\Libraries\France\MEP Tuyauterie","*.rfa",SearchOption.AllDirectories);
			//String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2014\Libraries\France\MEP Protection contre les incendies","*.rfa",SearchOption.AllDirectories);
			//String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2014\Libraries\France\MEP Gaine","*.rfa",SearchOption.AllDirectories);
			String[] RFAPaths = Directory.GetFiles(@"C:\ProgramData\Autodesk\RVT 2016\Libraries\France\MEP Gaine","*.rfa",SearchOption.AllDirectories);
			
			
			
			List<string> logFile = new List<string>();
			
			
			Document doc = this.ActiveUIDocument.Document;
			View currentView = doc.ActiveView;

			FilteredElementCollector collector = new FilteredElementCollector(doc);
			ICollection<Element> collection = collector.OfClass(typeof(Level)).ToElements();
			Level positionLevel = collection.First() as Level;

			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("FamilyLoader");
				
				double space = 14;
				int i = 0;
				
				foreach (string RFAPath in RFAPaths) {
					
					Family currentFamily;
					
					doc.LoadFamily(RFAPath,out currentFamily);
					
					if (currentFamily != null)
					{
						FamilySymbol currentFamilySymbol = (FamilySymbol)doc.GetElement( currentFamily.GetFamilySymbolIds().First());
						
						BuiltInCategory enumCategory = (BuiltInCategory)currentFamilySymbol.Category.Id.IntegerValue;
						
						//Find the position on the grid
						double lineNum = Math.Truncate(Convert.ToDouble(i/23));
						double columnNum = i - lineNum*23;
						
						if (!currentFamilySymbol.IsActive) currentFamilySymbol.Activate();
						doc.Create.NewFamilyInstance(new XYZ(columnNum*space,lineNum*space,0),currentFamilySymbol,positionLevel,StructuralType.NonStructural);
						
						i++;
					}
					else
					{
						logFile.Add(RFAPath);
					}
				}
				
				tx.Commit();
			}
			
			
			File.WriteAllLines(@"C:\Travail\Revit Dev\Files\LogFamilyLoader.csv",logFile.ToArray(),Encoding.UTF32);
		}
		
		public void ExtractNestedFamillies()
		{
			
			UIDocument uidoc = this.ActiveUIDocument;
			Autodesk.Revit.DB.Document doc = uidoc.Document;
			
			//Select a family instance
			FamilyInstance fi = doc.GetElement(
				uidoc.Selection.PickObject(
					ObjectType.Element ).ElementId )
				as FamilyInstance;
			
			// Create a filter to retrive all instance of this family
			List<ElementFilter> filters = new List<ElementFilter>();
			foreach (ElementId symbolId in fi.Symbol.Family.GetFamilySymbolIds()) {
				filters.Add(new FamilyInstanceFilter(doc,symbolId));
			}
			ElementFilter filter = new LogicalOrFilter(filters);

			// Apply the filter to the elements in the active document
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			ICollection<Element> familyInstances = collector.WherePasses(filter).ToElements();
			
			using (Transaction tx = new Transaction(doc)) {
				
				tx.Start("Extract Nested Familes");
				
				//Loop on all family instances in the project
				foreach (Element element in familyInstances) {
					
					FamilyInstance instance = element as FamilyInstance;
					
					ICollection<ElementId> subElementsIds = instance.GetSubComponentIds();
					
					//Loop on all nested family
					foreach (ElementId id in subElementsIds) {

						Element ee = doc.GetElement(id);
						FamilyInstance f = ee as FamilyInstance;
						
						//The fammily is face based
						if (f.HostFace != null)
						{
							Element host = f.Host;
							Face face = host.GetGeometryObjectFromReference(f.HostFace) as Face;
							LocationPoint locPoint = f.Location as LocationPoint;
							doc.Create.NewFamilyInstance(face,locPoint.Point,f.HandOrientation,f.Symbol);
						}
						//The fammily is host based
						else if (f.Host !=null)
						{
							LocationPoint locPoint = f.Location as LocationPoint;
							Level level = doc.GetElement( f.LevelId) as Level;
							
							FamilyInstance fam = doc.Create.NewFamilyInstance(locPoint.Point,f.Symbol,f.Host,level,StructuralType.NonStructural);
							
							//Flip the family if necessary
							if (instance.CanFlipFacing)
							{
								if (instance.FacingFlipped) {fam.flipFacing();}
							}
							if (instance.CanFlipHand)
							{
								if (instance.HandFlipped) {fam.flipHand();}
							}
						}
						//The family is point based
						else
						{
							LocationPoint locPoint = f.Location as LocationPoint;
							Level level = doc.GetElement( f.LevelId) as Level;
							doc.Create.NewFamilyInstance(locPoint.Point,f.Symbol,level,StructuralType.NonStructural);
						}
					}

				}
				
				tx.Commit();
			}

		}
		
		public void SetGridWorkset()
		{
			Document doc = this.ActiveUIDocument.Document;
			
			//Select the shared grid workset
			IList<Workset> worksetList = new FilteredWorksetCollector(doc).OfKind(WorksetKind.UserWorkset).ToWorksets();
			int sharedGridWorksetId=0;
			
			foreach (Workset workset in worksetList) {
				if (workset.Name.Contains("Shared Levels and Grids"))
				{
					sharedGridWorksetId = workset.Id.IntegerValue;
				}
			}
			
			if( sharedGridWorksetId == 0 ) return;
			
			//Reference planes
			List<Element> elements =  new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_CLines).ToElements().ToList();
			//Scope box
			List<Element> scopeBoxes = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_VolumeOfInterest).ToElements().ToList();
			elements.AddRange(scopeBoxes);
			
			using (Transaction tx = new Transaction(doc)) {
				tx.Start("Change Workset");
				
				foreach (Element e in elements) {
					
					//Retrive workset parameter
					Parameter wsparam = e.get_Parameter(BuiltInParameter.ELEM_PARTITION_PARAM );
					if( wsparam == null ) continue;
					
					//set workset to Shared Levels and Grids
					wsparam.Set(sharedGridWorksetId);
				}
				
				tx.Commit();
			}
		}
		
		private Autodesk.Revit.ApplicationServices.Application revit;
		private Document familyDocument;
		private Autodesk.Revit.Creation.FamilyItemFactory creationFamily;
		
		public void BatchCreateFamillies()
		{
			Document doc = this.ActiveUIDocument.Document;
			revit = this.Application;
			familyDocument = revit.NewFamilyDocument(@"C:\Users\smoreau\Desktop\Famillies\Metric Generic Model.rft");
			
			if (null != familyDocument)
			{
				creationFamily = familyDocument.FamilyCreate;
				CreateGenericModel(familyDocument);
				
				Family fam = familyDocument.LoadFamily(doc);
				
				familyDocument.Close(false);
				
				Document familyDoc = doc.EditFamily(fam);
				
				string path = @"C:\Users\smoreau\Desktop\Famillies\toto.rfa";
				
				familyDoc.SaveAs(path);
				familyDoc.Close();
			}
			
		}
		
		public void CreateGenericModel(Document familyDocument)
		{
			// use transaction if the family document is not active document
			using (Transaction transaction = new Transaction(familyDocument, "CreateGenericModel"))
			{
				double size = 10;
				transaction.Start();
				CreateExtrusion(size);

				CreateRevolution(size);

				transaction.Commit();
			}

			return;
		}
		
		private void CreateExtrusion(double size)
		{

			#region Create rectangle profile
			CurveArrArray curveArrArray = new CurveArrArray();
			CurveArray curveArray1 = new CurveArray();

			Autodesk.Revit.DB.XYZ normal = Autodesk.Revit.DB.XYZ.BasisZ;
			SketchPlane sketchPlane = CreateSketchPlane(normal, Autodesk.Revit.DB.XYZ.Zero);

			// create one rectangular extrusion
			Autodesk.Revit.DB.XYZ p0 = Autodesk.Revit.DB.XYZ.Zero;
			Autodesk.Revit.DB.XYZ p1 = new Autodesk.Revit.DB.XYZ(size, 0, 0);
			Autodesk.Revit.DB.XYZ p2 = new Autodesk.Revit.DB.XYZ(size, size, 0);
			Autodesk.Revit.DB.XYZ p3 = new Autodesk.Revit.DB.XYZ(0, size, 0);
			Line line1 = Line.CreateBound(p0, p1);
			Line line2 = Line.CreateBound(p1, p2);
			Line line3 = Line.CreateBound(p2, p3);
			Line line4 = Line.CreateBound(p3, p0);
			curveArray1.Append(line1);
			curveArray1.Append(line2);
			curveArray1.Append(line3);
			curveArray1.Append(line4);

			curveArrArray.Append(curveArray1);
			#endregion
			// here create rectangular extrusion
			Extrusion rectExtrusion = creationFamily.NewExtrusion(true, curveArrArray, sketchPlane, size);
//			// move to proper place
//			Autodesk.Revit.DB.XYZ transPoint1 = new Autodesk.Revit.DB.XYZ(-16, 0, 0);
//			ElementTransformUtils.MoveElement(familyDocument, rectExtrusion.Id, transPoint1);
		}
		
		/// <summary>
		/// Create one rectangular profile revolution
		/// </summary>
		private void CreateRevolution(double size)
		{
			#region Create rectangular profile
			CurveArrArray curveArrArray = new CurveArrArray();
			CurveArray curveArray = new CurveArray();

			Autodesk.Revit.DB.XYZ normal = Autodesk.Revit.DB.XYZ.BasisZ;
			SketchPlane sketchPlane = CreateSketchPlane(normal, Autodesk.Revit.DB.XYZ.Zero);

			// create one rectangular profile revolution
			Autodesk.Revit.DB.XYZ p0 = Autodesk.Revit.DB.XYZ.Zero;
			Autodesk.Revit.DB.XYZ p1 = new Autodesk.Revit.DB.XYZ(size/2, size/2, 0);
			Autodesk.Revit.DB.XYZ p2 = new Autodesk.Revit.DB.XYZ(0, size, 0);
			Curve arc = Arc.Create(p0,p2,p1);
			Line axis1 = Line.CreateBound(p0,p2);
			curveArray.Append(arc);
			curveArray.Append(axis1);

			curveArrArray.Append(curveArray);
			#endregion
			// here create rectangular profile revolution
			Revolution revolution1 = creationFamily.NewRevolution(true, curveArrArray, sketchPlane, axis1, -Math.PI, 0);
			// move to proper place
			Autodesk.Revit.DB.XYZ transPoint1 = new Autodesk.Revit.DB.XYZ(0, 32, 0);
			ElementTransformUtils.MoveElement(familyDocument, revolution1.Id, transPoint1);
		}
		
		/// <summary>
		/// Create sketch plane for generic model profile
		/// </summary>
		/// <param name="normal">plane normal</param>
		/// <param name="origin">origin point</param>
		/// <returns></returns>
		internal SketchPlane CreateSketchPlane(Autodesk.Revit.DB.XYZ normal, Autodesk.Revit.DB.XYZ origin)
		{
			// First create a Geometry.Plane which need in NewSketchPlane() method
			Plane geometryPlane = revit.Create.NewPlane(normal, origin);
			if (null == geometryPlane)  // assert the creation is successful
			{
				throw new Exception("Create the geometry plane failed.");
			}
			// Then create a sketch plane using the Geometry.Plane
			SketchPlane plane = SketchPlane.Create(familyDocument, geometryPlane);
			// throw exception if creation failed
			if (null == plane)
			{
				throw new Exception("Create the sketch plane failed.");
			}
			return plane;
		}
	}
	
	public class CustomFamilySymbols
	{
		
		private int _number;
		private double _height;
		
		public CustomFamilySymbols(FamilySymbol symbol,int number,double height)
		{
			_symbol = symbol;
			_height = height;
			_number = number;
		}
		
		private FamilySymbol _symbol;
		public FamilySymbol Symbol
		{
			get { return _symbol;}
		}
		
		
		public XYZ Offset
		{
			get{return new XYZ(0,0,_height*3.28084);}
		}
		
		public List<CustomFamilySymbols> Symbols
		{
			get
			{
				List<CustomFamilySymbols> x = Enumerable.Repeat(this, _number).ToList();
				return x;
			}
		}
	}
}